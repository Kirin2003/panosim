"""
Author:
    SUN-Hao
Description:
    Imu model test

"""

import numpy as np
import matplotlib.pyplot as plt
from imu_model import IMUModel


if __name__=="__main__":
    imu_model = IMUModel()

    num = 100
    t = np.linspace(0, 2 * np.pi, num=num)
    acc_x = np.sin(t)
    acc_noise = np.linspace(0, 2 * np.pi, num=num)

    for i in range(num):
        acc_noise[i],_,_,_,_,_ ,_= imu_model.run(acc_x[i],0,0,0,0,0,0)


    fig,ax = plt.subplots(1,1)

    ax.plot(t,acc_x )
    ax.plot(t,acc_noise)


    plt.show()

