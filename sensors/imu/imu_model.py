"""
Author:
    SUN-Hao
Description:
    IMU model with Gauss noise

"""
import numpy as np


class IMUModel(object):
    def __init__(self,**kwargs):
        self.noise_acc_x_bias = kwargs.get('noise_acc_x_bias',0)
        self.noise_acc_x_stddev = kwargs.get('noise_acc_x_stddev', 0.1)

        self.noise_acc_y_bias = kwargs.get('noise_acc_y_bias', 0.0)
        self.noise_acc_y_stddev = kwargs.get('noise_acc_y_stddev', 0.1)

        self.noise_acc_z_bias = kwargs.get('noise_acc_z_bias', 0.0)
        self.noise_acc_z_stddev = kwargs.get('noise_acc_z_stddev', 0.1)

        self.noise_gyro_x_bias = kwargs.get('noise_gyro_x_bias',0)
        self.noise_gyro_x_stddev = kwargs.get('noise_gyro_x_stddev', 0.1)

        self.noise_gyro_y_bias = kwargs.get('noise_gyro_y_bias', 0.0)
        self.noise_gyro_y_stddev = kwargs.get('noise_gyro_y_stddev', 0.1)

        self.noise_gyro_z_bias = kwargs.get('noise_gyro_z_bias', 0.0)
        self.noise_gyro_z_stddev = kwargs.get('noise_gyro_z_stddev', 0.1)

        self.noise_compass_bias = kwargs.get('noise_compass_bias', 0.0)
        self.noise_compass_stddev = kwargs.get('noise_compass_stddev', 0.1)

        self.noise_seed =0

    def _get_noise_data(self,bias,stddev,data):
        noise = np.random.normal(bias, stddev, 1)
        noise_data = data + noise
        return noise_data

    def run(self,acc_x, acc_y,acc_z, gyro_x, gyro_y, gyro_z,compass):
        acc_x = self._get_noise_data(self.noise_acc_x_bias,self.noise_acc_x_stddev,acc_x)
        # TODO
        return acc_x,acc_y,acc_z,gyro_x, gyro_y, gyro_z,compass
