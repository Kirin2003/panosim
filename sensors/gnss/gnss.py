"""
Author:
    SUN-Hao
Description:
    GNSS model test

"""


import numpy as np
import matplotlib.pyplot as plt

from gnss_model import GNSSModel



if __name__=="__main__":
    gnss_model = GNSSModel()

    num = 100
    x = np.linspace(0, 2*np.pi, num=num)
    y = np.sin(x)

    x0 = 326474.4650588095
    y0 = 3392563.1296714176


    x_map = x*10 + x0
    y_map = y*3.5 + y0

    lons= []
    lats= []
    for i in range(num):
        lon, lat= gnss_model.run(x_map[i], y_map[i], 0)
        lons.append(lon)
        lats.append(lat)

    fig, ax = plt.subplots(1, 1)  # Create a figure and an axes.
    # ax[0].plot(data_manager.x,data_manager.y)
    #ax[0].plot(x_map, y_map)  # Plot some data on the axes.
    #ax[0].plot(env.x, env.y)  # Plot some data on the axes.

    ax.plot(lons,lats)
    # plt.xticks([])
    # plt.yticks([])
    plt.show()

