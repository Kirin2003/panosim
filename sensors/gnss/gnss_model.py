"""
Author:
    SUN-Hao
Description:
    GNSS model with Gauss noise

"""
import math
import numpy as np
from pyproj import Proj


class GNSSModel(object):
    def __init__(self,**kwargs):
        self.map_x0 = kwargs.get('map_x0', 326474.4650588095)
        self.map_y0 = kwargs.get('map_y0', 3392563.1296714176)
        self.map_z0 = kwargs.get('map_z0', 0)

        self.noise_lon_bias = kwargs.get('noise_lon_bias',0)
        self.noise_lon_stddev = kwargs.get('noise_lon_stddev', 0.000001)

        self.noise_lat_bias = kwargs.get('noise_lat_bias',0)
        self.noise_lat_stddev = kwargs.get('noise_lat_stddev', 0.000001)

        self.noise_alt_bias = kwargs.get('noise_alt_bias',0)
        self.noise_alt_stddev = kwargs.get('noise_alt_stddev', 0.1)

        self.noise_heading_bias = kwargs.get('noise_heading_bias',0)
        self.noise_heading_stddev = kwargs.get('noise_heading_stddev', 0.1)

        self.noise_velocity_bias = kwargs.get('noise_velocity_bias',0)
        self.noise_velocity_stddev = kwargs.get('noise_velocity_stddev', 0.1)

        self.noise_seed =0

    def _transform_utm_into_lat_lon(self,x, y, zone, hemisphere):
        h_north = False
        h_south = False
        if (hemisphere == 'N'):
            h_north = True
        elif (hemisphere == 'S'):
            h_south = True
        else:
            print("Unknown hemisphere: " + hemisphere)

        proj_in = Proj(proj='utm', zone=zone, ellps='WGS84', south=h_south, north=h_north, errcheck=True)
        lon, lat = proj_in(x, y, inverse=True)

        lon = math.floor(lon * 1000000) / 1000000
        lat = math.floor(lat * 1000000) / 1000000

        return lon, lat

    def _get_noise_data(self,bias,stddev,data):
        noise = np.random.normal(bias, stddev, 1)
        noise_data = data + noise
        return noise_data

    def run(self,x,y,z):
        lon, lat = self._transform_utm_into_lat_lon(x,y,51,'N')
        lon = self._get_noise_data(self.noise_lon_bias,self.noise_lon_stddev,lon)
        lat = self._get_noise_data(self.noise_lat_bias, self.noise_lat_stddev, lat)
        # TODO
        return lon,lat
