"""
Author:
    SUN-Hao
Description:
    GNSS model test

"""


import numpy as np
import matplotlib.pyplot as plt
import math
from pyproj import Proj


def transform_utm_into_lat_lon(x, y, zone, hemisphere):
    # verify the hemisphere
    h_north = False
    h_south = False
    if (hemisphere == 'N'):
        h_north = True
    elif (hemisphere == 'S'):
        h_south = True
    else:
        print("Unknown hemisphere: " + hemisphere)

    proj_in = Proj(proj='utm', zone=zone, ellps='WGS84', south=h_south, north=h_north, errcheck=True)

    lon, lat = proj_in(x, y, inverse=True)

    # just printing the floating point number with 6 decimal points will round it
    lon = math.floor(lon * 1000000) / 1000000
    lat = math.floor(lat * 1000000) / 1000000

    # lon = "%.8f" % lon
    # lat = "%.8f" % lat

    return lon, lat


if __name__=="__main__":
    lon,lat = transform_utm_into_lat_lon(326474.4650588095, 3392563.1296714176,51,'N')

    num = 100
    x = np.linspace(0, 2*np.pi, num=num)
    y = np.sin(x)

    x0 = 326474.4650588095
    y0 = 3392563.1296714176


    x_map = x*10 + x0
    y_map = y*3.5 + y0

    x_noise = np.copy(x_map) # 没有新的副本
    y_noise = np.copy(y_map)

    lons= []
    lats= []
    for i in range(num):
        x_noise[i] = np.random.normal(x_map[i], 0.05)
        y_noise[i] = np.random.normal(y_map[i], 0.05)
        lon, lat =transform_utm_into_lat_lon(x_noise[i], y_noise[i], 51, 'N')
        lons.append(lon)
        lats.append(lat)

    fig, ax = plt.subplots(1, 1)  # Create a figure and an axes.


    # ax[0].plot(data_manager.x,data_manager.y)
    ax.plot(x_map, y_map)  # Plot some data on the axes.
    ax.plot(x_noise, y_noise)  # Plot some data on the axes.

    fig1, ax1 = plt.subplots(1, 1)  # Create a figure and an axes.
    ax1.plot(lons,lats)
    # plt.xticks([])
    # plt.yticks([])
    plt.show()