from DataInterface import *
from DataInterfacePython import *
import numpy as np
import math
import os
import keyboard

from casadi import *

# 总线数据字符串格式
EGO_FORMAT = 'time@i,x@d,y@d,z@d,yaw@d,pitch@d,roll@d,speed@d'
TRAFFIC_FORM有AT = 'time@i,100@[,id@i,type@b,shape@i,x@d,y@d,z@d,yaw@d,pitch@d,roll@d,speed@d'
TRAFFIC_LIGHT = 'time@i,64@[,id@i,direction@b,state@b,timer@i'
EGO_TRAFFIC = 'time@i,lane@i,station@d,lateral@d,internal@b,nextJunction@i'
EGO_CONTROL = 'time@i,valid@b,throttle@d,brake@d,steer@d,mode@i,gear@i'
WARNING = 'time@i,type@b,64@[,text@b'


def warning(userData, level, length, warn):
    bus = userData['warning'].getBus()
    size = userData['warning'].getHeaderSize()
    bus[size:size + length] = ('{}').format(warn).encode()
    userData['warning'].writeHeader(userData['time'], level, length)

# 模型初始化完成
def ModelStart(userData):
    # 初始化总线数据
    # 参考文件中只给出激光雷达、mono相机、以及ego控制例程其他均为反编译获得
    # decompile from "Caeri_Judge_Traffic.pyc"
    busId = userData["busId"]
    userData['ego'] = BusAccessor(busId, 'ego', EGO_FORMAT)
    userData['traffic'] = BusAccessor(busId, 'traffic', TRAFFIC_FORMAT)
    userData['traffic_light'] = BusAccessor(busId, 'traffic_light', TRAFFIC_LIGHT)
    userData['ego_traffic'] = BusAccessor(busId, 'ego_traffic', EGO_TRAFFIC)
    userData["ego_control"] = BusAccessor(busId, "ego_control", EGO_CONTROL)
    userData['warning'] = BusAccessor(busId, 'warning', WARNING)

# 模型输出基础10ms一次循环(不包含算法时间)
def ModelOutput(userData):
    os.system('cls')  # 系统清屏
    currettime = userData["time"]  # 系统时间

    print('====================================================')
    print('Roshan model handler. add casadi')

    # 当前车辆信息(坐标,姿态角,速度)
    (ego_time, ego_x, ego_y, ego_z, ego_yaw, ego_pitch, ego_roll, ego_speed) = userData['ego'].readHeader()
    print('当前车辆位置 x:' + str(round(ego_x, 2)) + ' y:' + str(round(ego_y, 2)) + ' z:' + str(round(ego_z, 2)))
    print('当前车辆转角 yaw:' + str(round(ego_yaw, 2)) + ' pitch:' + str(round(ego_pitch, 2)) + ' roll:' + str(
        round(ego_roll, 2)))
    print('当前车辆速度 speed:' + str(round(ego_speed, 2)))

    # 当前交通信息(数量以及数组,物体信息包含行人以及车辆)
    (_, width) = userData['traffic'].readHeader()
    print('交通Object数量 width:' + str(width))

    # 遍历每一个对象(ID,类型,坐标,姿态角,速度)
    for i in range(width):
        if i > 5:
            continue
        (id, type, shape, x, y, z, yaw, pitch, roll, speed) = userData['traffic'].readBody(i)
        print('---->Object ' + str(i) + ': id:' + str(id) + ': type:' + str(type) + ': x:' + str(
            round(x, 2)) + ': y:' + str(round(y, 2)) + ': z:' + str(round(z, 2)) + ': speed:' + str(round(speed, 2)))
    print('...')

    # 当前车辆车道线信息(当前车道线ID不在为-1,下一个交汇ID,旁边车道ID)
    (ego_traffic_time, ego_traffic_lane, ego_traffic_station, ego_traffic_lateral, ego_traffic_internal, nextJunction) = \
    userData['ego_traffic'].readHeader()
    print('当前车辆所在车道id:' + str(ego_traffic_lane) + ' 下一交叉口id:' + str(nextJunction))

    # 车辆控制信息(油门刹车转角)
    (ts, valid, throttle, brake, steer, mode, gear) = userData["ego_control"].readHeader()
    print('当前车辆油门:' + str(throttle) + ' 刹车' + str(brake) + ' 转角' + str(steer))

    # 键盘控制车辆(copy keyboardControl.py)
    throttle = 0
    brake = 0
    steer = 0
    if keyboard.is_pressed('up'):
        brake = 0
        throttle = 0.3
    if keyboard.is_pressed('down'):
        throttle = 0
        brake = 10000000
    if keyboard.is_pressed('left'):
        steer = 3.14
    if keyboard.is_pressed('right'):
        steer = -3.14
    userData["ego_control"].writeHeader(*(currettime, 1, throttle, brake, steer, 1, 0))

    # 设置屏幕下方文字提示
    # level 0不含图标 1为黄色警告 2为红色警告
    warning(userData, 0, 28, 'Unkn0wnH4ck3r model handler.')

# 模型终止
def ModelTerminate(userData):
    pass
