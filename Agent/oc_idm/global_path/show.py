import matplotlib.pyplot as plt
import numpy as np

# x = [878.13, 860.59, 858.91, 856.91, 854.90, 852.89, 850.89, 848.88, 846.88, 834.22, 832.37, 830.57, 828.82, 827.10, 825.47, 823.94, 822.51, 821.21, 820.06, 819.06, 818.23, 817.58, 817.11, 816.83, 816.75, 817.00, 817.14, 817.44, 806.27, 769.32, 749.89, 713.61, 705.33, 705.37, 705.42, 705.46, 705.51, 705.55, 705.60, 705.64, 705.69, 705.74, 705.78, 705.83, 705.87, 705.92, 705.96, 706.01, 706.06, 706.10, 706.15, 706.19, 706.24, 706.28, 706.33, 706.37, 706.42, 706.47, 706.51, 706.56, 706.60, 706.65, 706.69, 707.51, 707.75, 708.51, 709.44]
# y = [492.43, 492.21, 492.19, 492.15, 492.11, 492.06, 492.00, 491.93, 491.85, 491.34, 491.38, 491.60, 492.02, 492.63, 493.41, 494.37, 495.48, 496.74, 498.13, 499.65, 501.26, 502.94, 504.69, 506.49, 508.34, 535.92, 551.60, 584.95, 596.19, 596.4, 596.51, 596.72, 602.80, 604.79, 606.78, 608.77, 610.76, 612.75, 614.73, 616.72, 618.71, 620.70, 622.69, 624.68, 626.67, 628.66, 630.65, 632.63, 634.62, 636.61, 638.60, 640.59, 642.58, 644.57, 646.56, 648.55, 650.53, 652.52, 654.51, 656.50, 658.49, 660.48, 662.47, 678.88, 725.64, 741.49, 782.2]
x = []
y = []

with open('全局路径/坐标点轨迹1.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'b')

x = []
y = []

with open('全局路径/坐标点轨迹2-0.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'r')

x = []
y = []
with open('全局路径/坐标点轨迹2-1.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'r')

x = []
y = []
with open('全局路径/坐标点轨迹2-2.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'r')

x = []
y = []
with open('全局路径/坐标点轨迹2-3.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'r')

x = []
y = []

with open('全局路径/坐标点轨迹3-0.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'g')

x = []
y = []

with open('全局路径/坐标点轨迹3-1.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'g')

x = []
y = []

with open('全局路径/坐标点轨迹3-2.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'g')

x = []
y = []

with open('全局路径/坐标点轨迹4-0.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'black')

x = []
y = []

with open('全局路径/坐标点轨迹4-1.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'black')

x = []
y = []

with open('全局路径/坐标点轨迹4-2.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'black')

x = []
y = []

with open('全局路径/坐标点轨迹4-3.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'black')

x = []
y = []

with open('全局路径/坐标点轨迹5-0.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'gold')

x = []
y = []

with open('全局路径/坐标点轨迹5-1.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'gold')

x = []
y = []

with open('全局路径/坐标点轨迹6-0.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'pink')

x = []
y = []

with open('全局路径/坐标点轨迹6-1.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'pink')

x = []
y = []

with open('全局路径/坐标点轨迹6-2.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'pink')

x = []
y = []

with open('全局路径/坐标点轨迹6-3.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'pink')

x = []
y = []

with open('全局路径/坐标点轨迹7-0.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'yellow')

x = []
y = []

with open('全局路径/坐标点轨迹7-1.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'yellow')


x = []
y = []

with open('全局路径/坐标点轨迹8.txt', 'r+', encoding='utf-8') as f:
     for line in f.readlines():
          pos = line[:-1].split(',')
          x.append(float(pos[0]))
          y.append(float(pos[1]))

plt.plot(x, y, 'm')

# plt.xlim((-50, 1000))
# plt.ylim((-50, 1000))

plt.show()