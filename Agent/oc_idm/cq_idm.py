"""
Author:
    SUN-Hao

Description:
    Optimal control based intelligent driver model(OCIDM) for Panosim host vehicle.

Input:

"""


import numpy as np
from oc_idm.idm_ctrller.idm_sacc import SAccIDM
from oc_idm.idm_ctrller.idm_lc import LCIDM
from oc_idm.idm_ctrller.utils import RefPathSensor

ctrl_init_flag = False
acc_idm =None
lc_idm = None


def init_idm():
    print("sun.hao----initializing idms----- ")
    mpc_dt = 0.2
    ctrl_size = 15
    pred_size = 15
    args = {'ctrl_horizion': ctrl_size,
            'pred_horizion': pred_size,
            'ts': mpc_dt,
            'print_info':False,
            'tv_num': 1}
    acc_idm = SAccIDM(**args)
    #------------------------------
    mpc_dt = 0.2
    ctrl_size = 15
    pred_size = 15
    args = {'ctrl_horizion': ctrl_size,
            'pred_horizion': pred_size,
            'ts': mpc_dt,
            'print_info': False,
            'tv_num': 3}
    lc_idm = LCIDM(**args)

    print("sun.hao------initializing ok!---------")
    return acc_idm, lc_idm


def read_map(map_name:str):
    pts=[]
    id = []
    x=[]
    y=[]
    with open(map_name, 'r+', encoding='utf-8') as f:
        for line in f.readlines():
            pos = line[:-1].split(',')
            x.append(float(pos[0]))
            y.append(float(pos[1]))

    h = []
    cnt = len(x)
    for i in range(cnt-1):
        dy = y[i+1] - y[i]
        dx = x[i + 1] - x[i]
        theta = np.arctan2(dy,dx)
        h.append(theta)
        id.append(i)
    h.append(h[-1]) # 计算差值来判断航向差别
    id.append(cnt-1)

    for i in range(cnt):
        pts.append([x[i],y[i],h[i],id[i]])

    return pts


if __name__=="__main__":
    print("Testing cq_idm!")
    lane1 = 'pts_1.txt'

    lane2_0 = 'pts_2-0.txt'
    lane2_1 = 'pts_2-1.txt'
    lane2_2 = 'pts_2-2.txt'
    lane2_3 = 'pts_2-3.txt'

    lane3_0 = 'pts_3-0.txt'
    lane3_1 = 'pts_3-1.txt'
    lane3_2 = 'pts_3-2.txt'

    lane4_0 = 'pts_4-0.txt'
    lane4_1 = 'pts_4-1.txt'
    lane4_2 = 'pts_4-2.txt'
    lane4_3 = 'pts_4-3.txt'

    lane5_0 = 'pts_5-0.txt'
    lane5_1 = 'pts_5-1.txt'

    lane6_0 = 'pts_6-0.txt'
    lane6_1 = 'pts_6-1.txt'
    lane6_2 = 'pts_6-2.txt'
    lane6_3 = 'pts_6-3.txt'

    lane7_0 = 'pts_7-0.txt'
    lane7_1 = 'pts_7-1.txt'

    lane8 = 'pts_8.txt'

    pts= read_map('global_path/pts_1.txt')

    print(pts)
    # print(len(x))
    # print(len(y))
    # print(len(h))
    #
    sensor = RefPathSensor(pts)
    nearest_id = sensor.get_nearest_pt_id(858.91, 492.19,-3.12)

    ref_path_global = sensor.get_ref_path_std(858.91, 492.19,-3.12)
    print(ref_path_global)
