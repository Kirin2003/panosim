"""
Author:
    SUN-Hao

Description:
    Interpolation functions
"""
import numpy as np


def getinterpolation(s1, s2, kp):
    var = (s2 - s1) * kp + s1
    return var

def getKp(s1, s2, s):
    kp = (s - s1) / (s2 - s1)
    return kp

def trans2DPt(old_x, old_y, new_in_old_x, new_in_old_y, new_from_old_theta):
    new_x = (old_x - new_in_old_x) * np.cos(new_from_old_theta) + \
            (old_y - new_in_old_y) * np.sin(new_from_old_theta)
    new_y = (old_y - new_in_old_y) * np.cos(new_from_old_theta) - \
            (old_x - new_in_old_x) * np.sin(new_from_old_theta)
    return new_x, new_y
