# -*- coding: UTF-8 -*-


"""
intelligent driver model(IDM) based on optimal control problems(OCPs)

Author:
    SUN-Hao

Descriptions:

~~~~~~~~~~~~~~~~~~
    IDM inputs:

    IDM outputs1:
        control series list [omega,ax]

    IDM outputs2:
        trajectory: list [x,y, theta, vel]

~~~~~~~~~~~~~~~~~~
    IDM vehicle model

    states:
        x,y,theta,vel

    control inputs:
        omega, ax

    step:
        x = x + ts * vel* cos(theta)
        y = y + ts * vel* sin(theta)
        theta = theta + ts * omega
        vel = vel + ts * ax
~~~~~~~~~~~~~~~~~~
"""

import casadi as ca
import numpy as np


class BaseOCPIDM():

    def __init__(self, **kwargs):
        self.vm_func = None
        self.solver = None
        self.__set_parameters_flag = False
        #-------------------------------------
        self.__set_parameters(**kwargs)

    def __set_parameters(self,**kwargs):
        # only way to change parameters
        self.__set_parameters_flag = True
        self.__ctrl_dim = 2  # omega ,ax,
        self.__state_dim = 4
        # default parameters
        self.__ts = 0.2
        self.__ctrl_horizon = 10
        self.__pred_horizon = 10

        self.__min_ax = -8
        self.__max_ax = 6
        self.__max_ay = 3

        self.__weight_comf = 1 # weight of motion comfort
        self.__weight_vel = 5 # weight of motion velocity
        self.__weight_acc = 1 # weight of tracking accuracy

        self.__veh_circle_num = 2 # each veh is represented by two circle
        # Important:  long vehicle(such as bus) equals several standard vehicle
        self.__support_veh_num = 1

        self.__safe_dist = 7.5 # collision check dist
        self.__check_horizon = 8

        # acc parameters
        self.__t_h = 2.0 #
        self.__safe_d0 = 10
        self.__max_t_h = 5.0 #
        self.__lane_width = 3.5

        self.__lon_ctrl_min_vel = 0.5
        self.__is_stop_dist = 5

        self.__aeb_dist = 1


        self.__print_info = True
        # user define parameters
        if 'print_info' in kwargs:
            self.__print_info = kwargs['print_info']

        if 'ctrl_horizion' in kwargs:
            self.__ctrl_horizon = kwargs['ctrl_horizion']
        if 'pred_horizion' in kwargs:
            self.__pred_horizon = kwargs['pred_horizion']
        if self.__ctrl_horizon > self.__pred_horizon:
            raise RuntimeError("control horizon is longer than predictive horizon!")
        if 'ts' in kwargs:
            self.__ts = kwargs['ts']

        if 'min_ax' in kwargs:
            self.__min_ax = kwargs['min_ax']
        if 'max_ax' in kwargs:
            self.__max_ax = kwargs['max_ax']
        if 'max_ay' in kwargs:
            self.__max_ay = kwargs['max_ay']

        if 'tv_num' in kwargs:
            self.__support_veh_num = kwargs['tv_num']
        self.__circle_num_step = self.__veh_circle_num * self.__support_veh_num
        # self.__circle_num_step = 4  # circle number in one predictive step, according to vehicle cnt and type
        # set circle centre
        self.__offset = [-2,2] #
        if len(self.__offset) != self.__veh_circle_num:
            raise RuntimeError("wrong circle num")

        # set vehicle model
        self.vm_func = self.__create_vehicle_model()
        # log all parameters
        if self.__print_info:
            self.print_IDM_parameters()

    def print_IDM_parameters(self):
        print('pred horizon:',self.__pred_horizon)
        print('ctrl horizon:',self.__ctrl_horizon)
        print('discrete time:',self.__ts)
        print('support vehicle:',self.__support_veh_num)
        print('circles in each step:',self.__circle_num_step)

    #----getters-------------------
    def get_lon_ctrl_min_vel(self):
        return self.__lon_ctrl_min_vel

    def get_weight_acc(self):
        return self.__weight_acc

    def get_weight_comf(self):
        return self.__weight_comf

    def get_weight_vel(self):
        return self.__weight_vel

    def get_max_ay(self):
        return self.__max_ay

    def get_min_ax(self):
        return self.__min_ax

    def get_max_ax(self):
        return self.__max_ax

    def get_check_horizon(self):
        return self.__check_horizon

    def get_ts(self):
        return self.__ts

    def get_safe_dist(self):
        return self.__safe_dist

    def get_ctrl_horizon(self):
        return self.__ctrl_horizon

    def get_pred_horizon(self):
        return self.__pred_horizon

    def get_ctrl_dim(self):
        return self.__ctrl_dim

    def get_state_dim(self):
        return self.__state_dim

    def get_circle_num_step(self):
        return self.__circle_num_step

    def __create_vehicle_model(self):
        ts = self.__ts
        # vehicle states
        x = ca.SX.sym('x')  # x坐标
        y = ca.SX.sym('y')  # y坐标
        theta = ca.SX.sym('theta')  # 车辆航向
        vel = ca.SX.sym('vel')  # 速度
        states = ca.vertcat(x, y, theta, vel)

        # control inputs
        omega = ca.SX.sym('omega')
        ax = ca.SX.sym('ax')
        inputs = ca.vertcat(omega, ax)

        # vehicle model  x(k+1) = x(k) + ts * f(x(k),u(k)), where x_dot = f(x,u)
        x_next = x + ts * vel * ca.cos(theta)
        y_next = y + ts * vel * ca.sin(theta)
        theta_next = theta + ts * omega
        vel_next = vel + ts * ax
        states_next = ca.vertcat(x_next, y_next, theta_next, vel_next)

        vm_func = ca.Function('vm_func', [states, inputs], [states_next], ['states', 'inputs'], ['states_next'])
        return vm_func

    def step(self,x_0,cmd):
        x_1 = np.zeros(5,dtype=np.float32)
        x_1[0] = x_0[0] + self.__ts * x_0[3] * np.cos( x_0[2])
        x_1[1] = x_0[1] + self.__ts * x_0[3] * np.sin( x_0[2])
        x_1[2] = x_0[2] + self.__ts * cmd[0]
        x_1[3] = x_0[3] + self.__ts * cmd[1]
        x_1[4] = x_0[4] + self.__ts # time
        return x_1

    def gen_traj(self,cmds,init_vel):
        x =  np.zeros(5,dtype=np.float32) # Note: trajectory time start from 0!
        x[3] = init_vel
        traj = []
        traj.append( x.tolist())
        for i in range(self.__ctrl_horizon):
            x = self.step(x,cmds[:,i])
            traj.append(x.tolist())
        return traj

    def gen_traj_stopping(self,init_vel):
        x =  np.zeros(5,dtype=np.float32) # Note: trajectory time start from 0!
        x[3] = init_vel
        traj = []
        traj.append( x.tolist())
        for i in range(self.__ctrl_horizon):
            x[4] =  x[4] + self.__ts
            traj.append(x.tolist())
        return traj

    def gen_traj_AEB(self,init_vel):
        x =  np.zeros(5,dtype=np.float32) # TODO
        x[3] = init_vel / 2
        traj = []
        traj.append( x.tolist())
        for i in range(self.__ctrl_horizon):
            x[4] =  x[4] + self.__ts
            traj.append(x.tolist())
        return traj

    def predict_const_vel(self,obs:dict):
        init_x = obs['x']
        init_y = obs['y']
        obs_preds = []
        for i in range(self.__pred_horizon):
            ds = (i+1)+ obs['v'] * self.__ts
            x = init_x + ds* np.cos(obs['h'])
            y = init_y + ds* np.sin(obs['h'])
            h = obs['h']
            pred = {'x':x,'y':y,'h':h,'step':i+1}
            obs_preds.append(pred)
        return obs_preds

    def gen_circle_centre(self,obs:dict):
        centres = list()
        for d in self.__offset:
            x_c = obs['x'] + d * np.cos(obs['h'])
            y_c = obs['y'] + d * np.sin(obs['h'])
            centres.append({'x':x_c,'y':y_c})
        return centres

    def gen_offset_ref_path(self,ref_x,ref_y,offset):
        h = np.zeros(self.__pred_horizon,dtype=np.float32)
        for i in range(1,self.__pred_horizon):
            dx = ref_x[i] - ref_x[i-1]
            dy = ref_y[i] - ref_y[i-1]
            h[i] = np.arctan2(dy,dx)  #retrun in the range [-pi, pi]
        h = h +np.pi/2
        x = ref_x + offset* np.cos(h)
        y = ref_y + offset* np.sin(h)
        return x,y

    def pred_env(self, env):
        raise RuntimeError('pred env in Base IDM')

    def gen_circle_matrix(self):
        raise RuntimeError('gen circle matrix in Base IDM')

    def build(self):
        raise RuntimeError('build in Base IDM')

    def run(self, vel, ref_x, ref_y, ref_vel,env=None):
        raise RuntimeError('run in Base IDM')

    def run0(self, inti_y, init_h, inti_vel):
        raise RuntimeError('run in Base IDM')

    def is_stop(self,dist,des_vel):
        """
        (1) des_vel: leader vehicle velocity
        (2) dist: distance between two vehicles
        """
        if dist < self.__is_stop_dist and des_vel < 0.1:
            return True
        return False

    def is_AEB(self,init_dist, hv_vel,tv_vel,max_acc):
        if hv_vel <= tv_vel:
            return False
        dist_stop = (hv_vel*hv_vel - tv_vel*tv_vel) / (2 * max_acc)
        if dist_stop > (init_dist + self.__aeb_dist): # stop distance is longer tha safe distance
            return True
        return False

    def get_leader_veh(self,ref_x,ref_y,ref_v,vel,obs=None):
        """
        return:
            (1) init_dist between host and leader vehicle
            (2) desired speed
            (3) desired distance
            (4) real distance
        """
        real_dist = None
        max_acc_dist = self.__max_t_h * vel + self.__safe_d0
        des_acc_dist = self.__t_h * vel + self.__safe_d0
        des_vel = ref_v

        if obs is None:
            init_dist = des_acc_dist # no obs
            real_dist = max_acc_dist
        else:
            tv_x = obs['x']
            tv_y = obs['y']
            cnt = self.__pred_horizon -1
            for i in range(self.__pred_horizon):
                dx = ref_x[i] - tv_x
                dy = ref_y[i] - tv_y
                dist = np.sqrt(dx * dx + dy * dy)  # distance between tv and current pt
                if dist < self.__lane_width: # if in the same lane position
                    cnt = i
                    break
            dist_on_ref = (cnt + 1) * self.__ts * vel
            dx = ref_x[cnt] - tv_x
            dy = ref_y[cnt] - tv_y
            dist_to_end = np.sqrt(dx * dx + dy * dy)  # distance between tv and current pt
            # print(dist_on_ref,dist_to_end)
            init_dist = dist_on_ref + dist_to_end
            real_dist = init_dist
            if init_dist > max_acc_dist:
                init_dist = max_acc_dist
            else:
                des_vel = obs['v']
        return init_dist , des_vel ,des_acc_dist, real_dist

    def get_acc_info(self,ref_x,ref_y,ref_v,vel,obs=None):
        """
        vel: current velocity
        return: ref_dist, ref_v
        """
        if obs is None:
            ref_dist = vel * 3 + 10
            return True, ref_dist, ref_dist, ref_v
        else:
            tv_x = obs['x']
            tv_y = obs['y']
            tv_vel = obs['v']
            dist_al = self.__pred_horizon * self.__ts * vel # distance along lane
            on_ref =  False
            for i in range(self.__pred_horizon):
                dx = ref_x[i] - tv_x
                dy = ref_y[i] - tv_y
                dist = np.sqrt(dx*dx + dy*dy) # distance between tv and current pt
                if dist < np.sqrt(self.__safe_dist):
                    dist_al = (i + 1)* self.__ts * vel
                    on_ref = True
                    break
            if not on_ref:
                dx = ref_x[-1] - tv_x
                dy = ref_y[-1] - tv_y
                dist_to_end = np.sqrt(dx * dx + dy * dy)  # distance between tv and current pt
                dist_al = dist_al + dist_to_end
            #--------------------------
            tv_stop_dist = tv_vel * tv_vel / 2 / np.fabs(self.__min_ax)
            ego_stop_dist = vel * vel / 2 / np.fabs(self.__min_ax)
            if dist_al + tv_stop_dist + 2 < ego_stop_dist :  # 3 is vehicle length
                print("AEB is active!")
                return False, 0 ,0
            else:
                ref_dist = vel * 3 + 10
                if dist_al > ref_dist:
                    return True,dist_al,ref_dist,tv_vel # virtual ahead vehicle
                else:
                    return True,dist_al,ref_dist,tv_vel


    def get_ref_path(self,state:dict,lane_shape:np.ndarray,in_ds:float,in_pt_num:int): # lane_shape [[x0,y0],[x1,y]....  [xn,yn]]  type: np.array
        lane_x ,lane_y = self.trans2DPt(lane_shape[:, 0], lane_shape[:, 1], state['x'] ,state['y'], state['h'])
        pts = lane_x.shape[0]
        start_index = 0
        for i in range(pts):
            if lane_x[i]> 0:
                start_index = i
                break

        if start_index < 1:
            RuntimeError("vehicle not on lane, ERROR")

        kp = self.getKp(s1=lane_x[start_index-1],s2=lane_x[start_index],s=0)
        start_y = self.getinterpolation(s1=lane_y[start_index-1],s2=lane_y[start_index],kp=kp)

        path_x = list()
        path_y = list()
        path_s = list()

        # add the first point
        path_x.append(0)
        path_y.append(start_y)
        path_s.append(0)

        ref_path_s = in_ds*in_pt_num
        for i in range(start_index,pts):
            tmp_x = lane_x[i]
            tmp_y = lane_y[i]

            dx = tmp_x - path_x[-1]
            dy = tmp_y - path_y[-1]
            ds = np.sqrt(dx * dx+ dy*dy)
            tmp_s = path_s[-1] +ds
            # add pt
            path_x.append(tmp_x)
            path_y.append(tmp_y)
            path_s.append(tmp_s)

            if tmp_s > ref_path_s:
                break

        # get local ref path by ds
        cnt = len(path_x)
        local_path_x=[]
        local_path_y=[]
        for i in range(in_pt_num):
            s = (i+1) * in_ds
            if s<path_s[0] or s>path_s[-1]:
                raise RuntimeError("#err of RefPathSensor#,Get end of the reference map")
            for j in range(cnt-1):
                if s>path_s[j] and s<path_s[j+1]:
                    kp = self.getKp(path_s[j],path_s[j+1],s)
                    local_path_x.append(self.getinterpolation(path_x[j],path_x[j+1],kp))
                    local_path_y.append(self.getinterpolation(path_y[j], path_y[j + 1], kp))
        return local_path_x,local_path_y

    # util  functions
    def getinterpolation(self,s1, s2, kp):
        var = (s2 - s1) * kp + s1
        return var

    def getKp(self,s1, s2, s):
        kp = (s - s1) / (s2 - s1)
        return kp

    def trans2DPt(self,old_x, old_y, new_in_old_x, new_in_old_y, new_from_old_theta):
        new_x = (old_x - new_in_old_x) * np.cos(new_from_old_theta) + \
                (old_y - new_in_old_y) * np.sin(new_from_old_theta)
        new_y = (old_y - new_in_old_y) * np.cos(new_from_old_theta) - \
                (old_x - new_in_old_x) * np.sin(new_from_old_theta)
        return new_x, new_y

