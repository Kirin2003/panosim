"""
ss:
    single static vehicle without prediction

"""
import casadi as ca
import numpy as np
from oc_idm.idm_ctrller.idm_base import BaseOCPIDM


class DynamicIDM(BaseOCPIDM):
    def __init__(self, **kwargs):
        super().__init__( **kwargs)

        self.__circle_x = np.zeros((self.get_pred_horizon(),self.get_circle_num_step()),dtype=np.float32)
        self.__circle_y = np.zeros((self.get_pred_horizon(),self.get_circle_num_step()), dtype=np.float32)
        self.__check_start  = 1
        # build graph
        self.__build()

    def __build(self):
        # define ctrl cmd vars
        # cmd[0,] = omega, cmd[1,] = ax
        cmd = ca.SX.sym('U', self.get_ctrl_dim(), self.get_ctrl_horizon())

        #  ----------------------
        p_init_v = ca.SX.sym('inti_v')
        p_ref_x = ca.SX.sym('ref_x', self.get_pred_horizon())
        p_ref_y = ca.SX.sym('ref_y', self.get_pred_horizon())
        p_ref_v = ca.SX.sym('ref_v')
        # set init states,i.e. vel = p0
        states = ca.SX.zeros(4)
        states[3] = p_init_v[0]
        # calculate trajectory
        trajectory = []
        for i in range(self.get_pred_horizon()):
            if i < self.get_ctrl_horizon():
                inputs = cmd[:, i]
            else:
                inputs = cmd[:, -1]

            res = self.vm_func(states=states, inputs=inputs)
            states = res['states_next']
            trajectory.append(states)
        # calculate objective function
        obj = 0
        for i in range(self.get_ctrl_horizon()):
            obj_omega = cmd[0, i] * cmd[0, i]
            obj_ax = cmd[1, i] * cmd[1, i]
            obj = obj + self.get_weight_comf() * (obj_omega + obj_ax)

        for i in range(self.get_pred_horizon()):
            obj_x = (trajectory[i][0] - p_ref_x[i]) ** 2
            obj_y = (trajectory[i][1] - p_ref_y[i]) ** 2
            obj_vel = (trajectory[i][3] - p_ref_v) ** 2
            obj = obj + self.get_weight_acc() * (obj_y + obj_x) + self.get_weight_vel() * obj_vel

        #----add constraints for x--------------
        self.lbx = list()
        self.ubx  = list()
        for i in range(self.get_ctrl_horizon()):
            self.lbx.append(-ca.inf)
            self.ubx.append(ca.inf)

        for i in range(self.get_ctrl_horizon()):
            self.lbx.append(-8)
            self.ubx.append(6)


        #----add constraints for gx--------------
        self.lbg = list()
        self.ubg  = list()
        g = list()

        #--------------------------------------
        for i in range(self.get_pred_horizon()):
            g.append(trajectory[i][3])  # v boundary
            self.lbg.append(0.01)
            self.ubg.append(ca.inf)

        #------safe constraints---------------
        p_safety_x = list()
        p_safety_y = list()
        for j in range(self.get_circle_num_step()):
            p_centre_x = ca.SX.sym('centre_x', self.get_check_horizon() -self.__check_start )
            p_centre_y = ca.SX.sym('centre_y', self.get_check_horizon() -self.__check_start)

            p_safety_x.append(p_centre_x)
            p_safety_y.append(p_centre_y)
            for i in range(self.__check_start,self.get_check_horizon()): # for each step with safe constraints
                dist = (trajectory[i][0] - p_centre_x[i-self.__check_start])**2  + \
                       (trajectory[i][1] - p_centre_y[i-self.__check_start]) ** 2 # dist between two circles
                g.append(dist)
                self.lbg.append(self.get_safe_dist())
                self.ubg.append(ca.inf)
            #-------------------------------------------------
            # dist = (trajectory[4][0] - p_centre_x[0]) ** 2 + \
            #        (trajectory[4][1] - p_centre_y[0]) ** 2  # dist between two circles
            # g.append(dist)
            # self.lbg.append(self.get_safe_dist())
            # self.ubg.append(ca.inf)


        # ---------------------------
        opts_setting = {
            'ipopt.max_iter': 100,
            'ipopt.print_level': 0,
            'print_time': 0,
            'ipopt.acceptable_tol': 1e-6,
            'ipopt.acceptable_obj_change_tol': 1e-4
        }

        # ---------------------------
        P = list()
        P.append(p_init_v)
        P.append(p_ref_x)
        P.append(p_ref_y)
        P.append(p_ref_v)
        P.extend(p_safety_x)
        P.extend(p_safety_y)

        nlp_prop = {
            'f': obj,
            'x': ca.reshape(cmd, -1, 1),
            'p': ca.vertcat(*P),
            'g':ca.vertcat(*g)
        }
        # ---------------------------
        self.solver = ca.nlpsol('solver', 'ipopt', nlp_prop, opts_setting)

    def pred_env(self, env):
        col = 0
        for obs in env:
            obs_preds = self.predict_const_vel(obs)
            centres_num = 0
            for i in range(self.get_pred_horizon()):
                centres = self.gen_circle_centre(obs_preds[i])
                centres_num = len(centres)
                for j in range(centres_num):
                    self.__circle_x[i, col + j] = centres[j]['x']
                    self.__circle_y[i, col + j] = centres[j]['y']
            col += centres_num
        # -----------environment prediction end-------------------------------


    def run(self, vel, ref_x, ref_y, ref_vel,env=None):
        if env is None:
            raise RuntimeError("error env=None")
        if np.size(vel) != 1:
            raise RuntimeError("error vel")
        if np.size(ref_x) != self.get_pred_horizon():
            raise RuntimeError("error ref x")
        if np.size(ref_y) != self.get_pred_horizon():
            raise RuntimeError("error ref y")
        if np.size(ref_vel) != 1:
            raise RuntimeError("error ref vel")
        # if np.size(env) != 2:
        #     raise RuntimeError("error env")

        self.pred_env(env)

        vel = np.reshape(vel, (-1, 1))
        ref_x = np.reshape(ref_x, (-1, 1))
        ref_y = np.reshape(ref_y, (-1, 1))
        ref_vel = np.reshape(ref_vel, (-1, 1))

        #  slices
        check_circle_x = self.__circle_x[self.__check_start:self.get_check_horizon(),:]
        check_circle_y = self.__circle_y[self.__check_start:self.get_check_horizon(), :]
        circle_x = np.reshape(check_circle_x, (-1, 1))
        circle_y = np.reshape(check_circle_y, (-1, 1))

        init_u = np.zeros(self.get_ctrl_horizon() * self.get_ctrl_dim())
        p = np.concatenate((vel, ref_x, ref_y, ref_vel, circle_x, circle_y))

        #------------------------
        for i in range(self.get_ctrl_horizon()):
            self.lbx[i] = (-self.get_max_ay()/vel)
            self.ubx[i] = (self.get_max_ay()/vel)

        res = self.solver(x0=init_u, p=p,lbg=self.lbg,ubg=self.ubg,lbx=self.lbx,ubx=self.ubx)
        cmds = np.array(res['x'])
        # Note res['x'] is casadi.DM change to np: np.array(res['x'])
        u_sol = ca.reshape(res['x'], self.get_ctrl_dim(), self.get_ctrl_horizon())
        u_sol_np = np.array(u_sol)
        traj = self.gen_traj(u_sol_np, vel)
        return cmds, traj,check_circle_x,check_circle_y
