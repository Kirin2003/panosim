# -*- coding: UTF-8 -*-


"""
intelligent driver model(IDM) based on optimal control problems(OCPs)

Author: SUN-Hao

~~~~~~~~~~~~~~~~~~
IDM inputs:


IDM outputs:


IDM outputs1:
    control series list [omega,ax]

IDM outputs2:
    trajectory: list [x,y, theta, vel]

~~~~~~~~~~~~~~~~~~
IDM vehicle model

states:
    x,y,theta,vel

control inputs:
    omega, ax


step:
    x = x + ts * vel* cos(theta)
    y = y + ts * vel* sin(theta)
    theta = theta + ts * omega
    vel = vel + ts * ax


~~~~~~~~~~~~~~~~~~
"""
import casadi as ca
import numpy as np


class VehicleConf():

    def __init__(self,x=0,y=0,yaw=0,vel=0,t=0):
        self.x = x
        self.y = y
        self.yaw = yaw
        self.vel = vel
        self.t = t

    def __add__(self, delta):
        '''此方法用来制定self + other的规则'''

        x = self.x + delta.x
        y = self.y + delta.y
        yaw = self.yaw + delta.yaw
        vel = delta.vel
        t = delta.t
        return VehicleConf(x=x,y=y,yaw=yaw,vel=vel,t=t)

import rsh_math as mt


class Actuator():

    def __init__(self):
        self.trajectory = None # tpe: #list[VehicleConf]
        self.init_conf = None # type: #VehicleConf
        self.tran_x = 0
        self.tran_y = 0
        self.tran_yaw = 0

    def reset(self):
        self.trajectory = None
        self.init_conf = None

    def set_new_conditions(self,traj,conf):
        # 有了新的控制命令，就设置新的条件, 新轨迹和
        self.__set_traj(traj) # Note start from x =0, y = 0, yaw = 0
        self.__set_init_conf(conf)
        #print traj

    def __set_traj(self,traj):
        self.trajectory = traj  # in local vehicle frame (right hand z-up)

    def __set_init_conf(self,conf):
        self.init_conf = conf # in global frame (right hand z-up)
        self.tran_x,self.tran_y = mt.trans2DPt(0,0,self.init_conf.x,self.init_conf.y,self.init_conf.yaw)
        self.tran_yaw = -self.init_conf.yaw
        # print self.init_conf.x,self.init_conf.y
        # print mt.trans2DPt(0, 0, self.tran_x, self.tran_y, self.tran_yaw)

    def get_conf(self,current_time):
        delta_cfg = self.__get_delta_conf(current_time)
        x,y = mt.trans2DPt(delta_cfg.x,delta_cfg.y, self.tran_x,self.tran_y,self.tran_yaw)
        cfg = VehicleConf(x=x,y=y,
                    yaw=delta_cfg.yaw+self.init_conf.yaw,
                    vel=delta_cfg.vel,
                    t=delta_cfg.t + self.init_conf.t)

        return cfg


    def __get_delta_conf(self,current_time):
        dt = current_time - self.init_conf.t
        # Note: dt 的时间，是从set_new_condition的timestamp 开始算，单位：second
        # if self.previous_dt > dt:
        #     raise RuntimeError('ERROR,current dt is smaller than previous dt')
        # else:
        #     self.previous_dt = dt
        #  check trajectory
        if self.trajectory is None:
            raise RuntimeError('ERROR, no trajectory')
        # check initial config
        if self.init_conf is None:
            raise RuntimeError('ERROR, no init config')
        # check dt
        if dt < 0:
            raise RuntimeError('ERROR, dt is negative')

        # if dt < 0.0001:
        #     print('dt is small return init config')
        #     return self.init_conf

        # check trajectory length
        traj_len = len(self.trajectory)
        if traj_len <= 1:
            raise RuntimeError('ERROR, trajectory only has one waypoint')
        #check trajectory timestamp
        if dt > self.trajectory[-1][4]:
            raise RuntimeError('WARNING,dt is larger than the trajectory')
            #return self.init_conf+ self.trajectory[-1] # global frame (right hand z-up)
        # normal
        for i in range(traj_len-1):
            if dt < 0.0001:
                kp = 0
                d_x = mt.getinterpolation(self.trajectory[i][0],self.trajectory[i+1][0],kp)
                d_y = mt.getinterpolation(self.trajectory[i][1], self.trajectory[i + 1][1], kp)
                d_yaw = mt.getinterpolation(self.trajectory[i][2], self.trajectory[i + 1][2], kp)
                d_vel = mt.getinterpolation(self.trajectory[i][3], self.trajectory[i + 1][3], kp)
                d_t = mt.getinterpolation(self.trajectory[i][4], self.trajectory[i + 1][4], kp)
                delta_cfg = VehicleConf(d_x,d_y,d_yaw,d_vel,d_t)
                return delta_cfg #
            if dt > self.trajectory[i][4] and dt < self.trajectory[i+1][4] :
                kp = mt.getKp(self.trajectory[i][4],self.trajectory[i+1][4],dt)
                d_x = mt.getinterpolation(self.trajectory[i][0],self.trajectory[i+1][0],kp)
                d_y = mt.getinterpolation(self.trajectory[i][1], self.trajectory[i + 1][1], kp)
                d_yaw = mt.getinterpolation(self.trajectory[i][2], self.trajectory[i + 1][2], kp)
                d_vel = mt.getinterpolation(self.trajectory[i][3], self.trajectory[i + 1][3], kp)
                d_t = mt.getinterpolation(self.trajectory[i][4], self.trajectory[i + 1][4], kp)
                delta_cfg = VehicleConf(d_x,d_y,d_yaw,d_vel,d_t)
                return delta_cfg #
        # unknown condition
        raise RuntimeError('ERROR,unknown conditions')


class OCPIDM():

    def __init__(self, **kwargs):
        # default parameters
        self.ctrl_dim = 2  # omega ,ax
        self.state_dim = 4

        # get all parameters
        self.w_acc = 1
        self.w_comf = 5
        self.w_vel = 0.5

        self.ctrl_horizon = kwargs['ctrl_horizion']
        self.pred_horizon = kwargs['pred_horizion']
        if self.ctrl_horizon > self.pred_horizon:
            raise RuntimeError("control horizon is longer than predictive horizion!")

        self.ts = kwargs['ts']
        # set vehicle model
        self.vm_func = self.__create_vehicle_model()
        # build graph
        self.__build()

    def __build_static_env(self):
        cmd = ca.SX.sym('U', self.ctrl_dim, self.ctrl_horizon)

        #  para[0] = vel
        #  para[1:self.pred_horizon+1] =  ref_x
        #  para[self.pred_horizon+1:1+ 2* self.pred_hoizon] = ref_y
        #  para[1+ 2* self.pred_horizon] = ref_vel
        p_static_env = ca.SX.sym('p', 1 + 2 * self.pred_horizon + 1 + 2) #
        #

        # set init states,i.e. vel = p0
        states = ca.SX.zeros(4)
        states[3] = p_static_env[0]

        # calculate trajectory
        trajectory = []
        for i in range(self.pred_horizon):
            if i < self.ctrl_horizon:
                inputs = cmd[:, i]
            else:
                inputs = cmd[:, -1]
            res = self.vm_func(states=states, inputs=inputs)
            states = res['states_next']
            trajectory.append(states)

        # calculate objective function
        obj = 0
        for i in range(self.ctrl_horizon):
            obj_omega = cmd[0, i] * cmd[0, i]
            obj_ax = cmd[1, i] * cmd[1, i]
            obj = obj + self.w_comf * (obj_omega + obj_ax)

        for i in range(self.pred_horizon):
            obj_x = (trajectory[i][0] - p_static_env[1 + i]) **2
            obj_y = (trajectory[i][1] - p_static_env[1 + self.pred_horizon + i]) **2
            obj_vel = (trajectory[i][3] - p_static_env[1+ 2* self.pred_horizon ]) ** 2
            obj = obj + self.w_acc * (obj_y + obj_x) + self.w_vel*obj_vel

        #
        g = list()
        for i in range(self.pred_horizon):
            dist = (trajectory[i][0] - p_static_env[1 + 2 * self.pred_horizon + 1])**2 \
                + (trajectory[i][1] - p_static_env[1 + 2 * self.pred_horizon + 2]) ** 2
            g.append(dist)

        #  set nlp solver
        opts_setting = {
            'ipopt.max_iter': 100,
            'ipopt.print_level': 0,
            'print_time': 0,
            'ipopt.acceptable_tol': 1e-6,
            'ipopt.acceptable_obj_change_tol': 1e-4
        }

        #---------------------------
        nlp_prop = {
            'f': obj,
            'x': ca.reshape(cmd, -1, 1),
            'p': p_static_env,
            'g':ca.vertcat(*g)
        }
        self.solver = ca.nlpsol('solver', 'ipopt', nlp_prop, opts_setting)

    def __build(self):
        # define ctrl cmd vars
        # cmd[0,] = omega, cmd[1,] = ax
        cmd = ca.SX.sym('U', self.ctrl_dim, self.ctrl_horizon)
        #  para[0] = vel
        #  para[1:self.pred_horizon+1] =  ref_x
        #  para[self.pred_horizon+1:1+ 2* self.pred_hoizon] = ref_y
        #  para[1+ 2* self.pred_horizon] = ref_vel
        para = ca.SX.sym('p', 1 + 2 * self.pred_horizon + 1) #

        # set init states,i.e. vel = p0
        states = ca.SX.zeros(4)
        states[3] = para[0]

        # calculate trajectory
        trajectory = []
        for i in range(self.pred_horizon):
            if i < self.ctrl_horizon:
                inputs = cmd[:, i]
            else:
                inputs = cmd[:, -1]

            res = self.vm_func(states=states, inputs=inputs)
            states = res['states_next']
            trajectory.append(states)

        # calculate objective function
        obj = 0
        for i in range(self.ctrl_horizon):
            obj_omega = cmd[0, i] * cmd[0, i]
            obj_ax = cmd[1, i] * cmd[1, i]
            obj = obj + self.w_comf * (obj_omega + obj_ax)

        for i in range(self.pred_horizon):
            obj_x = (trajectory[i][0] - para[1 + i]) **2
            obj_y = (trajectory[i][1] - para[1 + self.pred_horizon + i]) **2
            obj_vel = (trajectory[i][3] - para[1+ 2* self.pred_horizon ]) ** 2
            obj = obj + self.w_acc * (obj_y + obj_x) + self.w_vel*obj_vel

        opts_setting = {
            'ipopt.max_iter': 100,
            'ipopt.print_level': 0,
            'print_time': 0,
            'ipopt.acceptable_tol': 1e-6,
            'ipopt.acceptable_obj_change_tol': 1e-4
        }
        #---------------------------
        nlp_prop = {
            'f': obj,
            'x': ca.reshape(cmd, -1, 1),
            'p': para
        }
        self.solver = ca.nlpsol('solver', 'ipopt', nlp_prop, opts_setting)

    def __create_vehicle_model(self):
        ts = self.ts
        # vehicle states
        x = ca.SX.sym('x')  # x坐标
        y = ca.SX.sym('y')  # y坐标
        theta = ca.SX.sym('theta')  # 车辆航向
        vel = ca.SX.sym('vel')  # 速度
        states = ca.vertcat(x, y, theta, vel)

        # control inputs
        omega = ca.SX.sym('omega')
        ax = ca.SX.sym('ax')
        inputs = ca.vertcat(omega, ax)

        # vehicle model  x(k+1) = x(k) + ts * f(x(k),u(k)), where x_dot = f(x,u)
        x_next = x + ts * vel * ca.cos(theta)
        y_next = y + ts * vel * ca.sin(theta)
        theta_next = theta + ts * omega
        vel_next = vel + ts * ax
        states_next = ca.vertcat(x_next, y_next, theta_next, vel_next)

        vm_func = ca.Function('vm_func', [states, inputs], [states_next], ['states', 'inputs'], ['states_next'])
        return vm_func

    def __step(self,x_0,cmd):
        x_1 = np.zeros(5,dtype=np.float32)
        x_1[0] = x_0[0] + self.ts * x_0[3] * np.cos( x_0[2])
        x_1[1] = x_0[1] + self.ts * x_0[3] * np.sin( x_0[2])
        x_1[2] = x_0[2] + self.ts * cmd[0]
        x_1[3] = x_0[3] + self.ts * cmd[1]
        x_1[4] = x_0[4] + self.ts # time
        return x_1

    def __gen_traj(self,cmds,init_vel):
        x =  np.zeros(5,dtype=np.float32)
        x[3] = init_vel
        traj = []
        traj.append( x.tolist())
        for i in range(self.ctrl_horizon):
            x = self.__step(x,cmds[:,i])
            traj.append(x.tolist())
        return traj


    def run(self, vel, ref_x, ref_y, ref_vel):
        if np.size(vel) != 1:
            raise RuntimeError("error vel")
        if np.size(ref_x) != self.pred_horizon:
            raise RuntimeError("error ref x")
        if np.size(ref_y) != self.pred_horizon:
            raise RuntimeError("error ref y")
        if np.size(ref_vel) != 1:
            raise RuntimeError("error ref vel")


        vel = np.reshape(vel, (-1, 1))
        ref_x = np.reshape(ref_x, (-1, 1))
        ref_y = np.reshape(ref_y, (-1, 1))
        ref_vel = np.reshape(ref_vel, (-1, 1))

        init_u = np.zeros(self.ctrl_horizon * self.ctrl_dim)
        p = np.concatenate((vel, ref_x, ref_y,ref_vel))

        res = self.solver(x0=init_u, p=p)
        cmds = np.array(res['x'])
        # Note res['x'] is casadi.DM change to np: np.array(res['x'])
        u_sol = ca.reshape(res['x'], self.ctrl_dim, self.ctrl_horizon)
        u_sol_np = np.array(u_sol)
        traj = self.__gen_traj(u_sol_np,vel)
        return cmds,traj




if __name__ == "__main__":
    args = {'ctrl_horizion': 2,
            'pred_horizion': 2,
            'ts': 0.2}

    # idm = OCPIDM(**args)
    # idm.build()
    # print(idm.solver)
    init_u = np.zeros(3)
    print(init_u.shape)
