#  场景描述：step1根据主车的时刻、位置、速度触发发车模型；
#          step2基于主车的位置发车：纵向距离、横向默认具体为车道线宽度左右方向根据发车lane决定、发车速度、车辆类型、触发车道
#          step3执行变道动作：满足什么条件下执行换道、换道后车辆速度变化

import math
from TrafficModelInterface import *


def ModelStart(userData):
    userData["disturbEgoTime"],userData["disturbEgoStation"],userData["disturbEgoSpeed"],\
    userData["disturbVehicleType"],userData["disturbDepartLane"],userData["disturbDepartS"], userData["disturbDepartSpeed"],\
    userData["disturbOccurTime"],userData["disturbOccurDistance"],userData["disturbFinishSpeed"],userData["changeSpeedDuration"] \
        = map(float, userData["parameters"]["Parameters"].split(','))
    #3000,150,45,1,-1,10,8,6000,10,4,2

    userData["phase"] = 0
    print("初始化OK")


def ModelOutput(userData, true=1):

    # 根据条件发车
    if userData["phase"] == 0:

        # 组合判断条件  发车
        if userData["time"] >= userData["disturbEgoTime"] or getTotalDistance(0) >= userData["disturbEgoStation"] or getVehicleSpeed(0) >= userData["disturbEgoSpeed"]:
            print("发车触发OK")
            print(getTotalDistance(0))

            userData["phase"] += 1

            #具体触发类型：
            if userData["time"] >= userData["disturbEgoTime"]:
                print("时间触发OK")

            if getTotalDistance(0) >= userData["disturbEgoStation"]:
                print("距离触发OK")

            if getVehicleSpeed(0) >= userData["disturbEgoSpeed"]:
                print("速度触发OK")


            if userData["disturbDepartLane"] == -1:
                if userData["disturbVehicleType"] == 0:
                    userData["newId"] = addVehicleRelated(0,  userData["disturbDepartS"],0.0,
                                                          userData["disturbDepartSpeed"], lane_type.left,
                                                          vehicle_type.Car, )

                if userData["disturbVehicleType"] == 1:
                    userData["newId"] = addVehicleRelated(0,  userData["disturbDepartS"],0.0,
                                                          userData["disturbDepartSpeed"], lane_type.left,
                                                          vehicle_type.Van, )
                if userData["disturbVehicleType"] == 2:
                    userData["newId"] = addVehicleRelated(0,  userData["disturbDepartS"],0.0,
                                                          userData["disturbDepartSpeed"], lane_type.left,
                                                          vehicle_type.Bus, )

                if userData["disturbVehicleType"] == 3:
                    userData["newId"] = addVehicleRelated(0,  userData["disturbDepartS"],0.0,
                                                          userData["disturbDepartSpeed"], lane_type.left,
                                                          vehicle_type.OtherVehicle, )
                print("发车lane设置OK")

            if userData["disturbDepartLane"] == 1:
                if userData["disturbVehicleType"] == 0:
                    userData["newId"] = addVehicleRelated(0, userData["disturbDepartS"], 0.0,
                                                          userData["disturbDepartSpeed"], lane_type.left,
                                                          vehicle_type.Car, )

                if userData["disturbVehicleType"] == 1:
                    userData["newId"] = addVehicleRelated(0,  userData["disturbDepartS"],0.0,
                                                          userData["disturbDepartSpeed"], lane_type.left,
                                                          vehicle_type.Van, )
                if userData["disturbVehicleType"] == 2:
                    userData["newId"] = addVehicleRelated(0,  userData["disturbDepartS"],0.0,
                                                          userData["disturbDepartSpeed"], lane_type.left,
                                                          vehicle_type.Bus, )

                if userData["disturbVehicleType"] == 3:
                    userData["newId"] = addVehicleRelated(0,  userData["disturbDepartS"],0.0,
                                                          userData["disturbDepartSpeed"], lane_type.left,
                                                          vehicle_type.OtherVehicle, )
                print("发车lane设置OK")

    # 根据条件触发变道   时间  距离
    if userData["phase"] == 1: 
        
        hostX = getVehicleX(0)
        hostY = getVehicleY(0)
        addX = getVehicleX(userData["newId"])
        addY = getVehicleY(userData["newId"])
        distanceBetweenEgoDisturb = math.pow((hostX - addX),2) + math.pow((hostY - addY),2)
        distanceBetweenEgoDisturb = math.sqrt(distanceBetweenEgoDisturb)

        #如果满足条件车辆执行变道动作  否则继续保持发车速度运行
        if userData["time"] >= userData["disturbOccurTime"] or distanceBetweenEgoDisturb >= userData["disturbOccurDistance"]:
            userData["phase"] += 1
            userData["disturbCurrentTime"] = userData["time"]

            if userData["disturbDepartLane"] == -1:
                changeLane(userData["newId"], change_lane_direction.right)

            if userData["disturbDepartLane"] == 1:
                changeLane(userData["newId"], change_lane_direction.left)

            print("换道设置OK")

        else:
            changeSpeed(userData["newId"],userData["disturbDepartSpeed"],0.0)

    # 在时间周期内改变速度
    if userData["phase"] == 2:
        changeSpeed(userData["newId"], userData["disturbDepartSpeed"], 0.0)

        #基于换道时间3000ms判断是否变速  多久后将速度修正为预期值
        if userData["time"] - userData["disturbCurrentTime"] >= 3000:

            userData["phase"] += 1

            if userData["disturbFinishSpeed"] >= 0:
                changeSpeed(userData["newId"], userData["disturbFinishSpeed"],userData["changeSpeedDuration"])

                print("改变速度设置OK")

            else:
                #afterCutinSpeed = 0.0
                changeSpeed(userData["newId"], 0.0,userData["changeSpeedDuration"])
                #changeSpeed(userData["newId"], 0, serData["changeSpeedDuration"])
                print("改变速度设置OK")

            userData["changeSpeedOccurTime"] = userData["time"]

    # 车辆变速后  维持变速后的车速继续行驶
    if userData["phase"] == 3:
        if userData["time"] - userData["changeSpeedOccurTime"] >= userData["changeSpeedDuration"]:
            if userData["disturbFinishSpeed"] >= 0:
                changeSpeed(userData["newId"],userData["disturbFinishSpeed"],0.0)
            else:
                changeSpeed(userData["newId"], 0.0, 0.0)

def ModelTerminate(userData):
    pass