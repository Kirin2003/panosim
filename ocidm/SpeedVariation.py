
from TrafficModelInterface import *

def ModelStart(userData):
    userData["disturbEgoTime"],userData["disturbEgoStation"],userData["disturbEgoSpeed"],\
    userData["disturbVehicleType"],userData["disturbDepartS"],userData["disturbDepartLat"],\
    userData["disturbDepartSpeedLow"],userData["disturbDepartSpeedHigh"],userData["changeSpeedDuration"]\
        =map(float,userData["parameters"]["Parameters"].split(','))

    #3000,150,45,2,10,0,10,15,2


    userData["phase"] = 0
    print("初始化OK")

# 自己定义随机数
def myrandint(start,end,seed):
    a = 32310901 + seed
    b = 1729
    rOld = seed
    m = end - start
    rNew = (a * rOld + b) % m
    rNew = rNew + start
    return rNew


def ModelOutput(userData,true=1):

    #根据触发条件增加干扰车
    if userData["phase"] == 0:
        if userData["time"] >= userData["disturbEgoTime"] or getTotalDistance(0) >= userData[
            "disturbEgoStation"] or getVehicleSpeed(0) >= userData["disturbEgoSpeed"]:

            userData["phase"] += 1

            if userData["disturbVehicleType"] == 0:
                userData["newId"] = addVehicleRelated(0, userData["disturbDepartS"], userData["disturbDepartLat"],
                                                      userData["disturbDepartSpeedLow"], lane_type.current,
                                                      vehicle_type.Car, )

            if userData["disturbVehicleType"] == 1:
                userData["newId"] = addVehicleRelated(0, userData["disturbDepartS"], userData["disturbDepartLat"],
                                                      userData["disturbDepartSpeedLow"], lane_type.current,
                                                      vehicle_type.Van, )

            if userData["disturbVehicleType"] == 2:
                userData["newId"] = addVehicleRelated(0, userData["disturbDepartS"], userData["disturbDepartLat"],
                                                      userData["disturbDepartSpeedLow"], lane_type.current,
                                                      vehicle_type.Bus, )

            if userData["disturbVehicleType"] == 3:
                userData["newId"] = addVehicleRelated(0, userData["disturbDepartS"], userData["disturbDepartLat"],
                                                      userData["disturbDepartSpeedLow"], lane_type.current,
                                                      vehicle_type.OtherVehicle, )

            userData["departObjectTime"] = userData["time"]

            print("添加干扰车OK")

    #根据changeSpeedDuration触发车辆变速干扰
    if userData["phase"] == 1:
        if (userData["time"] - userData["departObjectTime"]) % userData["changeSpeedDuration"] == 0:
            if userData["disturbDepartSpeedHigh"] > userData["disturbDepartSpeedLow"]:
                lowSpeed = userData["disturbDepartSpeedLow"] * 100
                highSpeed = userData["disturbDepartSpeedHigh"] * 100
                changeSpeedResult = myrandint(lowSpeed,highSpeed,userData["time"]) / 100

                print("changeSpeedResult")
                print(changeSpeedResult)

                changeSpeed(userData["newId"],changeSpeedResult,userData["changeSpeedDuration"])

                #print("变速成功")

            else:
                changeSpeed(userData["newId"], userData["disturbDepartSpeedLow"], userData["changeSpeedDuration"])
                # print("变速成功")

def ModelTerminate(userData):
    pass