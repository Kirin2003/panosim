# 脚本描述：     该文件提供一个场景车辆管理类myDisturbeSceneManager
# 版本：        v1.0
# 作者：        韩飞
# 说明：        场景管理，用于多车干扰功能的发车模型
# 日期：        2021-06-08

# 更新：       v0.1    2021-05-16    起草场景车辆管理类
# 更新：       v0.2    2021-05-17    添加场景管理初始化函数，以TopLeader车辆限制多车干扰整体交通的速度；
#                                   添加场景管理更新函数，判断需要删除的车辆（原则是超过TopLeader车辆且超过2秒的车辆），并删除车辆
# 更新：       v0.3    2021-05-18    修改语法错误，并完成初步验证
# 更新：       v0.4    2021-05-20    修改语法及用法错误，并完成初步验证
#                                   修改myDisturbeSceneManager::__myGetSceneDeleteVehicleList函数，在删除车辆的同时，增加一个发车
# 更新：       v0.5    2021-05-30    增加主车的行为识别，且在不同行为下，配置周围交通车
# 更新：       v0.6    2021-06-02    增加车辆管理的删除原则，增加交叉口处交通车处理列表条件
# 更新：       v0.7    2021-06-03    修改v10 API相关函数和调用
# 更新：       v0.8    2021-06-04    增加车辆管理的新增车辆原则；增加主车临近交叉口时，对驶出交叉口车辆的创建；
# 更新：       v0.9    2021-06-05    测试用发现deleteVehicle接口耗时严重，优化调用频次和性能，减少耗时
# 发布：       v1.0    2021-06-05    todo: [1] 需要判断一下主车是否超过了TopLeader?
#                                         [2] 使用setSpeed和setLateral之后，车辆位置异常。如何保证主车在仅setSpeed后，不会产生换道？
#                                         [3] 使用addVehicleXY后，无法获得laneID，导致交叉口处车辆消失
#                                         [4] 在后方增加车辆的方法需要优化
# 更新：       v1.1    2021-06-07    修改查找前车们的方法，优化车辆删除的方法
# 更新：       v1.2    2021-06-25    所有车辆初始速度，最低设为10m/s
#                                   todo: [1] 修改承接UI的“交通流密度”参数
# 更新：       v1.3    2021-06-26    新增一个选择多车干扰的类myMultiDisturbeChosen，该类更新后，返回需要控制的“多车”，
#                                   和该步长需要删除的“多车”，用于下一步多车的控制。通过全局变量g_nDefaultMaxDisturbVehicleCount
#                                   可以调整最多控制几台干扰车
# 更新：       v1.4    2021-06-27    优化发车模型，扩大发车模型校验区域，防止车辆重叠
#                                   优化删车模型，车道内超过校验区域的车辆进行删除，防止车道内车辆过多
#                                   增加发车模型的车速
#                                   优化交叉口处的选车模型
#                                   发车和选车模型，增加时间间隔
# 更新：       v1.5    2021-06-30    更新myMultiDisturbeChosen的选车参数，和时间间隔
#




import math
from numpy import random
import operator


from TrafficModelInterface import *
from PanoSimTrafficAPI2 import *

g_dMaxLeaderDistance = 150  # 250.0
g_dMaxFollowerDistance = 50  # 150.0
g_nHostVehicleID = 0

g_dicExceedVehicles = {}
g_dicLocalUserData = {}
g_nLastCheckDeletedTimeStamp = 0
g_nLastCheckAddTimeStamp = 0

g_dicAddedVehicleByXY = {}

g_nDefaultTrafficDensity = 10        # 这里可以修改交通流的密度  todo：编写UI接口
g_dDefaultMinVehicleSpeed = 5          # 这里可以修改交通车的最小初始车速
g_nDefaultMaxDisturbVehicleCount = 1    # 这里可以修改，最多支持几个多车干扰！ todo：编写UI接口


MY_PANOSIM_INVALID_VALUE = -1073741824


class myDisturbeSceneManager:

    def test(self, modelUserData):
        pass

    # 函数: mySceneInitialization
    # 用途: 设置场景管理的初始化状态
    # [o]: Bool -   True: 已完成车辆初始化；  False： 未完成车辆初始化
    def mySceneInitialization(self, modelUserData):
        global g_dicLocalUserData
        g_dicLocalUserData = modelUserData
        if not myCheckHostVehicleInitialized():
            self.m_bInit = False
            self.__myReinitialized()
            return False
        elif self.m_bInit:
            return True
        else:
            self.m_bInit = True
            print("Host Position when Init:", myGetHostVehicleCoordinate(), myGetHostVehicleStationInLane(), myGetHostVehicleSpeed())
            (self.m_lGlobalLeaders, self.m_lVehicleList, self.m_nVehCountInLane, self.m_dVehIntervalInLane) = setInitializedVehicles(self.m_nTrafficDensity)

            return True

    # 函数: mySceneUpdate
    # 用途: 更新场景，期间包括补发车辆、删除车辆、更新TopLeader车辆，并返回场景管理状态
    # [o]: [vehicleList] -   返回需要被控制的车辆列表
    def mySceneUpdate(self):
        if not self.m_bInit: return []
        self.m_lVehicleList = []
        lDeleteVehicleList = []
        hostVehicleSpeed = myGetHostVehicleSpeed()

        # todo: 这里因为无法直接设置行驶路径，临时措施
        global g_dicAddedVehicleByXY
        tmpDicAddedVehicleByXY = dict(g_dicAddedVehicleByXY)
        g_dicAddedVehicleByXY = {}
        for id in tmpDicAddedVehicleByXY:
            if( mySetValidRoute(int(id)) == False):
                g_dicAddedVehicleByXY[id] = 0

        # 判断主车的行为，适时地增加仿真车辆
        (hostVehicleBehavior, isStatusChanged) = self.m_cHostVehicleBehaivorClass.updateHostVehicleStatus()
        if( hostVehicleBehavior == 0 or hostVehicleBehavior == 1 ) and isStatusChanged:  # 主车正常行驶或换道行驶
            # todo：需要判断一下主车是否超过了TopLeader
            pass
        elif( hostVehicleBehavior == 2 ) and isStatusChanged:  # 主车接近交叉口
            self.__mySetVehicles4ApproachJunction()
            print( "Tips: setVehicles When Approaching Junction" )
        elif( hostVehicleBehavior == 3 ) and isStatusChanged:  # 主车在交叉口中
            #self.__mySetVehicles4ApproachJunction()
            pass
        elif( hostVehicleBehavior == 4 ) and isStatusChanged:  # 主车驶出交叉口，到正常的车道中
            self.__mySetVehicles4NextLane()
            print("Tips: setVehicles When into a new lane from a junction")
            pass

        # 更新TopLeader交通车辆
        self.__mySceneUpdateTopLeaders()
        lDeleteVehicleList = self.__myGetSceneDeleteVehicleList(hostVehicleBehavior, isStatusChanged)  # 确定需要删除的交通车辆
        #print( "DeleteVehicles:", lDeleteVehicleList )

        # 如果PanoSim提供的车辆列表中的车辆，不属于主车且不在需要被删除的交通车辆列表中，则需要被控制，插入控制列表
        for id in getVehicleList():
            if (id not in lDeleteVehicleList) and (not myCheckHostVehicle(id)) and (id not in self.m_lGlobalLeaders):
                if( not getVehicleModelingControlRights(id) ):  # 如果该车已经接近交叉口，则不再由交通模型控制
                    self.m_lVehicleList.append(id)
                self.m_lVehicleList.append(id)

        # 删除需要被删除的车辆，即那些跑到TopLeader前面的车辆
        for id in lDeleteVehicleList:
            if id not in self.m_lGlobalLeaders:  # 不是头车，即一个交通车超车到头车前方，删除，并在该车道后方添加交通车
                #newID = myAddVehicleRelated(id, -(g_dMaxLeaderDistance + g_dMaxFollowerDistance), 0.0, hostVehicleSpeed, 0)
                #self.m_lVehicleList.append(newID)
                myDeleteVehicle(id)
            else:  # 头车被删除了
                self.m_bNeedRescene = True

        # 根据场景情况，在车辆后方增加车辆
        if( myGetSimulationTimeStamp() - self.m_nLastAddFollowerTime > 600 ): #每0.6秒判断增加后方车辆
            if( self.__myCheckAddVehicles() ):
                self.m_nLastAddFollowerTime = myGetSimulationTimeStamp()

        # 如果场景需要重建，即TopLeader被删除了，那么需要重新设定TopLeaders
        if self.m_bNeedRescene:
            pass


        #print( "@@@@@@ TopLeaders: ", self.m_lGlobalLeaders )


        # 从车辆列表中，选择多车干扰的控制车辆和本次删除的控制车辆
        (lDisturbVehicleChosen, lDisturbRemoveFocusID) = self.m_cDisturbVehicleChosenClass.updateDisturbeChosen(hostVehicleBehavior, self.m_lVehicleList, myGetSimulationTimeStamp())

        return (self.m_lVehicleList, lDisturbVehicleChosen, lDisturbRemoveFocusID)

    def __init__(self, modelUserData):
        self.__myReinitialized()
        global g_dicUserData
        g_dicUserData = modelUserData

        # todo： UI参数传入
        global g_nDefaultTrafficDensity, g_nDefaultMaxDisturbVehicleCount
        #g_nDefaultTrafficDensity = modelUserData[ "xxxx" ]
        #g_nDefaultMaxDisturbVehicleCount = modelUserData[ "xxxx" ]

        global g_dicExceedVehicles, g_dicLocalUserData, g_nLastCheckAddTimeStamp, g_nLastCheckDeletedTimeStamp, g_dicAddedVehicleByXY
        g_dicExceedVehicles = {}
        g_dicLocalUserData = {}
        g_nLastCheckDeletedTimeStamp = 0
        g_nLastCheckAddTimeStamp = 0
        g_dicAddedVehicleByXY = {}


    def __myReinitialized(self):
        self.m_bInit = False
        self.m_bNeedRescene = False
        self.m_lGlobalLeaders = []
        self.m_lVehicleList = []
        self.m_nTrafficDensity = g_nDefaultTrafficDensity
        self.m_cHostVehicleBehaivorClass = myHostVehicleEstimation()
        self.m_cDisturbVehicleChosenClass = myMultiDisturbeChosen()
        self.m_nVehCountInLane = 0
        self.m_dVehIntervalInLane = 0
        self.m_nLastAddFollowerTime = 0
        global g_dicExceedVehicles
        g_dicExceedVehicles = {}

    # 更新TopLeader车辆，这里设置尽量让TopLeader车辆与主车保持一定领航的距离。
    def __mySceneUpdateTopLeaders(self):
        self.__myCheckTopLeaderStatus()

        hostSpeed = myGetHostVehicleSpeed()
        hostStation = myGetHostVehicleStationInLane()

        for topLeaderID in self.m_lGlobalLeaders:
            isControlActive = getVehicleModelingControlRights(topLeaderID)
            if not isControlActive:
                continue

            ownCarSpeed = getVehicleSpeed(topLeaderID)
            ownCarStation = myGetDistanceFromLaneStart(topLeaderID)

            targetDeltaStation = ownCarStation - hostSpeed

            changeSpeed(topLeaderID, hostSpeed, 1.0)  # todo 这里需要根据TopLeader和主车的距离间隔，动态的调整车速。
            #changeLateralOffset(topLeaderID, 0.0)  # todo 添加该句后，会使这些车辆横在道上

    # 这里校验TopLeader是否应该被删除：
    # 如果topLeader驶出场景，则在列表中删除ID，
    # 如果topLeader不在是主车的车道，则从场景中删除，并添加新的头车
    def __myCheckTopLeaderStatus(self):
        lDeleteList = []
        lNotExistList = []
        for topLeaderID in self.m_lGlobalLeaders:
            if (topLeaderID not in getVehicleList()):
                lNotExistList.append(topLeaderID)

        for id in lNotExistList:
            self.m_lGlobalLeaders.remove(id)

    # 确认需要删除的车辆，并返回删除车辆的列表，同时需要在相同的车道增加一个车辆
    # 基本原则是，TopLeader的前车们，并持续了2秒以上
    def __myGetSceneDeleteVehicleList(self, hostVehicleBehavior, isStatusChanged):
        vehDeleteList = []
        curTimeStamp = myGetSimulationTimeStamp()
        # 如果有车辆超过头车，则删除
        for topLeader in self.m_lGlobalLeaders:
            #leaders = self.__myGetLeadersInCurLane(topLeader)
            leaders = myGetLeaders(topLeader)
            for exceedVehicleID in leaders:
                global g_dicExceedVehicles
                lastExceedTime = g_dicExceedVehicles[str(exceedVehicleID)]
                if (curTimeStamp - lastExceedTime > 3500):  # 如果比上一个超过时间超3.5秒以上还没被处理，证明这个时间是初始化时间
                    g_dicExceedVehicles[str(exceedVehicleID)] = curTimeStamp
                elif (curTimeStamp - lastExceedTime > 2000):  # 如果比上一个超过时间超2.0秒以上，则删除车辆
                    vehDeleteList.append(exceedVehicleID)
                else:
                    pass

        # 需要判断一下，当车辆在常规车道中行驶时，哪些车辆需要被删除，
        # 这里期望时删除那些非主车edge且距离大于50m的车辆
        global g_nLastCheckDeletedTimeStamp
        if( curTimeStamp - g_nLastCheckDeletedTimeStamp > 2000 ):
            lanes = []
            dirs = []
            g_nLastCheckDeletedTimeStamp = curTimeStamp
            hostLaneID = myGetLaneID(g_nHostVehicleID)
            if (hostLaneID == ""):
                print("Tips: host vehicle is not located in a lane!")
            else:
                (lanes, dirs) = myGetAllLanesInEdge(hostLaneID)

            if(hostVehicleBehavior==4 and isStatusChanged == True) or (hostVehicleBehavior==0 or hostVehicleBehavior==1):  # 主车刚刚进入下一个车道
                if len(lanes) > 0:
                    lTempVehicleList = getVehicleList()
                    for vehId in lTempVehicleList:
                        if(vehId not in vehDeleteList) and (not myCheckHostVehicle(vehId)) and (vehId not in self.m_lGlobalLeaders):
                            vehLaneID = myGetLaneID(vehId)
                            if( vehLaneID not in lanes):
                                (hostX, hostY) = myGetHostVehicleCoordinate()
                                (vehX, vehY) = myGetVehicleCoordinate(vehId)
                                distanceBetween = math.sqrt( math.pow(hostX-vehX,2) + math.pow(hostY-vehY,2) )
                                if( distanceBetween > g_dMaxLeaderDistance ):
                                    vehDeleteList.append(vehId)
                pass

            if(hostVehicleBehavior==0 or hostVehicleBehavior==1): # 主车在车道中行驶（车道保持 或 换道）
                if len(lanes) > 0:
                    for laneID in lanes:
                        if( laneID == ""): continue
                        (followers, leaders) = myGetAllVehiclesWithHostVehicleReleated( laneID, g_dMaxLeaderDistance + 20, g_dMaxFollowerDistance + 20 )
                        vehicleListInLane = myGetVehiclesInLane(laneID)
                        for vehID in vehicleListInLane:
                            if( (vehID not in followers) and (vehID not in leaders) and (not myCheckHostVehicle(vehID)) ):
                                #print( "Delete because beyond distance" )
                                vehDeleteList.append(vehID)



        return vehDeleteList

    # 获得所有的当前车道前车
    def __myGetLeadersInCurLane(self, vehID):
        leaders = []
        targetID = vehID
        targetID = getLeaderVehicle(targetID)
        while (targetID != -1 and targetID != MY_PANOSIM_INVALID_VALUE):
            leaders.append(targetID)
            targetID = getLeaderVehicle(targetID)

        return leaders

    # 根据不同的主车状态，判断是否增加车辆
    # 情况2： 主车即将进入交叉口，在交叉口的其他驶入车道上增加交通车辆
    def __mySetVehicles4ApproachJunction(self):
        defaultInterval = self.m_dVehIntervalInLane  #车道中各车辆的间距
        defaultStartStation = 15 #头车距交叉口的距离
        vehCount = 6
        defaultVehicleSpeed = 20.0/3.6
        lAddVehicleList = []
        curLane = myGetLaneID(g_nHostVehicleID)
        (lanesInEdge, dirsInEdge) = myGetAllLanesInEdge(curLane)

        # 在交叉口的incomingLanes上增加车辆
        junctionID = myGetToJunction(curLane)
        incomingLanes = myGetIncomingLanes(junctionID)
        for laneID in incomingLanes:
            if( laneID in lanesInEdge ): continue

            laneLength = myGetLaneLength(laneID)
            idx = 0
            sumStation = defaultStartStation
            while idx < vehCount:
                dstId = myAddVehicleInLane( laneID, laneLength - sumStation, defaultVehicleSpeed, )
                if( dstId >= 0 ):
                    sumStation = sumStation + defaultInterval * random.ranf() * defaultInterval
                    lAddVehicleList.append(dstId)
                idx = idx + 1

        # 在主车可能行驶的车道们上增加车辆   junction的去向车道

        lToLanes = myGetAllToLanes(curLane)
        if( len(lToLanes) > 0 ):
            for toLane in lToLanes:
                interval = myGetRandomDouble(self.m_dVehIntervalInLane/2, self.m_dVehIntervalInLane*3/2)
                while (1):
                    dstId = myAddVehicleInLane(toLane, interval, defaultVehicleSpeed)
                    if( dstId >= 0 ):
                        interval = interval + interval + myGetRandomDouble( 0, interval )
                        lAddVehicleList.append(dstId)
                    else:
                        print( "Tips: end until vehicle beyond length of lane" )
                        break

        #print( "Add vehicles when approaching junction:", lAddVehicleList )
        return lAddVehicleList

    # 情况3： 主车已经在某一个交叉口中
    def __mySetVehicles4InJunction(self):
        pass

    # 情况4： 主车驶入了下一条车道，这时需要确立新的TopLeader车辆，并判断是否需要补车
    def __mySetVehicles4NextLane(self):
        idx = 0
        finalLeaders = []
        hostLaneID = myGetLaneID(g_nHostVehicleID)
        if( myCheckInternalLane(hostLaneID) ):
            print( "Error when setting vehicle for next lane, due to the host vehicle is still in junction" )
            return
        (allLanes, laneDir) = myGetAllLanesInEdge(hostLaneID)
        #print( allLanes, laneDir )

        baseDeltaStation = g_dMaxLeaderDistance * 0.3
        if baseDeltaStation > 50:
            baseDeltaStation = 50.0
        elif baseDeltaStation < -50:
            baseDeltaStation = -50.0

        randomDeltaStation = 0
        if (idx == 0): randomDeltaStation = 0
        elif ( idx % 2 == 1): randomDeltaStation = myGetRandomDouble( 10, 10 + baseDeltaStation )
        else: randomDeltaStation = myGetRandomDouble(0, baseDeltaStation/2)

        for laneID in allLanes:
            (lFollowers,lLeaders) = myGetAllVehiclesWithHostVehicleReleated(laneID, g_dMaxLeaderDistance, g_dMaxFollowerDistance)
            leaderCount = len(lLeaders)

            if( leaderCount == 0 ):
                newID = -1
                newID = myAddVehicleRelated(g_nHostVehicleID, g_dMaxLeaderDistance*2/3, 0, myGetHostVehicleSpeed(), laneDir[idx])
                if (idx == 0):
                    newID = myAddVehicleRelated(g_nHostVehicleID, g_dMaxLeaderDistance*4/5, 0, myGetHostVehicleSpeed(), laneDir[idx])
                else:
                    newID = myAddVehicleRelated(g_nHostVehicleID, g_dMaxLeaderDistance - randomDeltaStation, 0, myGetHostVehicleSpeed(), laneDir[idx])
                if( newID < 0 ):
                    print("Error when add vehicles due to nextLane in lane!")
                else:
                    finalLeaders.append(newID)
            else:
                maxLeaderDeltaDistance = myGetLongitudinalDistance( lLeaders[leaderCount-1], g_nHostVehicleID )
                newID = -1
                if( maxLeaderDeltaDistance < g_dMaxLeaderDistance*2/3 ):
                    if( idx == 0 ):
                        newID = myAddVehicleRelated(g_nHostVehicleID, g_dMaxLeaderDistance*4/5, 0, myGetHostVehicleSpeed(), laneDir[idx])
                    else:
                        newID = myAddVehicleRelated(g_nHostVehicleID, g_dMaxLeaderDistance - randomDeltaStation, 0, myGetHostVehicleSpeed(), laneDir[idx])
                    if (newID < 0):
                        print("Error when add vehicles due to nextLane in lane!")
                    else:
                        finalLeaders.append(newID)
                else:
                    finalLeaders.append(lLeaders[leaderCount-1])

            idx = idx + 1

        self.m_lGlobalLeaders = finalLeaders
        #print( "New Global Leaders due to a new lane", self.m_lGlobalLeaders )

    # 实时监控道路中的交通情况，如果主车station超过maxFollowerDistance，且车道内车辆数不足密度要求或主车后方没有车辆，则插入一个车辆。
    def __myCheckAddVehicles(self):
        vehicleAdded = False
        global  g_nLastCheckAddTimeStamp
        curTimeStamp = myGetSimulationTimeStamp()
        if( curTimeStamp - g_nLastCheckAddTimeStamp < 1500 ): return
        g_nLastCheckAddTimeStamp = curTimeStamp

        (lanesInEdge, lanesDir) = myGetAllLanesInEdge(myGetLaneID(g_nHostVehicleID))
        if( len(lanesInEdge) > 0 ):
            idx = 0
            newID = -1
            hostStation = myGetHostVehicleStationInLane()
            for laneID in lanesInEdge:
                (followers, leaders) = myGetAllVehiclesWithHostVehicleReleated(laneID, g_dMaxLeaderDistance, g_dMaxFollowerDistance+20)
                vehCount = len(followers) + len(leaders)
                if( vehCount < self.m_nVehCountInLane * 1.2 ):
                    if( len(followers) == 0 ):
                        #newID = myAddVehicleRelated(g_nHostVehicleID, -g_dMaxFollowerDistance, 0, myGetHostVehicleSpeed(), lanesDir[idx])
                        newID = myAddVehicleInLane( laneID, hostStation - g_dMaxFollowerDistance, myGetHostVehicleSpeed()*1.5 )
                    else: #如果最后一个follower的位置超过最大距离的15米，则增加车辆
                        lastVeh = followers[0]
                        deltaDistance = myGetLongitudinalDistance(lastVeh, g_nHostVehicleID)
                        if( deltaDistance > (-g_dMaxFollowerDistance + 30) ):
                            #pass
                            #newID = myAddVehicleRelated(g_nHostVehicleID, -g_dMaxFollowerDistance, 0, myGetHostVehicleSpeed(), lanesDir[idx])
                            #print( "Last DeltaDistance:", deltaDistance, "Last Station:", myGetDistanceFromLaneStart(lastVeh), "Create New Station", hostStation - g_dMaxFollowerDistance )
                            newID = myAddVehicleInLane(laneID, hostStation - g_dMaxFollowerDistance, myGetHostVehicleSpeed() * 1.5)
                            #print(newID, myGetAllVehiclesWithHostVehicleReleated(laneID, g_dMaxLeaderDistance, g_dMaxFollowerDistance+20))

                if( newID > 0 ):
                    vehicleAdded = True
                    if( getVehicleModelingControlRights(newID) ):
                        self.m_lVehicleList.append(newID)
                idx = idx + 1
        return vehicleAdded



    # members
    m_bInit = False
    m_lVehicleList = []
    m_lGlobalLeaders = []
    m_nTrafficDensity = g_nDefaultTrafficDensity
    m_nVehCountInLane = 0
    m_dVehIntervalInLane = 0
    m_nLastAddFollowerTime = 0

    m_bNeedRescene = False
    m_cHostVehicleBehaviorClass = 0
    m_cDisturbVehicleChosenClass = 0




class myHostVehicleEstimation:

    def __init__(self):
        self.reset()

    def reset(self):
        self.m_nHostVehicleBehavior = -1
        self.m_nLastHostVehicleBehavior = -1
        self.m_dLastDistanceToJunction = MY_PANOSIM_INVALID_VALUE
        self.m_dLastVehicleSpeed = MY_PANOSIM_INVALID_VALUE
        self.m_nLastLaneID = -1

    def updateHostVehicleStatus(self):
        isFirstChanged = False
        newSpeed = myGetHostVehicleSpeed()
        newDistanceToJunction = myGetDistanceToLaneEnd(g_nHostVehicleID)
        newLaneID = myGetLaneID(g_nHostVehicleID)
        #print( newDistanceToJunction, newLaneID  )
        if( self.m_nHostVehicleBehavior == -1):
            #print("###@@@ -1")
            isInited = myCheckHostVehicleInitialized()
            if( isInited == True ):
                self.m_nHostVehicleBehavior = 0
                self.m_nLastHostVehicleBehavior = self.m_nHostVehicleBehavior
                self.m_nLastLaneID = myGetLaneID(g_nHostVehicleID)
                self.m_dLastVehicleSpeed = myGetHostVehicleSpeed()
                if( myCheckInternalLane(self.m_nLastLaneID) ):
                    self.m_dLastDistanceToJunction = MY_PANOSIM_INVALID_VALUE
                    print( "Warning: the host vehicle is initialized in a junction" )
                else:
                    self.m_dLastDistanceToJunction = myGetDistanceToLaneEnd(g_nHostVehicleID)
            else:
                return -1
        elif( self.m_nHostVehicleBehavior == 0 or self.m_nHostVehicleBehavior == 1):
            #print("###@@@ 0 1")
            if( self.__checkInJunction() ):  #进入junction
                self.m_nHostVehicleBehavior = 3
                newDistanceToJunction = MY_PANOSIM_INVALID_VALUE
                print( "### Warning: Host vehicle is from a lane to a junction directly without approaching junction")
            elif( self.__checkApproachJunction() ):  #接近junction
                self.m_nHostVehicleBehavior = 2
            elif( self.__checkEnterLane() ):  #驶入下一条车道
                self.m_nHostVehicleBehavior = 4
                print( "### Warning: Host vehicle is from a lane to next lane directly without via junction!" )
            elif( self.__checkLaneChange() ):  #主车换道
                self.m_nHostVehicleBehavior = 1
            else:
                self.m_nHostVehicleBehavior = 0
        elif( self.m_nHostVehicleBehavior == 2 ):
            #print("###@@@ 2")
            if( self.__checkInJunction() ):
                self.m_nHostVehicleBehavior = 3
                newDistanceToJunction = MY_PANOSIM_INVALID_VALUE
            else:
                pass
        elif( self.m_nHostVehicleBehavior == 3):
            #print("###@@@ 3")
            if( self.__checkEnterLane() ):
                self.m_nHostVehicleBehavior = 4
            else:
                pass
        elif( self.m_nHostVehicleBehavior == 4):
            #print("###@@@ 4")
            self.m_nHostVehicleBehavior = 0


        self.m_dLastDistanceToJunction = newDistanceToJunction
        self.m_dLastVehicleSpeed = newSpeed
        self.m_nLastLaneID = newLaneID

        if( self.m_nHostVehicleBehavior == self.m_nLastHostVehicleBehavior ):
            isFirstChanged = False
        else:
            isFirstChanged = True
            #print("###@@@ HostVehicle is ", self.m_nHostVehicleBehavior)

        self.m_nLastHostVehicleBehavior = self.m_nHostVehicleBehavior
        return (self.m_nHostVehicleBehavior, isFirstChanged)


    def __checkApproachJunction(self):
        newDistanceToJunction = myGetDistanceToLaneEnd(g_nHostVehicleID)
        curLaneID = myGetLaneID(g_nHostVehicleID)
        if( myCheckInternalLane(curLaneID) ): return False
        else:
            timeToJunction = 9999
            if( myGetHostVehicleSpeed() > 0.1 ): timeToJunction = newDistanceToJunction / myGetHostVehicleSpeed()
            if( newDistanceToJunction<30 or timeToJunction < 3.0 ):
                return True
            else:
                return False

    def __checkInJunction(self):
        curLaneID = myGetLaneID(g_nHostVehicleID)
        if( myCheckInternalLane(curLaneID) ): return True
        else: return False

    def __checkLaneChange(self):
        curLaneID = myGetLaneID(g_nHostVehicleID)
        if( curLaneID == self.m_nLastLaneID ): return False
        else:
            if( myCheckInternalLane(curLaneID) == myCheckInternalLane(self.m_nLastLaneID) ): return True
            else: return False

    def __checkEnterLane(self):
        curLaneID = myGetLaneID(g_nHostVehicleID)
        if( curLaneID == self.m_nLastLaneID ): return False
        else:
            if( myCheckInternalLane(curLaneID)==False and myCheckInternalLane(self.m_nLastLaneID)==True ): return True
            else: return False


    ### -1->0; 0->1,2; 1->0,2; 2->3; 3->4; 4->0
    m_nHostVehicleBehavior = -1  # -1: Init, 0: keeping, 1: laneChange, 2: approachJunction, 3: in junction, 4: next lane
    m_nLastHostVehicleBehavior = -1
    m_dLastDistanceToJunction = -99999
    m_dLastVehicleSpeed = -99999
    m_nLastLaneID = -1






class myMultiDisturbeChosen:
    def __init__(self):
        self.__reInitialized()

    def updateDisturbeChosen(self, hostVehicleBehavior, vehicleList, curTime):
        maxDisturbCount = g_nDefaultMaxDisturbVehicleCount
        self.__getInterestLanes()

        self.m_lVehicleChosen = self.__focusRemove(vehicleList, curTime)
        self.__focusUpdate(curTime)

        if( curTime - self.m_nLastCreateTime > 1510 ): # 每1.51s，更新一次“多车”
            self.m_lVehicleChosen = self.__focusCreate(vehicleList, self.m_lVehicleChosen, maxDisturbCount, curTime)
            if( len(self.m_lVehicleChosen) <= 0 ):
                self.m_nLastCreateTime = curTime
        self.m_lVehicleChosen = self.__focusCreate(vehicleList, self.m_lVehicleChosen, maxDisturbCount, curTime)

        #print( "######Chosen Vehicle:", self.m_lVehicleChosen )
        #print( "######Chosen dic:", self.m_dicVehicleFlag)
        return (self.m_lVehicleChosen, self.m_lRemoveFocusID)

    def __focusRemove(self, vehicleList, curTime):
        tmpVehicleList = list(self.m_lVehicleChosen)
        lInterestLanes = self.m_lInterestLanes
        self.m_lRemoveFocusID = []

        for id in self.m_lVehicleChosen:
            # 计算条件0： 车辆已不在车辆列表中。
            if id not in vehicleList:
                self.m_lRemoveFocusID.append(id)
                tmpVehicleList.remove(id)
                self.m_dicVehicleFlag.pop(str(id))
                continue

            # 计算条件0： 车辆进入交叉口管理范围
            if not getVehicleModelingControlRights(id):
                self.m_lRemoveFocusID.append(id)
                tmpVehicleList.remove(id)
                self.m_dicVehicleFlag.pop(str(id))
                continue

            # 计算条件1： 车辆不在主车的周边车道。 todo: 暂时未考虑交叉口的情况
            condition1 = self.__checkCondition_interestLanes(id)
            if not condition1 and not self.__checkCondition_FalseTimer(id, curTime, 1000): #不满足车道哟球，且持续时间1s
                self.m_lRemoveFocusID.append(id)
                tmpVehicleList.remove(id)
                self.m_dicVehicleFlag.pop(str(id))
                continue

            # 计算条件2： 车辆不在主车的有效距离内
            condition2 = self.__checkCondition_distance(id)
            if not condition2 and not self.__checkCondition_FalseTimer(id, curTime, 2000): # 不满足距离要求，且持续时间2s
                self.m_lRemoveFocusID.append(id)
                tmpVehicleList.remove(id)
                self.m_dicVehicleFlag.pop(str(id))
                continue
        return tmpVehicleList

    def __focusUpdate(self, curTime):
        for id in self.m_lVehicleChosen:
            if( str(id) not in self.m_dicVehicleFlag ):
                self.__focusInit(id, curTime)
                continue
            condition1 = self.__checkCondition_interestLanes(id)
            condition2 = self.__checkCondition_distance(id)
            condition3 = getVehicleModelingControlRights(id)
            condition = True
            if( condition1 and condition2 and condition3): condition = True
            else: condition = False
            dicVehInfo = self.m_dicVehicleFlag[str(id)]
            dicVehInfo["Condition"] = condition
            if condition:
                dicVehInfo["TimerFlag"] = True
                dicVehInfo["StartTime"] = curTime

    def __focusCreate(self, vehicleList, vehicleChosen, maxDisturbVehicle, curTime):
        if( len(vehicleChosen) >= maxDisturbVehicle ): return vehicleChosen
        idx = 0
        nNeedVehicles = maxDisturbVehicle - len(vehicleChosen)
        lTmpVehicleChosenList = list(vehicleChosen)
        lVehicleAroundHost = []

        hostLaneID = myGetLaneID(g_nHostVehicleID)
        for laneID in self.m_lInterestLanes:
            if( laneID == hostLaneID ): continue
            (followers, leaders) = myGetAllVehiclesWithHostVehicleReleated(laneID, 30, 5)  #寻找车道内，主车前方20米后方50米范围内的所有车辆
            for id in followers:
                if id not in vehicleChosen and id not in self.m_lRemoveFocusID and getVehicleModelingControlRights(id):
                    lVehicleAroundHost.append(id)
            for id in leaders:
                if id not in vehicleChosen and id not in self.m_lRemoveFocusID and getVehicleModelingControlRights(id):
                    lVehicleAroundHost.append(id)

        while( idx < nNeedVehicles):
            count = len(lVehicleAroundHost)
            if count <= 0:
                break
            vehID = lVehicleAroundHost[int(myGetRandomDouble(0, count-1))]
            lVehicleAroundHost.remove(vehID)

            self.__focusInit(vehID, curTime)
            lTmpVehicleChosenList.append(vehID)
            idx = idx + 1

        return lTmpVehicleChosenList

    def __focusInit(self, vehID, curTime):
        condition1 = self.__checkCondition_interestLanes(vehID)
        condition2 = self.__checkCondition_distance(vehID)
        condition = condition1 or condition2

        timerFlag = False
        if condition: timerFlag = True
        else: timerFlag = False

        timerStartTime = 9999999
        if condition: timerStartTime = curTime

        self.m_dicVehicleFlag[str(vehID)] = {"Condition": condition, "TimerFlag": timerFlag, "StartTime": timerStartTime}
        pass

    def __getInterestLanes(self):
        self.m_lInterestLanes = []
        hostLaneID = myGetLaneID(g_nHostVehicleID)
        if (hostLaneID == ""):
            print("Error: cannot get host vehicle in lane!")
            return
        leftLaneID = myGetLeftLaneID(g_nHostVehicleID)
        rightLaneID = myGetRightLaneID(g_nHostVehicleID)

        self.m_lInterestLanes.append(hostLaneID)
        if leftLaneID != "": self.m_lInterestLanes.append(leftLaneID)
        if rightLaneID != "": self.m_lInterestLanes.append(rightLaneID)
        return self.m_lInterestLanes

    def __checkCondition_interestLanes(self, clientID):
        condition = True
        clientLaneID = myGetLaneID(clientID)
        # 计算条件1： 车辆不在主车的周边车道。 todo: 暂时未考虑交叉口的情况
        if (clientLaneID not in self.m_lInterestLanes):
            condition = False
        else:
            clientLaneID = myGetLaneID(clientID)
            hostLaneID = myGetLaneID(g_nHostVehicleID)
            if( clientLaneID == hostLaneID ):
                clientStation = myGetDistanceFromLaneStart(clientID)
                hostStation = myGetDistanceFromLaneStart(g_nHostVehicleID)
                if( clientStation < hostStation ):
                    condition = False
                else:
                    condition = True
            else:
                condition = True
        return condition

    def __checkCondition_distance(self, clientID):
        condition = True
        # 计算条件2： 车辆不在主车的有效距离内
        deltaDistance = myGetLongitudinalDistance(clientID, g_nHostVehicleID)
        if 30 > deltaDistance > -20:
            condition = True
        else:
            condition = False
        return condition

    def __checkCondition_FalseTimer(self, clientID, curTime, limitDuration):
        dicVehInfo = self.m_dicVehicleFlag[str(clientID)]
        if( dicVehInfo["TimerFlag"] == False ): return True
        if( dicVehInfo["Condition"] == True ): return True
        timerStartTime = dicVehInfo["StartTime"]
        if( curTime - timerStartTime > limitDuration ): return False
        else: return True

    def __reInitialized(self):
        self.m_lVehicleChosen = []
        self.m_dicVehicleFlag = {}
        self.m_lInterestLanes = []
        self.m_lRemoveFocusID = []
        self.m_nLastCreateTime = 0

    m_lVehicleChosen = []
    m_dicVehicleFlag = {}
    m_lInterestLanes = []
    m_lRemoveFocusID = []
    m_nLastCreateTime = 0







# ==================================
#           基础函数
# =================================

# 函数: myCheckHostVehicleInitialized
# 用途: 判断主车是否完成初始化过程
# [i]: vehID - 车辆ID
# [o]: Bool - True已经完成，False正在初始化
def myCheckHostVehicleInitialized():
    (hostX, hostY) = myGetHostVehicleCoordinate()
    if ((hostX < -999999.0 and hostY < -999999.0) or (hostX == 0.0 and hostY == 0.0)) and (
            myGetSimulationTimeStamp() < 1000):
        return False
    else:
        return True

def myGetSimulationTimeStamp():
    global g_dicLocalUserData
    return g_dicLocalUserData["time"]


# 函数: myAddVehicleRelated
# 用途: 根据相对位置，添加车辆，接口与PanoSimAPI相同，仅对距离产生判断
# [i]: id - 参考车ID
# [i]: s - 相对station
# [i]: l - 相对offset
# [i]: speed - 车速
# [i]: lane -   -1：左侧； 0：当前； 1：右侧
# [i]: type - 车辆类型
# [i]: shape - 车辆形状
# [i]: driver - 驾驶员类型
# [o]: int - 创建的车辆ID
def myAddVehicleRelated(id, s, l, speed, lane=0, type=0, shape=0, driver=0):
    if( speed < g_dDefaultMinVehicleSpeed ): speed = g_dDefaultMinVehicleSpeed
    targetStation = myGetDistanceFromLaneStart(id)
    if( s + targetStation < 0 ): return -1
    if( s > myGetDistanceToLaneEnd(id) ): return -1

    dstId = addVehicleRelated(id, s, l, speed, lane_type(lane), vehicle_type(type), )
    # if dstId == MY_PANOSIM_INVALID_VALUE:
    #    return dstId

    if( dstId > 0):
        mySetValidRoute(dstId)
        global g_dicExceedVehicles
        g_dicExceedVehicles[str(dstId)] = 0

    return dstId

def myAddVehicleRelatedJunction(id, direction, route, speed, distance, type=0, shape=0, driver=0):
    if (speed < g_dDefaultMinVehicleSpeed): speed = g_dDefaultMinVehicleSpeed
    dstId = addVehicleRelatedJunction(id, direction, route, speed, distance, vehicle_type(type),)
    if( dstId > 0 ):
        mySetValidRoute(dstId, True)
        global g_dicExceedVehicles
        g_dicExceedVehicles[str(dstId)] = 0
    return dstId

def myAddVehicleInLane(laneID, s, speed, type=0, shape=0, driver=0):
    if (speed < g_dDefaultMinVehicleSpeed): speed = g_dDefaultMinVehicleSpeed
    (x,y) = getXYFromSL( laneID, s, 0)
    if( x == None or y == None ): return -1

    dstId = addVehicle(x, y, speed, vehicle_type(type), )  # todo: 该函数后，无法获得laneID，导致该车到交叉口就消失了
    if( dstId > 0 ):
        mySetValidRoute(dstId)
        global g_dicExceedVehicles
        g_dicExceedVehicles[str(dstId)] = 0
        global g_dicAddedVehicleByXY
        g_dicAddedVehicleByXY[str(dstId)] = 0

    return dstId




def myDeleteVehicle(id):
    if(id <= 0):
        pass
    else:
        deleteVehicle(id)
        global g_dicExceedVehicles
        if str(id) in g_dicExceedVehicles:
            g_dicExceedVehicles.pop(str(id))


# 函数: myGetFirstVehicleInCurLane
# 用途: 获得vehID当前车道中的头车
# [i]: vehID - 车辆ID
# [o]: vehID - 头车ID。   -1：无车；  INVALID：无车道；   其他：头车ID
def myGetFirstVehicleInCurLane(vehID, maxDistanceWithHost = 999999, type = 0):
    if(vehID < 0): return -1
    curLane = myGetLaneID(vehID)
    vehiclesInCurLane = myGetVehiclesInLane(curLane)

    curLeaderID = getLeaderVehicle(vehID)
    if type == 0:
        if curLeaderID == -1:
            return -1
        elif curLeaderID == MY_PANOSIM_INVALID_VALUE:
            print("Error when searching leader in current lane, returned invalid int value!")
            return MY_PANOSIM_INVALID_VALUE
    else:
        if curLeaderID < 0:  # -1 or INVALID_VALUE
            return vehID
    deltaDistance = myGetLongitudinalDistance(curLeaderID, vehID)
    if( deltaDistance > maxDistanceWithHost ):
        if type == 0:
            return -1
        else:
            return vehID
    return myGetFirstVehicleInCurLane(curLeaderID, maxDistanceWithHost, 1)


# 函数: myGetFirstVehicleInLeftLane
# 用途: 获得vehID左车道中的头车
# [i]: vehID - 车辆ID
# [o]: vehID - 头车ID。   -1：无车；  INVALID：无车道；   其他：头车ID
def myGetFirstVehicleInLeftLane(vehID, maxDistanceWithHost = 999999):
    leftLeaderID = getLeftLeaderVehicle(vehID)

    if leftLeaderID == -1:  # 有路没车
        leftFollowerID = getLeftFollowerVehicle(vehID)
        if(leftFollowerID == -1):
            return -1
        elif(leftFollowerID == MY_PANOSIM_INVALID_VALUE):
            print("Error in searching left follower in myGetFirstVehicleInLeftLane")
            return -1
        else:
            return myGetFirstVehicleInCurLane(leftFollowerID, maxDistanceWithHost, 1)
    elif leftLeaderID == MY_PANOSIM_INVALID_VALUE:
        print("Error in left first leader")
        return MY_PANOSIM_INVALID_VALUE
    else:
        deltaDistance = myGetLongitudinalDistance(leftLeaderID, g_nHostVehicleID)
        if( deltaDistance > maxDistanceWithHost ):
            return -1
        else:
            return myGetFirstVehicleInCurLane(leftLeaderID, maxDistanceWithHost, 1)


# 函数: myGetFirstVehicleInRightLane
# 用途: 获得vehID右车道中的头车
# [i]: vehID - 车辆ID
# [o]: vehID - 头车ID。   -1：无车；  INVALID：无车道；   其他：头车ID
def myGetFirstVehicleInRightLane(vehID, maxDistanceWithHost = 999999, type = 0):
    rightLeaderID = getRightLeaderVehicle(vehID)

    if rightLeaderID == -1:  # 有路没车
        rightFollowerID = getRightFollowerVehicle(vehID)
        if(rightFollowerID == -1):
            return -1
        elif(rightFollowerID == MY_PANOSIM_INVALID_VALUE):
            print("Error in searching right follower in myGetFirstVehicleInRightLane")
            return -1
        else:
            return myGetFirstVehicleInCurLane(rightFollowerID, maxDistanceWithHost, 1)
    elif rightLeaderID == MY_PANOSIM_INVALID_VALUE:
        print("Error in right first leader")
        return MY_PANOSIM_INVALID_VALUE
    else:
        deltaDistance = myGetLongitudinalDistance(rightLeaderID, g_nHostVehicleID)
        if( deltaDistance > maxDistanceWithHost ):
            return -1
        else:
            return myGetFirstVehicleInCurLane(rightLeaderID, maxDistanceWithHost, 1)

# 函数: clearVehicleList
# 用途: 清除车辆列表中非主车车辆
def __clearVehicleList():
    for id in getVehicleList():
        if myCheckHostVehicle(id):
            continue
        else:
            deleteVehicle(id)

# 函数: setInitializedVehicles
# 用途: 初始化车辆，放置全局头车以及其他交通车
# [i]: density - 1km内的车辆数
# [o]: globalLeader - 全局头车
# [o]: lVehicleList - 除全局头车的车辆信息
# [o]: vehCountInLane - 车道内需要的车辆数目
# [o]: avgVehiclesInterval - 平均车辆间隔
def setInitializedVehicles(density):
    __clearVehicleList()
    if not myCheckHostVehicleInitialized():
        return ([], [], 0, 0)

    lTopVehicles = []
    lVehicleList = []

    (hostX, hostY) = myGetHostVehicleCoordinate()
    hostSpeed = myGetHostVehicleSpeed()
    hostS = myGetHostVehicleStationInLane()

    ######
    ###### 设置各车道头车
    ######
    tmpLeaderDistance = g_dMaxLeaderDistance
    laneLength = myGetLaneLength( myGetLaneID(g_nHostVehicleID) )
    hostStation = myGetDistanceFromLaneStart( g_nHostVehicleID )
    if( hostStation+tmpLeaderDistance > laneLength - 5 ):
        tmpLeaderDistance = laneLength - 5 - hostStation

    # 增加当前车道头车

    tmpIDinSameLane = myAddVehicleRelated(g_nHostVehicleID, tmpLeaderDistance, 0.0, hostSpeed, 0)
    lTopVehicles.append(tmpIDinSameLane)

    # 单独定义：
    # 设置左侧车道（们）的头车
    setNeighborTopVehicles(tmpIDinSameLane, tmpLeaderDistance, -1, 0, lTopVehicles)

    # 设置右侧车道（们）的头车
    setNeighborTopVehicles(tmpIDinSameLane, tmpLeaderDistance, 1, 0, lTopVehicles)

    print(lTopVehicles)

    ######
    ###### 设置各车道车辆
    ######
    hostVehicleLength = getVehicleLength(getVehicleType(g_nHostVehicleID))
    vehAssignmentCountInLane = density * (g_dMaxLeaderDistance + g_dMaxFollowerDistance) / 1000
    if( vehAssignmentCountInLane <= 1 ): vehAssignmentCountInLane = 2
    avgVehiclesInterval = (g_dMaxLeaderDistance + g_dMaxFollowerDistance) / vehAssignmentCountInLane
    print("VehicleInLane:", vehAssignmentCountInLane, "VehicleInterval:", avgVehiclesInterval)

    for laneTopLeaderID in lTopVehicles:
        vehCountInLane = 1
        sumInterval = 0
        leaderVehicleLength = getVehicleLength(getVehicleType(laneTopLeaderID))
        while (vehCountInLane <= vehAssignmentCountInLane):
            randomInterval = random.ranf() * avgVehiclesInterval / 3.0
            #print("Value: ", randomInterval, "Type: ", type(randomInterval))
            sumInterval = sumInterval \
                          + avgVehiclesInterval \
                          + leaderVehicleLength \
                          + randomInterval
            # + 0 #这里应该加一个随机数
            if (laneTopLeaderID == tmpIDinSameLane) \
                    and (abs(g_dMaxLeaderDistance - sumInterval) < 20):
                continue

            tmpID = myAddVehicleRelated(laneTopLeaderID, -sumInterval, 0.0, hostSpeed, 0)
            if tmpID > 0:
                lVehicleList.append(tmpID)
                vehCountInLane = vehCountInLane + 1
                leaderVehicleLength = getVehicleLength(getVehicleType(tmpID))
            else:
                break

    print( "Total Vehicle Count: ", len(lTopVehicles) + len(lVehicleList) )

    return (lTopVehicles, lVehicleList, vehAssignmentCountInLane, avgVehiclesInterval)

# 函数: setNeighborTopVehicles
# 用途: 迭代设置旁车道头车
# [i]: ownVehicleID - 基于的车辆ID
# [i]: ownVehicleStation - 基于的车辆station
# [i]: dir - 方向   -1：左；  1：右
# [i]: iteration - 默认为0，迭代的起始次数
# [io]: topVehicles - 头车列表
def setNeighborTopVehicles(ownVehicleID, ownVehicleStation, dir, iteration, topVehicles):
    print("Set neighbor tops")
    if dir == 0:
        return
    if not myCheckLaneExist(ownVehicleID, dir):
        return

    baseDeltaStation = g_dMaxLeaderDistance * 0.2
    if baseDeltaStation > 50:
        baseDeltaStation = 50.0
    elif baseDeltaStation < -50: \
            baseDeltaStation = -50.0

    factor = 1.0
    if iteration % 2 == 1:
        factor = 1.0
    else:
        factor = -1.0

    randomDeltaStation = baseDeltaStation * factor * random.ranf() + 10.0
    print( "NeighborTopVehicle: ", randomDeltaStation )
    finalStation = ownVehicleStation + randomDeltaStation

    tmpID = myAddVehicleRelated(ownVehicleID, randomDeltaStation, 0, myGetHostVehicleSpeed(), dir)
    if (tmpID < 0):
        print("Error in setNeighborTopVehicles when creating vehicles", dir, iteration)
        return

    topVehicles.append(tmpID)
    setNeighborTopVehicles(tmpID, finalStation, dir, iteration + 1, topVehicles)
    return


# 函数: getVehicleListInLaneRelatedHost
# 用途: 基于主车返回，主车所在的车道中的所有车辆，范围限制在主车后g_dMaxFollowerDistance米的范围
# [i]: direction  -1:left, 0: cur, 1:right
# [o]: vehicleList[]
def getVehicleListInLaneRelatedHost(vehID, dir):
    lVehicleListInLane = []
    firstCar = -1
    if dir<0:
        firstCar = myGetFirstVehicleInLeftLane(vehID)
    elif dir==0:
        firstCar = myGetFirstVehicleInCurLane(vehID)
    else:
        firstCar = myGetFirstVehicleInRightLane(vehID)

    if( firstCar < 0 ):
        if dir < 0: firstCar = getLeftFollowerVehicle(g_nHostVehicleID)
        elif dir == 0: firstCar = getFollowerVehicle(g_nHostVehicleID)
        else: firstCar = getRightFollowerVehicle(g_nHostVehicleID)

        deltaDistance = myGetLongitudinalDistance(followerId, g_nHostVehicleID)
        if (firstCar > 0 and deltaDistance < -g_dMaxFollowerDistance) or firstCar < 0:
            firstCar = MY_PANOSIM_INVALID_VALUE  # INVALID

    if firstCar <= 0:
        return lVehicleListInLane

    lVehicleListInLane.append(firstCar)
    while( firstCar >= 0 ):
        followerId = getFollowerVehicle(firstCar)
        if( followerId == 0): continue
        elif( followerId < 0):
            firstCar = followerId
            break
        else:
            deltaDistance = myGetLongitudinalDistance(followerId, g_nHostVehicleID)
            if( deltaDistance < -g_dMaxFollowerDistance ): break
            lVehicleListInLane.append(followerId)
            firstCar = followerId

    return lVehicleListInLane


# 函数: myGetAllLanesInEdge
# 用途: 基于一个车道，获得edge中的所有车道
# [i]: laneID
# [o]: lanes[], laneDir[]
def myGetAllLanesInEdge(laneID):
    lLanes = []
    lLaneDir = []

    if( laneID == "" ): return (lLanes, lLaneDir)
    lLanes.append(laneID)
    lLaneDir.append(0)
    # 寻找左侧
    idx = -1
    targetLane = laneID
    while( targetLane!="" ):
        targetLane = myGetLeftLaneIDBasedLane(targetLane)
        if( targetLane!="" ):
            lLanes.append(targetLane)
            lLaneDir.append(idx)
            idx = idx - 1
    # 寻找右侧
    idx = 1
    targetLane = laneID
    while( targetLane!="" ):
        targetLane = myGetRightLaneIDBasedLane(targetLane)
        if( targetLane!="" ):
            lLanes.append(targetLane)
            lLaneDir.append(idx)
            idx = idx + 1
    return (lLanes, lLaneDir)


def myGetAllVehiclesWithHostVehicleReleated(laneID, maxLeaderDistance, maxFollowerDistance):
    lFollowers = []
    lLeaders = []
    if( laneID=="" ): return (lFollowers, lLeaders)
    vehicleListInLane = myGetVehiclesInLane(laneID)

    for idx in vehicleListInLane:
        if idx == g_nHostVehicleID:
            continue
        deltaDistance = myGetLongitudinalDistance( idx, g_nHostVehicleID )
        if( deltaDistance < -maxFollowerDistance ):
            continue
        elif( deltaDistance > maxLeaderDistance):
            continue
        else:
            if( deltaDistance < 0 ):    lFollowers.append(idx)
            else: lLeaders.append(idx)
    return (lFollowers, lLeaders)

def myGetLeaders(vehID, maxLeaderDistance = 99999999):
    lLeaders = []
    if( vehID < 0 ): return lLeaders
    vehicleListInlane = myGetVehiclesInLane(myGetLaneID(vehID))

    for idx in vehicleListInlane:
        deltaDistance = myGetLongitudinalDistance( vehID, idx )
        if( deltaDistance >= 0 ): continue
        else:
            lLeaders.append( idx )
    return lLeaders

def myGetAllToLanes(laneID):
    lToLanes = []
    dirs = myGetValidDirections(laneID)
    for dir in dirs:
        route = myGetNextLanes(laneID, dir)
        if( len(route) > 0 ):
            toLane = route[ len(route)-1 ]
            if( myCheckInternalLane(toLane) ):
                print( "Warning: an internal lane is regarded!" )
            else:
                lToLanes.append( toLane )
    return lToLanes

