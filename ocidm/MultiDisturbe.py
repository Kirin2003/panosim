"""
Auther: 
    SUN-Hao
Description:
    OCP based disturbe traffic model for Panosim
"""

import math
import time
import copy
import numpy as np

from TrafficModelInterface import *
from PanoSimTrafficAPI2 import *
from MultiDisturbeSceneManager import *
from multiprocessing.dummy import Pool as ThreadPool
from oc_idm.panosim.traffic_ocidm import idm_ctrl

g_dicVehicleStrategyThread = {}
g_dicVehicleTrajectory = {}
g_threadPool = 0
g_dicTrafficObjectRoute = {}

def ModelStart(userData):

    global g_dicVehicleStrategyThread, g_dicVehicleTrajectory, g_threadPool, g_dicTrafficObjectRoute
    g_dicVehicleStrategyThread = {}
    g_dicVehicleTrajectory = {}
    g_threadPool = 0
    g_dicTrafficObjectRoute = {}


    userData["phase"] = 0
    userData["SceneManager"] = myDisturbeSceneManager(userData)
    userData["TimeMaster"] = myTimeMaster()

    initLaneShapes()
    print("#############初始化OK============")
    g_threadPool = ThreadPool(processes=4)


def ModelOutput(userData, true=1):
    global g_dicVehicleStrategyThread
    global g_dicVehicleTrajectory
    global g_threadPool
    global g_dicTrafficObjectRoute
    # ----------------
    #print( "Host::\n", "X,Y:", myGetHostVehicleCoordinate(), "Speed:", myGetVehicleSpeed(0), "LaneID:", myGetLaneID(0)  )

    classSceneManager = userData["SceneManager"]
    classTimeMaster = userData["TimeMaster"]

    classTimeMaster.updateTimeStamp(userData["time"])

    if( classTimeMaster.getCurrentTimeStamp() < 50 ): return

    '''for id in getVehicleList():
        if( str(id) not in g_dicTrafficObjectRoute ):
            errorCode = mySetValidRoute(id)
            if errorCode:
                g_dicTrafficObjectRoute[str(id)] = myGetLaneID(id)
        else:
            laneID = myGetLaneID(id)
            if( g_dicTrafficObjectRoute[str(id)] != laneID ):
                errorCode = mySetValidRoute(id)
                if errorCode:
                    g_dicTrafficObjectRoute[str(id)] = myGetLaneID(id)'''


    timeStep = classTimeMaster.getDeltaTimeStep()

    #判断是否初始化成功
    if not classSceneManager.mySceneInitialization(userData):
        return

    #车辆序号为非主车,且不是全局头车的序号
    (vehicleList, disturbVehicleList, disturbRemoveList) = classSceneManager.mySceneUpdate()
    # print(disturbVehicleList)

    # ----------------------------------------------

    for id in disturbRemoveList:
        if (str(id) in g_dicVehicleStrategyThread):
            threadID = g_dicVehicleStrategyThread[str(id)]
            g_dicVehicleStrategyThread.pop(str(id))
        if (str(id) in g_dicVehicleTrajectory):
            g_dicVehicleTrajectory.pop(str(id))

    for id in disturbVehicleList:
        if( not getVehicleModelingControlRights(id) ):  # 如果临近交叉口，就跳过控制。 如果想做交叉口建模，需要删掉这条
            if( str(id) in g_dicVehicleStrategyThread ):
                threadID = g_dicVehicleStrategyThread[str(id)]
                g_dicVehicleStrategyThread.pop(str(id))
            if( str(id) in g_dicVehicleTrajectory ):
                g_dicVehicleTrajectory.pop(str(id))
            continue

        vehTraj = myTrajectory(id)
        if( str(id) in g_dicVehicleTrajectory ):
            vehTraj = g_dicVehicleTrajectory[str(id)]

        if( str(id) in g_dicVehicleStrategyThread ):  # 已经创建了一个线程
            threadID = g_dicVehicleStrategyThread[str(id)]
            if( threadID.ready() ):
                if( threadID.successful() ): # 线程已成功返回控制轨迹
                    vehTraj = threadID.get()
                    g_dicVehicleTrajectory[str(id)] = vehTraj
                    g_dicVehicleStrategyThread.pop( str(id) )
                else:  # 线程还没有结束，那就先按照当前状态行驶
                    pass
            else:  # 线程还没开始
                pass
        else:  # 当前还没有线程
            threadID = g_threadPool.apply_async(do, (id, classTimeMaster.getCurrentTimeStamp(), vehTraj))
            g_dicVehicleStrategyThread[str(id)] = threadID

        (vehID, x, y, yaw, speed) = myTrajectoryTracking(id, vehTraj.m_lTrajectory, classTimeMaster.getCurrentTimeStamp(), classTimeMaster.getDeltaTimeStep())
        if( x==None or y==None or yaw==None or speed==None ):
            pass
        else:
            myMoveTo(vehID, x, y, yaw)
            changeSpeed(vehID, speed, 1)
            pass

def ModelTerminate(userData):
    pass

# 函数： 根据轨迹，产生下一个步长的位置
# [i] vehID
# [i] trajectory [ [x1,y1,yaw1,speed1,t1], ..., [xn,yn,yawn,speedn,tn] ], 或者为空
# [i] curTime
# [i] deltaTime
# [o] (vehID, x, y, yaw)
def myTrajectoryTracking(vehID, trajectory, curTime, deltaTime):
    def defaultMoving():  # 用于没有轨迹时的行驶
        curYaw = myGetVehicleYaw(vehID)
        curX = myGetVehicleX(vehID)
        curY = myGetVehicleY(vehID)
        curSpeed = myGetVehicleSpeed(vehID)

        traj_in_vehicle = []
        #traj_in_vehicle.append([0, 0, 0, curSpeed, curTime])
        traj_in_vehicle.append([curSpeed * deltaTime / 1000, 0, 0, curSpeed, curTime + deltaTime])
        #print( "yaw:", curYaw, "curX", curX, "curY", curY )
        traj_in_world = myCoorTransFromVehicleToSUMOWorld(curX, curY, curYaw, curTime, traj_in_vehicle)

        x = traj_in_world[0][0]
        y = traj_in_world[0][1]
        yaw = traj_in_world[0][2]
        speed = curSpeed
        #print( "dstyaw:", yaw, "dstX", x, "dstY", y )
        return (vehID, x, y, yaw, speed)

    def linearInterpolation(trajectory, curTime):
        def calculateRatio(trajectory, curTime):
            sizeTraj = len(trajectory)
            idx = 0
            while idx < sizeTraj-1:
                time0 = trajectory[idx][4]
                time1 = trajectory[idx+1][4]
                if( curTime>=time0 and curTime<=time1 ):
                    ratio = (curTime-time0) / (time1-time0)
                    return (idx, ratio)
                idx = idx + 1
            return (None, None)

        (idx, ratio) = calculateRatio(trajectory, curTime)
        if( idx == None ):
            return ( None, None, None, None)
        else:
            if( trajectory[idx+1][0]==None or trajectory[idx][0]==None or trajectory[idx+1][1]==None or trajectory[idx][1]==None ):
                return ( None, None, None, None)
            newX = trajectory[idx][0] + ratio*(trajectory[idx+1][0]-trajectory[idx][0])
            newY = trajectory[idx][1] + ratio*(trajectory[idx+1][1]-trajectory[idx][1])
            newYaw = trajectory[idx][2] + ratio * (trajectory[idx + 1][2] - trajectory[idx][2])
            newSpeed = trajectory[idx][3] + ratio * (trajectory[idx + 1][3] - trajectory[idx][3])
            return ( newX, newY, newYaw, newSpeed)

    if( vehID <= 0 ): return (None, None, None, None)
    trajSize = len(trajectory)
    # 情况1：当前还没有可用的轨迹
    if( trajSize <= 1 ):
        #print("Tips: veh %d doesn't have trajectory now!" %vehID)
        return defaultMoving()

    # 情况2：当前时间已经超过轨迹的最大时间
    if( curTime > trajectory[trajSize-1][4] ):
        print("Tips: veh %d had exceeded the desired time!" %vehID)
        return defaultMoving()

    # 情况3：轨迹的时间戳要早于当前的车辆时间，理论上不可能
    if( curTime < trajectory[0][4] ):
        print("Warning: veh %d had an earlier timeStamp" %vehID)
        return defaultMoving()

    # 情况4：正常情况
    (newX, newY, newYaw, newSpeed) = linearInterpolation(trajectory, curTime)
    if (newX == None or newY == None or newYaw == None or newSpeed == None):
        return defaultMoving()
    else:
        return (vehID, newX, newY, newYaw, newSpeed)



# 线程返回的轨迹类
# [i] vehID
# [o] 需要设置预计的轨迹 [ [x1,y1,yaw1,speed1,t1], ..., [xn,yn,yawn,speedn,tn] ]
# [o] m_bTrajectoryAvailable 轨迹是否设置成功？ 最好在线程的最后赋值，不再做互锁了
# [o] m_dOldEnoughTime 可以是动态的值，返回这段轨迹可以在什么时候再次控制
#                      主线程会在这个时间后，再次调用线程开始决策
class myTrajectory():

    def __init__(self, vehID):
        self.m_nVehID = vehID
        self.__reset()

    def __reset(self):
        self.m_lTrajectory = []
        self.m_bTrajectoryAvailable = False
        self.m_dOldEnoughTime = -1.0

    m_nVehID = -1
    m_lTrajectory = []
    m_bTrajectoryAvailable = False
    m_dOldEnoughTime = -1.0


# 决策算法模板
#
def do(vehID, curTime, oldTraj) :
    def createObs(id,veh_x, veh_y, veh_yaw):
        def myCoorTrans(old_x, old_y, new_in_old_x, new_in_old_y, new_from_old_theta):
            new_x = (old_x - new_in_old_x) * np.cos(new_from_old_theta) + (old_y - new_in_old_y) * np.sin(
                new_from_old_theta)
            new_y = (old_y - new_in_old_y) * np.cos(new_from_old_theta) - (old_x - new_in_old_x) * np.sin(
                new_from_old_theta)
            return (new_x, new_y)
        if id < 0:
            obs = None
        else:
            vel = getVehicleSpeed(id)
            yaw = myGetVehicleYaw(id)
            x = getVehicleX(id)
            y = getVehicleY(id)
            x ,y = myCoorTrans(x,y,veh_x, veh_y, veh_yaw)
            yaw  -= veh_yaw
            obs = {"x": x, "y": y, "h": yaw, "v": vel,"id":id}
            return obs

    host_veh_lane_id = myGetLaneID(0)

    vel = getVehicleSpeed(vehID)
    yaw = myGetVehicleYaw(vehID)
    x = getVehicleX(vehID)
    y = getVehicleY(vehID)
    if x < -100000:
        print("Invalid vehicle state, waiting...")
        return
    #--------create traffic vehicle states dict--------------------
    state = {"x":x,"y":y,"h":yaw,"v":vel,"id":vehID}
    print("------------my state---------------:",state)
    
    #--------create road dict----------------
    speed_limit = 20
    right_lane_id = myGetRightLaneID(vehID)
    right_lane = myGetLaneShape(right_lane_id)

    current_lane_id = myGetLaneID(vehID)
    current_lane = myGetLaneShape(current_lane_id)

    left_lane_id = myGetLeftLaneID(vehID)
    left_lane = myGetLaneShape(left_lane_id)

    road = {'left_lane':left_lane,
    'right_lane':right_lane,
    'current_lane':current_lane,
    'speed_limit':speed_limit}
    #--------create env dict  ---------------
    id = getLeaderVehicle(vehID)
    leader = createObs(id,x,y,yaw)
    id = getLeftLeaderVehicle(vehID)
    left_leader = createObs(id,x,y,yaw)
    id = getLeftFollowerVehicle(vehID)
    left_follower = createObs(id,x,y,yaw)
    id = getRightLeaderVehicle(vehID)
    right_leader = createObs(id,x,y,yaw)
    id = getRightFollowerVehicle(vehID)
    right_follower = createObs(id,x,y,yaw)
    env = {'leader':leader,
    'left_leader': left_leader,
    'left_follower':left_follower,
    'right_leader':right_leader,
    'right_follower':right_follower}
    #--------------------------
    better_flag = None
    if host_veh_lane_id == right_lane_id:
        better_flag = 'right'
    if host_veh_lane_id == current_lane_id:
        better_flag = 'current'
    if host_veh_lane_id == left_lane_id:
        better_flag = 'left'

    print("--------",better_flag)
    idm_traj = idm_ctrl(state=state,road = road ,env=env, better_flag=better_flag)
    #--------------------------
    traj = myTrajectory(vehID)   # ! 必须赋值
    traj.m_lTrajectory = myCoorTransFromVehicleToSUMOWorld(x, y, yaw, curTime, idm_traj)
    traj.m_dOldEnoughTime = curTime + 2000  # !  必须赋值
    traj.m_bTrajectoryAvailable = True   # !  必须赋值
    return traj





