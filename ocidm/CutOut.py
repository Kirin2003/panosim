# 基于三车的状态进行测试评价：第一辆车是静止的事故车或者交通标志，第二辆车会有切出的动作，后面的第三辆车是测试主车
# 场景触发背景：第一辆车在某个车道的某个位置抛锚或者慢速行驶，主车跟随第二辆车进行跟驰动作，当第二辆车遇到故障车辆时执行换道动作，第三辆由自动驾驶算法控制
# 场景触发参数：  背景车辆设置：edge lane s l x y ;将第二辆车与第三辆车设置成同速度、同路径、同一时刻触发、两者间距；一二间隔为触发换道，左右换道



import math
from TrafficModelInterface import *


def ModelStart(userData):
    userData["dropS"], userData["dropL"], userData["dropSpeed"],\
    userData["CutOutDepartS"], userData["CutOutDepartL"], userData["CutOutDepartSpeed"],\
    userData["CutOutDirection"], userData["CutOutForwardDistance"], userData["CutOutFinishSpeed"], \
    userData["CutOutEgoTime"], userData["CutOutEgoStation"], userData["CutOutEgoSpeed"], \
        = map(float, userData["parameters"]["Parameters"].split(','))

    userData["phase"] = 0

    print("初始化OK")

    # 160,0.1,0,10,0.1,11.11,-1,36,5,40000,180,45
    # 换道方向  左负右正


def ModelOutput(userData, true=1):
    # 添加故障车和中间车辆
    if userData["phase"] == 0:

        # 必须在主车发车后其他车辆才有相对主车发车的资格  目前测试至少要在两个线程20ms后才能执行
        if userData["time"] >= 4000:

            userData["DropObjectId"] = addVehicleRelated(0, userData["dropS"], userData["dropL"], userData["dropSpeed"],
                                                         lane_type.current, vehicle_type.Van, 9)
            userData["CutOutObjectId"] = addVehicleRelated(0, userData["CutOutDepartS"], userData["CutOutDepartL"],
                                                           userData["CutOutDepartSpeed"], lane_type.current,
                                                           vehicle_type.OtherVehicle, )

            userData["phase"] += 1

            print(userData["CutOutObjectId"])
            print(userData["DropObjectId"])
            print("发出车辆ID")

    # 维持发车速度  判断是否换道
    if userData["phase"] == 1:

        changeSpeed(userData["DropObjectId"], userData["dropSpeed"], 0.0)
        print("DropObjchangeSpeedOK")
        # changeSpeed(userData["DropObjectId"], 0.0, 0.0)
        # changeSpeed(5,0,0)
        changeSpeed(userData["CutOutObjectId"], userData["CutOutDepartSpeed"], 0.0)
        print("CutOutObjchangeSpeedOK")
        print(userData["CutOutObjectId"])
        print(getVehicleX(userData["CutOutObjectId"]))

        CutoutX = getVehicleX(userData["CutOutObjectId"])
        CutoutY = getVehicleY(userData["CutOutObjectId"])
        DropX = getVehicleX(userData["DropObjectId"])
        DropY = getVehicleY(userData["DropObjectId"])
        distanceBetweenDropCutout = math.pow((CutoutX - DropX), 2) + math.pow((CutoutY - DropY), 2)
        distanceBetweenDropCutout = math.sqrt(distanceBetweenDropCutout)

        #print(distanceBetweenDropCutout)

        if distanceBetweenDropCutout < userData["CutOutForwardDistance"] or \
                userData["time"] >= userData["CutOutEgoTime"] or getTotalDistance(0) >= userData["CutOutEgoStation"] or \
                getVehicleSpeed(0) >= userData["CutOutEgoSpeed"]:

            userData["phase"] += 1
            print("车辆变道触发OK")

            if userData["CutOutDirection"] == -1:
                changeLane(userData["CutOutObjectId"], change_lane_direction.left)

            if userData["CutOutDirection"] == 1:
                changeLane(userData["CutOutObjectId"], change_lane_direction.right)

            userData["CutoutCurrentTime"] = userData["time"]

    # 变道期间维持速度
    if userData["phase"] == 2:
        changeSpeed(userData["DropObjectId"], userData["dropSpeed"], 0.0)
        changeSpeed(userData["CutOutObjectId"], userData["CutOutDepartSpeed"], 0.0)
        # print("车速维持OK")
        if userData["time"] - userData["CutoutCurrentTime"] >= 3000:
            userData["phase"] += 1

    if userData["phase"] == 3:
        changeSpeed(userData["DropObjectId"], userData["dropSpeed"], 0.0)
        changeSpeed(userData["CutOutObjectId"], userData["CutOutFinishSpeed"], 0.0)
        #print("finish车速维持OK")


def ModelTerminate(userData):
    pass