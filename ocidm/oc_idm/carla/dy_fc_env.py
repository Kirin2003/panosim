# -*- coding: UTF-8 -*-
#!/usr/bin/env python

# Copyright (c), SUN-Hao  dr.sunhao@outlook.com
#

import glob
import os
import sys

try:
    sys.path.append(glob.glob('../../carla_env/utils/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    print('cannot append carla egg file path!')
    raise RuntimeError('egg error')

import carla
import random

try:
    import numpy as np
except ImportError:
    raise RuntimeError('cannot import numpy, make sure numpy package is installed')

try:
    import queue
except ImportError:
    import Queue as queue


class CarlaSyncMode(object):
    def __init__(self, world, *sensors, **kwargs):
        self.world = world
        self.sensors = sensors
        self.frame = None
        self.delta_seconds = 1.0 / kwargs.get('fps', 20) # 20: default
        self._queues = []
        self._settings = None

    def __enter__(self):
        self._settings = self.world.get_settings()
        self.frame = self.world.apply_settings(carla.WorldSettings(
            no_rendering_mode=False,
            synchronous_mode=True,
            fixed_delta_seconds=self.delta_seconds))
        # self.frame type: int

        def make_queue(register_event):  # make_queue ，参数是一个函数A，函数A的参数为函数B，make_queue的作用是将函数B传入到函数A中
            q = queue.Queue()
            register_event(q.put) # 注册回调函数  q.put()
            self._queues.append(q)

        make_queue(self.world.on_tick) #  on_tick 注册函数
        for sensor in self.sensors:
            make_queue(sensor.listen) #sensor.listen 注册函数
        return self

    def tick(self, timeout):
        self.frame = self.world.tick() #
        data = [self._retrieve_data(q, timeout) for q in self._queues]
        assert all(x.frame == self.frame for x in data)
        return data

    def __exit__(self, *args, **kwargs):
        #self._settings.synchronous_mode = False
        #self.world.apply_settings(self._settings) # 重置虚拟环境配置
        self.world.apply_settings(carla.WorldSettings(
            no_rendering_mode=False,
            synchronous_mode=False,
            fixed_delta_seconds=self.delta_seconds))

    def _retrieve_data(self, sensor_queue, timeout):
        while True:
            data = sensor_queue.get(timeout=timeout)
            if data.frame == self.frame:
                return data


import rospy
from carla_env.utils.sensor import RefPathSensor
from carla_env.utils.sensor import CSVManager
import carla_env.ros.rviz_api as viz_api
from oc_idm.idm_ctrller.idm import Actuator
from oc_idm.idm_ctrller.idm import VehicleConf
import carla_env.utils.rsh_math as cal
from oc_idm.idm_ctrller.idm_dy_fc import DynamicIDM
from carla_env.ros.rviz_api import circle_to_marker
import time


def main():

    data_mnger =CSVManager('/home/sunhao/dy_fc_env.csv')
    # init ros node
    #rospy.init_node('idm', anonymous=True)
    rviz_api = viz_api.RvizApi()
    img_api = viz_api.VizRGBImgApi('rgb_img_2')
    marker_api = viz_api.VizMarkerApi()
    # ---load refernce map----------------------
    print("loading reference map")
    dir_path = os.path.dirname(__file__)
    dir_path = os.path.dirname(dir_path)
    dir_path = os.path.dirname(dir_path)
    # save_data = os.path.join(dir_path,"resources/training_data/2021-03-10-01.csv")
    file = os.path.join(dir_path, "resources/map/town05_07.csv")
    rf_sensor = RefPathSensor(file)
    #------------------------
    actuator =Actuator()

    #----init carla------------
    print("linking to carla engine")
    client = carla.Client('127.0.0.1', 2000)
    # client = carla.Client('10.11.50.160', 2000)
    client.set_timeout(10.0) #
    world = client.load_world('Town05')
    simulation_fps = 30 #  仿真器更新频率
    control_delta_time = 0.1 #
    actor_list = [] #
    try:
        m = world.get_map() # 得到 XODR格式的地图，该函数很耗时，
        blueprint_library = world.get_blueprint_library() # Done, get blueprint library
        #--------spawn a host vehicle -----------------
        print("spawn ego vehicle")
        pts =  m.get_spawn_points() # 得到推荐车辆生成点列，type: carla.Transform, pts type: list
        start_pose = random.choice(pts)   # choice() 方法返回一个列表，元组或字符串的随机项。
        vehicle = world.spawn_actor(
            random.choice(blueprint_library.filter("model3")),
            start_pose ) # Done，生成一个车辆 actor
        vehicle.set_simulate_physics(False) #  让车辆动力学起作用
        actor_list.append(vehicle)  # actor 单独管理
        # ----设置车辆固定初始位置----------------------
        cfg = rf_sensor.getInitPos(1) # Note: get right hand z-up pt
        start_location = carla.Location(cfg.x,-cfg.y,0)# transfrom to left hand z-up
        waypoint = m.get_waypoint(start_location) # Waypoint 自动定位到最近的waypoint上去了。
        vehicle.set_transform(waypoint.transform) # 设定车辆在carla中的位置，注意，此处为初始位置

        #-------spawn a traffic vehicle----------------
        print("spwaning a static traffice vehicle")
        pts = m.get_spawn_points()  # 得到推荐车辆生成点列，type: carla.Transform, pts type: list
        start_pose = random.choice(pts)  # choice() 方法返回一个列表，元组或字符串的随机项。
        traffic_veh = world.spawn_actor(
            random.choice(blueprint_library.filter("model3")),
            start_pose ) # Done，生成一个车辆 actor
        traffic_veh.set_simulate_physics(False) #  让车辆动力学起作用
        actor_list.append(traffic_veh)  # actor 单独管理

        cfg = rf_sensor.getInitPos(30) # Note: get right hand z-up pt
        start_location = carla.Location(cfg.x,-cfg.y,0)# transfrom to left hand z-up
        waypoint = m.get_waypoint(start_location) # Waypoint 自动定位到最近的waypoint上去了。
        traffic_veh.set_transform(waypoint.transform) # 设定车辆在carla中的位置，注意，此处为初始位置

        # ------set map and initial states for ros rviz--------------------
        rviz_api.set_map(rf_sensor.data[:, 0], rf_sensor.data[:, 1])
        rviz_api.set_veh_states(waypoint.transform.location.x,-waypoint.transform.location.y,
                                np.deg2rad(-waypoint.transform.rotation.yaw))
        #----add camera-----------------------------------
        print('add rgb camera')
        camera_rgb = world.spawn_actor(
            blueprint_library.find('sensor.camera.rgb'),
            carla.Transform(carla.Location(x=-5.5,y=0, z=3), carla.Rotation(pitch=-15)),
            attach_to=vehicle)
        actor_list.append(camera_rgb)
        #-----create idm----------------
        print("create intelligent driver")
        mpc_dt = 0.2
        ctrl_size = 15
        pred_size = 15
        args = {'ctrl_horizion': ctrl_size,
                'pred_horizion': pred_size,
                'ts': mpc_dt}
        idm = DynamicIDM(**args)
        # -------------------------------------
        init_flag = False
        veh_cfg = VehicleConf()
        veh_cfg.x = waypoint.transform.location.x
        veh_cfg.y = -waypoint.transform.location.y
        veh_cfg.yaw = -np.deg2rad(waypoint.transform.rotation.yaw)
        veh_cfg.vel = 5
        #next_veh_cfg = veh_cfg
        t0 = 0
        # Create a synchronous mode context.
        with CarlaSyncMode(world, camera_rgb,fps=simulation_fps) as sync_mode:
            while not rospy.is_shutdown():
                # Advance the simulation and wait for the data.
                snapshot, image_rgb = sync_mode.tick(timeout=2.0)
                current_time = image_rgb.timestamp
                # ---------------------------------
                if not init_flag:
                    init_flag = True
                    t0 = image_rgb.timestamp
                    continue
                #---------update time stamp-------------
                t1 = image_rgb.timestamp
                dt = t1-t0
                #----------------------------------
                veh_trans = vehicle.get_transform()
                if np.fabs(dt-control_delta_time) < 0.0001 or dt > control_delta_time:
                    # 保证在控制周期附近可以控制一次车辆
                    t0 = t1
                    #-------------------------------------
                    wp = m.get_waypoint(veh_trans.location)
                    veh_trans.location.z = wp.transform.location.z
                    veh_trans.rotation.pitch = wp.transform.rotation.pitch
                    veh_trans.rotation.roll = wp.transform.rotation.roll

                    veh_cfg.x = veh_trans.location.x
                    veh_cfg.y = -veh_trans.location.y
                    veh_cfg.yaw = -np.deg2rad(veh_trans.rotation.yaw)
                    # ----------get reference path--------------------------
                    cfg.x = veh_cfg.x
                    cfg.y = veh_cfg.y
                    cfg.h = veh_cfg.yaw
                    ds = veh_cfg.vel*mpc_dt
                    lx,ly = rf_sensor.getRefPath(cfg, in_ds=ds, in_pt_num=pred_size)
                    #-----------get traffic veh states---------------------
                    tv_trans = traffic_veh.get_transform()
                    tv_pos = tv_trans.location
                    tv_x,tv_y = cal.trans2DPt(tv_pos.x, - tv_pos.y, veh_cfg.x,veh_cfg.y,veh_cfg.yaw)
                    #--------oc_idm solver---------------
                    obs = {'x':tv_x,'y':tv_y,'h':  (-veh_cfg.yaw +  np.deg2rad(-tv_trans.rotation.yaw) ) ,'v':0.1 }
                    t1 = time.time()
                    cmds,traj,circle_x,circle_y = idm.run(vel=np.array(veh_cfg.vel),ref_x=lx,ref_y=ly,ref_vel=20,env=[obs])
                    t2 = time.time()
                    #----------update actuator------------
                    veh_cfg.t = current_time
                    actuator.reset()
                    actuator.set_new_conditions(traj=traj,conf=veh_cfg)
                    #---------rviz---------------
                    rviz_api.pub_map()
                    rviz_api.pub_veh( cfg.x,cfg.y,cfg.h)
                    rviz_api.pub_ref_path(lx,ly)
                    rviz_api.pub_trajectroy(traj)
                    img_api.pub_img(image_rgb)

                    k =0
                    for i in range(circle_x.shape[0]):
                        for j in range(circle_y.shape[1]):
                            marker = circle_to_marker(x = circle_x[i,j],y = circle_y[i,j],frame_id='veh',id=k)
                            marker_api.pub_marker(marker)
                            k =k +1

                    #--------save data------------------------
                    t =t2 -t1
                    dy = ly[0]
                    s = np.sqrt( tv_x*tv_x + tv_y*tv_y )
                    data_mnger.add_data([[t,dy,s]])
                    #---print-------------
                    # print(circle_y.shape)
                    # print("circle num:",k)
                    print("time consuming:", t2 - t1)
                    # print ('set',veh_cfg.x, veh_cfg.y, veh_cfg.yaw)
                    # print traj
                # ---------set vehicle transform to carla--------------
                if actuator.trajectory is None :
                    continue
                else:
                    veh_cfg_next = actuator.get_conf(current_time)
                    veh_trans.location.x = veh_cfg_next.x
                    veh_trans.location.y = -veh_cfg_next.y
                    veh_trans.rotation.yaw = -np.rad2deg(veh_cfg_next.yaw)
                    vehicle.set_transform(veh_trans)
                    veh_cfg.vel = veh_cfg_next.vel

    finally:
        print('destroying actors.')
        for actor in actor_list:
            actor.destroy()
        print('done.')


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('\nCancelled by user. Bye!')
