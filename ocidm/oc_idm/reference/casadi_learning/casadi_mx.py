import casadi as ca


x = ca.SX.sym('x',2,2)
y = ca.SX.sym('y')
print(x)
print(y)
print('-----------------')
f = 3*x + y
print(f)
print(f.shape)


print('-----------------')
x = ca.MX.sym('x',2,2)
y = ca.MX.sym('y')
print(x)
print(x.shape)
print(y)

print('-----------------')
f = 3*x + y
print(f)
print(f.shape)

print('-----------------')
x = ca.MX.sym('x',2,2)
print(x[1,1])

print('-----------------')
x = ca.MX.sym('x',2)
A = ca.MX(2,2)
print('A:', A)
A[0,0] = x[0]
A[1,1] = x[0]+x[1]
print('A:', A)

print('-----------------')
M = ca.SX([[3,7],[4,5]])
M[0,:][0,0] = 1
print(M)


x = ca.SX.sym('x',5)
y = ca.SX.sym('y',5)
print(ca.vertcat(x,y).shape)

