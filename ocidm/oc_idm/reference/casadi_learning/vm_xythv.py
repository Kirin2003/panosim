"""

vehicle model

states:

x,y,theta,vel

control inputs:

omega, ax

"""

import casadi as ca
import numpy as np
import matplotlib.pyplot as plt

if __name__=="__main__":
    # discrete time Ts
    ts = 0.2

    # vehicle states
    x = ca.SX.sym('x')  # x坐标
    y = ca.SX.sym('y')  # y坐标
    theta = ca.SX.sym('theta')  # 车辆航向
    vel = ca.SX.sym('vel')  # 速度
    states = ca.vertcat(x, y, theta, vel)

    # control inputs
    omega = ca.SX.sym('omega')
    ax = ca.SX.sym('ax')
    inputs = ca.vertcat(omega, ax)

    x_next = x + ts * vel * ca.cos(theta)
    y_next = y + ts * vel * ca.sin(theta)
    theta_next = theta + ts * omega
    vel_next = vel + ts * ax
    states_next = ca.vertcat(x_next, y_next, theta_next, vel_next)

    vm_func = ca.Function('vm_func', [states, inputs], [states_next], ['states', 'inputs'], ['states_next'])

    print(vm_func)
    #-------------------------------------

    x0 = 0
    y0 = 0
    theta0 =0
    vel0 =10
    states = [x0, y0, theta0, vel0]

    omega = 0.05
    ax = 1
    cmd = [omega,ax]

    res1 = vm_func(states=states,inputs=cmd)
    print('res1:',res1['states_next']) # return dict

    res2 = vm_func(states,cmd)
    print('res2:',res2) # return num

    # -------------------------------------

    cmds = np.zeros((20,2),dtype=np.float32)
    cmds[0:10,0]=0.5 # index from 0 to 9
    cmds[10:20, 0] = -0.5 # index from 10 to 19
    print(cmds)

    path = []
    x= []
    y = []
    theta = []
    vel = []
    for i in range(len(cmds)):
        cmd = cmds[i,:]
        res = vm_func(states=states,inputs=cmd)
        states = res['states_next']
        path.append(states)
        x.append(states[0])
        y.append(states[1])
        theta.append(states[2])
        vel.append(states[3])
    print(path)

    #------------------------------
    plt.plot(x,y)
    plt.show()

    plt.plot(theta)
    plt.show()

    plt.plot(vel)
    plt.show()


