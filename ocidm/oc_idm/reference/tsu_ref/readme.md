# README

一些说明

## vehicle.py

```python
# vehicle dynamics state transfer model
Vmodel = ca.Function('Vmodel', [states, controls], [rhs], ['input_state', 'control_input'], ['rhs'])
```

CasADi框架下的车辆预测模型，就是一个CasADi函数类型，一般不需要修改。



## util.py



### Class - NMPC

非线性MPC控制器

关键看两个成员函数

- `NMPC.constraintFunction`

  构造 g(X,U) >= 0的约束形式，在`NMPC.nmpc_singletrack`中被调用，返回值不需要关心

  输入 `env_predict`: list of dict

  例子

  ```python
  env_predict = [
      # 'c': 圆心坐标    'r': 半径
      {
          'c':[np.array([1,1]), np.array([2,2]), ...] # 列表长度不超过Np，类型为np.ndarray
          'r':1.5   # 若上述列表长度小于Np，则默认列表最后一个元素为第Np步的预测
      },
      {
          'c':[np.array([1,1]), np.array([2,2]), ...] 
          'r':1.5
      }   # 值得注意的是，一辆车需要两个字典才能描述，一个字典只代表双圆中的一个圆在Np步长内的状态
  ]
  ```

- `NMPC.nmpc_singletrack`

  输入：

  `x0`: np.ndarray   形状： (n_state, )，即三自由度模型中的六个状态。

  `u0`: 控制量的初值猜测，形状：(n_control, Nc)，第一次可以给全0，之后使用该函数返回值（字典类型）中 'u_next'对应的值即可。

  `trajectory`: np.ndarray   形状：(Np, 2)，期望轨迹，平面点对即可，一共Np个。

  `env_constraint`: 默认None，即不考虑周车。需要考虑周车时传入上述`env_predict`样式的list即可。

  

  返回值：

  `result`: dict

  ```python
  {
      'u_c': # 求解得到的单步控制量,
      'u_next': # 下一次调用此函数，可以将这个值直接输入给u0参数,作为一个合理的优化变量猜测
      'cost': # 计算得到的损失函数值
  }
  ```

  