#!/usr/bin/env python

# Copyright (c), SUN-Hao  dr.sunhao@outlook.com
#



"""
1：get current velocity,
2：set desired velocity in local space
3: get reference path'
4: add osqp mpc solver
"""

import glob
import os
import sys

try:
    sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    print('cannot append carla egg file path!')
    pass

import carla
import random

try:
    import pygame
except ImportError:
    raise RuntimeError('cannot import pygame, make sure pygame package is installed')

try:
    import numpy as np
except ImportError:
    raise RuntimeError('cannot import numpy, make sure numpy package is installed')

try:
    import queue
except ImportError:
    import Queue as queue

from util import NMPC

class CarlaSyncMode(object):
    def __init__(self, world, *sensors, **kwargs):
        self.world = world
        self.sensors = sensors
        self.frame = None
        self.delta_seconds = 1.0 / kwargs.get('fps', 20)
        self._queues = []
        self._settings = None

    def __enter__(self):
        self._settings = self.world.get_settings()
        self.frame = self.world.apply_settings(carla.WorldSettings(
            no_rendering_mode=False,
            synchronous_mode=True,
            fixed_delta_seconds=self.delta_seconds))
        # self.frame type: int

        def make_queue(register_event):  # make_queue ，参数是一个函数A，函数A的参数为函数B，make_queue的作用是将函数B传入到函数A中
            q = queue.Queue()
            register_event(q.put) # 注册回调函数  q.put()
            self._queues.append(q)

        make_queue(self.world.on_tick) #  on_tick 注册函数
        for sensor in self.sensors:
            make_queue(sensor.listen) #sensor.listen 注册函数
        return self

    def tick(self, timeout):
        self.frame = self.world.tick() #
        data = [self._retrieve_data(q, timeout) for q in self._queues]
        assert all(x.frame == self.frame for x in data)
        return data

    def __exit__(self, *args, **kwargs):
        #self._settings.synchronous_mode = False
        #self.world.apply_settings(self._settings) # 重置虚拟环境配置
        self.world.apply_settings(carla.WorldSettings(
            no_rendering_mode=False,
            synchronous_mode=False,
            fixed_delta_seconds=self.delta_seconds))

    def _retrieve_data(self, sensor_queue, timeout):
        while True:
            data = sensor_queue.get(timeout=timeout)
            if data.frame == self.frame:
                return data


def draw_image(surface, image, blend=False):
    array = np.frombuffer(image.raw_data, dtype=np.dtype("uint8"))
    array = np.reshape(array, (image.height, image.width, 4))
    array = array[:, :, :3]
    array = array[:, :, ::-1]
    image_surface = pygame.surfarray.make_surface(array.swapaxes(0, 1))
    if blend:
        image_surface.set_alpha(100)
    surface.blit(image_surface, (0, 0))


def get_font():
    fonts = [x for x in pygame.font.get_fonts()]
    default_font = 'ubuntumono'
    font = default_font if default_font in fonts else fonts[0]
    font = pygame.font.match_font(font)
    return pygame.font.Font(font, 14)


def should_quit():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            return True
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_ESCAPE:
                return True
    return False

#--------------------------------------------
#  user import

import math
from carla_env.utils.sensor import RefPathSensor
from carla_env.utils.ctrller import control as mpc_control
from carla_env.utils.sensor import CSVManager



def main():
    actor_list = []
    pygame.init()

    display = pygame.display.set_mode(
        (800, 600),
        pygame.HWSURFACE | pygame.DOUBLEBUF)
    font = get_font()
    clock = pygame.time.Clock() # pygame 里的  time.Clock()

    client = carla.Client('127.0.0.1', 2000)
    client.set_timeout(2.0) #
    simulation_fps = 30
    simulation_delta_time = 1 / simulation_fps
    control_delta_time = 0.1

    world = client.get_world()

    try:

        m = world.get_map() # 得到 XODR格式的地图，该函数很耗时，
        pts =  m.get_spawn_points() # 得到推荐车辆生成点列，type: carla.Transform, pts type: list
        start_pose = random.choice(pts)   # choice() 方法返回一个列表，元组或字符串的随机项。

        blueprint_library = world.get_blueprint_library() # Done, get blueprint library
        vehicle = world.spawn_actor(
            random.choice(blueprint_library.filter("model3")),
            start_pose ) # Done，生成一个车辆 actor
        vehicle.set_simulate_physics(True) #  让车辆动力学起作用
        actor_list.append(vehicle)  # actor 单独管理

        # ----设置车辆固定初始位置----------------------
        dir_path = os.path.dirname(__file__)
        dir_path = os.path.dirname(dir_path)
        dir_path = os.path.dirname(dir_path)
        save_data = os.path.join(dir_path,"resources/training_data/2021-03-10-01.csv")
        file = os.path.join(dir_path,"resources/map/new_map3.csv")
        rf_sensor = RefPathSensor(file)
        cfg = rf_sensor.getInitPos(20) # Note: get right hand z-up pt
        start_location = carla.Location(cfg.x,-cfg.y,0)# transfrom to left hand z-up
        waypoint = m.get_waypoint(start_location) # Waypoint 自动定位到最近的waypoint上去了。
        vehicle.set_transform(waypoint.transform)

        #----add camera-----------------------------------
        camera_rgb = world.spawn_actor(
            blueprint_library.find('sensor.camera.rgb'),
            carla.Transform(carla.Location(x=2,y=0, z=1), carla.Rotation(pitch=-15)),
            attach_to=vehicle)
        actor_list.append(camera_rgb)

        # -----set vehicle speed------------------------
        des_vel = carla.Vector3D(5,0,0) # speed: ms, local space
        vehicle.enable_constant_velocity(des_vel)

        #-------add recorder----------------------
        recorder = CSVManager(save_data)

        #-----------------------------------
        time_recoder = 0
        cmd = carla.VehicleControl()
        # Create a synchronous mode context.
        with CarlaSyncMode(world, camera_rgb,fps=simulation_fps) as sync_mode:
            while True:
                if should_quit():
                    return
                clock.tick() # pygame的时钟更新

                # Advance the simulation and wait for the data.
                snapshot, image_rgb = sync_mode.tick(timeout=2.0)
                time_recoder = time_recoder + simulation_delta_time

                if np.fabs(time_recoder-control_delta_time) < 0.0001:
                    # control freq = 10Hz
                    #----------get reference path--------------------------
                    vel = vehicle.get_velocity()
                    speed = math.sqrt(vel.x ** 2 + vel.y ** 2 + vel.z ** 2)
                    veh_location = vehicle.get_location()
                    yaw = vehicle.get_transform().rotation.yaw
                    cfg.x = veh_location.x
                    cfg.y = - veh_location.y # transform to right-hand
                    cfg.h = np.deg2rad(-yaw)

                    #-------mpc control-------------------
                    dt = 0.10
                    pred_size = 15
                    ds = speed*dt
                    lx,ly = rf_sensor.getRefPath(cfg, in_ds=ds, in_pt_num=pred_size)
                    cmd_omega = mpc_control(speed=speed, ref_y=ly, pred_size=pred_size, dt=dt, w_u=5.0, w_y=1.0)

                    #------------------------------------
                    delta = -np.rad2deg( 2.5 * cmd_omega[0] / speed )# transfrom to front wheel angle
                    steering = delta / 75
                    cmd.steer = steering

                    time_recoder = 0

                    #-------record data-------------
                    this_data = []
                    this_data.append(steering)
                    this_data.append(speed)
                    this_data.append(cfg.x)
                    this_data.append(cfg.y)
                    this_data.append(cfg.h)
                    for i in range(len(ly )):
                        this_data.append( ly[i] )
                    #print(this_data)
                    #print("delta time:", time_recoder)
                    recorder.add_data([this_data])


                # -------------------------------
                vehicle.apply_control(cmd)
                # --------------------------------------
                fps = round(1.0 / snapshot.timestamp.delta_seconds)# 计算仿真服务器的fps
                # Draw the display.--------------------------
                draw_image(display, image_rgb)
                #draw_image(display, image_semseg, blend=True)  # blend
                display.blit(
                    font.render('% 5d FPS (real)' % clock.get_fps(), True, (255, 255, 255)),
                    (8, 10)) # pygame的fps
                display.blit(
                    font.render('% 5d FPS (simulated)' % fps, True, (255, 255, 255)),
                    (8, 28)) # simulator的 FPS
                pygame.display.flip()

    finally:

        print('destroying actors.')
        for actor in actor_list:
            actor.destroy()

        pygame.quit()
        print('done.')


if __name__ == '__main__':

    try:
        main()

    except KeyboardInterrupt:
        print('\nCancelled by user. Bye!')
