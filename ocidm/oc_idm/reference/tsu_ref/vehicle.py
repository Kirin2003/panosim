from casadi.casadi import cos, sin
import casadi as ca

# ---------vehicle model--------
# vehicle predictive model in casadi
# parameter
Ts = 0.1
# Rw = 0.325
m = 1412.0
lf = 1.06
lr = 1.85
kf = -128915.5
kr = -85943.6
Iz = 1536.7
G = 9.81
mu = 0.85

# state variable
x = ca.SX.sym('x') # 全局x坐标
y = ca.SX.sym('y') # 全局y坐标
u = ca.SX.sym('u') # 纵向速度
v = ca.SX.sym('v') # 横向速度
phi = ca.SX.sym('phi') # 车头朝向 
omega = ca.SX.sym('omega') # 角速度
states = ca.vertcat(x, y, u, v, phi, omega)
n_state = states.size()[0]
# print(states)

# control variable
a = ca.SX.sym('a')
delta = ca.SX.sym('delta')
controls = ca.vertcat(a, delta)
n_control = controls.size()[0]

# discrete dynamics model  (backward Euler method)
# form: state-space model  x(k+1) = F(x(k), u(k)) # TODO confirm
# x -- states;  u -- controls
x_next = x + Ts * (u * cos(phi) - v * sin(phi))
y_next = y + Ts * (v * cos(phi) + u * sin(phi))
u_next = u + Ts * a
v_next = (m*v*u/Ts + (lf*kf-lr*kr)*omega - kf*delta*u - m*u*u*omega)/(m*u/Ts - (kf+kr))
phi_next = phi + Ts * omega
omega_next = (Iz*omega*u/Ts + (lf*kf-lr*kr)*v - lf*kf*delta*u) / (Iz*u/Ts - (lf**2*kf+lr**2*kr))
rhs = ca.vertcat(x_next, y_next, u_next, v_next, phi_next, omega_next)

# vehicle dynamics state transfer model
Vmodel = ca.Function('Vmodel', [states, controls], [rhs], ['input_state', 'control_input'], ['rhs'])

if __name__ == "__main__":
    pass
