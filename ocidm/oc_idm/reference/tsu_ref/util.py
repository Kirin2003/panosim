import casadi as ca
from casadi import cos, sin
import numpy as np
from oc_idm.reference.tsu_ref.vehicle import Vmodel
from oc_idm.reference.tsu_ref.vehicle import n_control, n_state

# ----------- 一些参数 ------------
# 可以写到类的成员变量中，看个人需要

r_ego = 1.5          # 自车轮廓经过双圆变换后的圆半径，我的车尺寸为4.8 * 1.8，故圆半径1.5m
height = 4.8 / 4     # 自车长度 / 4, 用于自车双圆坐标的推导
safety_margin = 3    # 碰撞检测中，两圆心距离平方 D^2 应大于等于两圆半径和的平方 (r1+r2)^2 + safety_margin
Np = 20              # 预测时域
Nc = 5               # 控制时域

class NMPC:

    def __init__(self) -> None:

        # ---------MPC parameter--------
        ## penalty matrix   # user define
        Q = np.array([
            [1.0, 0],
            [0, 1.0]
        ])
        R = np.array([
            [5, 0],
            [0, 40]
        ])

        # ----------Variables-----------
        U = ca.SX.sym('U', n_control, Nc)
        X = ca.SX.sym('X', n_state, Np+1)
        P = ca.SX.sym('P', n_state + 2 * Np)

        # -----------States-------------
        X[:, 0] = P[:n_state]
        for i in range(Np):
            if i < Nc:
                x_next = Vmodel(X[:, i], U[:, i])
            else:
                x_next = Vmodel(X[:, i], U[:, -1])
            X[:, i+1] = x_next
        self.State = X
        self.Control = U
        self.u_p = 0
        # -----Function: [U, P]-> X-----
        # 暂时用不到
        #F_state = ca.Function('f_state', [U, P], [X], ['input', 'target'], ['horizon_state'])

        # ------------Object------------
        obj = 0   # objetive function
        T = P[n_state:].reshape((2, -1)) # reference trajectory

        for i in range(1, Np+1):
            obj = obj + ca.mtimes([(X[:2, i] - T[:, i-1]).T, Q, X[:2, i] - T[:, i-1]])
        
        for j in range(Nc):
            obj = obj + ca.mtimes([U[:, j].T, R, U[:, j]])

        # -----Control constraints------
        self.lbx = list()
        self.ubx = list()
        a_max = 2.5
        delta_max = np.pi / 18
        for _ in range(Nc):
            self.lbx.append(-a_max)
            self.ubx.append(a_max)
            self.lbx.append(-delta_max)
            self.ubx.append(delta_max)

        # ---------Options set----------
        self.opts_setting = {
            'oc_idm.max_iter':80,
            'oc_idm.print_level':0,
            'print_time':0, 
            'oc_idm.acceptable_tol':1e-6,
            'oc_idm.acceptable_obj_change_tol':1e-4
            }
        
        ## no constrain function
        self.nlp_prop = {
            'f': obj,
            'x': ca.reshape(U, -1, 1),
            'p': P
        }
        self.solver = ca.nlpsol('solver', 'oc_idm', self.nlp_prop, self.opts_setting)
        ## with constraints
        self.nlp_prop_constraint = {
            'f': obj,
            'x': ca.reshape(U, -1, 1),
            'p': P,
            'g': ca.DM([])
        }

    def constraintFunction(self, env_predict:list):

        g = list()
        X = self.State
        h = height
        bound_num = 0
        for bound in env_predict:
            r_env = bound['r']
            Dmin2 = (r_ego + r_env)**2 + safety_margin
            bounds = bound['c']
            num = len(bounds)
            bound_num += num
            start = Np - num
            for k in range(num):
                xx = X[:2, k+1+start]
                phi = X[4, k+1+start]
                x0 = xx + h * ca.vertcat(cos(phi), sin(phi))
                x1 = xx - h * ca.vertcat(cos(phi), sin(phi))
                d0 = (x0 - bounds[k])**2
                d1 = (x1 - bounds[k])**2
                g.append(d0[0]+d0[1]-Dmin2)
                g.append(d1[0]+d1[1]-Dmin2)

        return ca.vertcat(*g), bound_num

    def nmpc_singletrack(self, x0:np.ndarray, u0, trajectory:np.ndarray, env_constraint=None) -> dict: # with or w/o constraints

        # x0: (n_state, )
        # u0: (n_control, Nc)
        # trajectory: (Np, 2)
        x0 = np.reshape(x0, (-1,1))
        t = trajectory.reshape(-1, 1)
        c_p = np.concatenate((x0, t))
        init_control = ca.reshape(u0, -1, 1)
        if env_constraint is None:
            solver = self.solver
            res = solver(x0=init_control, p=c_p, lbx=self.lbx, ubx=self.ubx)
        else:
            self.nlp_prop_constraint['g'], _ = self.constraintFunction(env_constraint)
            solver = ca.nlpsol('solver', 'oc_idm', self.nlp_prop_constraint, self.opts_setting)
            res = solver(x0=init_control, p=c_p, lbx=self.lbx, ubx=self.ubx, lbg=0, ubg=ca.inf)

        u_sol = ca.reshape(res['x'], n_control, Nc)

        result = {}
        result['u_c'] = u_sol[:, 0]
        result['u_next'] = ca.horzcat(u_sol[:, 1:], u_sol[:, -1])
        result['cost'] = res['f']
        return result
