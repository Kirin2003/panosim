"""
Author:
    SUN-Hao

Description:
    Optimal control based intelligent driver model(OCIDM) for Panosim traffic model.

Input:
    state:
        state['x'] :float
        state['y'] :float
        state['h'] :float
        state['v'] :float
    road:
        road['left_lane']
        road['left_limit']
        road['current_lane']
        road['current_limit']
        road['right_lane']
        road['right_limit']
        road['speed_limit']
    env:
        env['leader']:dict obs
        env['left_leader']:dict obs
        env['right_leader']:dict obs
        env['left_follower']:dict obs
        env['right_follower']:dict obs
    obs:
        obs['v'] :float
        obs['h'] :float
        obs['x'] :float
        obs['y'] :float
        obs['w'] :float
        obs['l'] :float
"""


import numpy as np
from copy import deepcopy
from oc_idm.idm_ctrller.idm_sacc import SAccIDM
from oc_idm.idm_ctrller.idm_lc import LCIDM


ctrl_init_flag = False
acc_idm =None
lc_idm = None


def init_idm():
    print("traffic-----initializing idms----- ")
    mpc_dt = 0.2
    ctrl_size = 15
    pred_size = 15
    args = {'ctrl_horizion': ctrl_size,
            'pred_horizion': pred_size,
            'ts': mpc_dt,
            'print_info':False,
            'tv_num': 1}
    acc_idm = SAccIDM(**args)
    #------------------------------
    mpc_dt = 0.2
    ctrl_size = 15
    pred_size = 15
    args = {'ctrl_horizion': ctrl_size,
            'pred_horizion': pred_size,
            'ts': mpc_dt,
            'print_info': False,
            'tv_num': 3}
    lc_idm = LCIDM(**args)

    print("traffic------initializing ok!")
    return acc_idm, lc_idm


def get_envs(env:dict):
    acc_env = list()
    llc_env  = list()
    rlc_env = list()
    if env['leader'] is not None:
        acc_env.append( env['leader']) 
        llc_env.append( env['leader'] )
        rlc_env.append( env['leader'] )
    if env['left_leader'] is not None:
        llc_env.append( env['left_leader'] )
    if env['left_follower'] is not None:
        llc_env.append( env['left_follower'])
    if env['right_leader'] is not None:
        rlc_env.append(env['right_leader'])
    if env['right_follower'] is not None:
        rlc_env.append(env['right_follower'])
    return acc_env,llc_env,rlc_env

def decision(traj:list, beta:list):
    # decision by average speed
    J = [0]
    opt = 0
    for i in  range(len(traj)):
        v_average = 0
        for  j in range ( len(traj[0])):
            v_average += traj[i][j][3]
        J.append(v_average * beta[i])
        # print("v_average",v_average)
        if v_average > J[-1]:
            opt+=1
    # print("opt:",opt)
    return traj[opt]

def idm_ctrl(state=None,road=None,env=None,better_flag=None):
    global ctrl_init_flag
    global acc_idm
    global lc_idm

    if ctrl_init_flag is False:
        acc_idm,lc_idm = init_idm()
        ctrl_init_flag = True
        print("idm init successfully")
    
    # w_hv_lane = 2
    # w_lc = 2
    cur_vel = state['v'] # TODO
    acc_env,llc_env,rlc_env = get_envs(env)
    # traj_list = list()
    # beta_list = list()
    #  sacc
    # lx,ly = lc_idm.get_ref_path(state,np.array( road['current_lane']),cur_vel*0.2,15)
    # if len(acc_env) !=0:
    #     obs = acc_env[0]
    # else:
    #     obs = None
    # _, traj_acc = acc_idm.run(vel=np.array(cur_vel),ref_x=lx,ref_y=ly,ref_vel=20,obs=obs)
    # traj_list.append(traj_acc)
    # if better_flag == 'current':
    #     beta_list.append(w_hv_lane)
    # else:
    #     beta_list.append(1)
    
    if better_flag == 'right':
        lx,ly = lc_idm.get_ref_path(state,np.array( road['right_lane']),cur_vel*0.2,15)
        _,traj_rlc = lc_idm.run(vel=np.array(cur_vel),ref_x=lx,ref_y=ly,ref_vel=20,env=llc_env)
        return traj_rlc
    elif better_flag == 'left':
        lx,ly = lc_idm.get_ref_path(state,np.array( road['left_lane']),cur_vel*0.2,15)
        _,traj_llc = lc_idm.run(vel=np.array(cur_vel),ref_x=lx,ref_y=ly,ref_vel=20,env=llc_env)
        return traj_llc
    else:
        if len(road['left_lane']) != 0:
            lx,ly = lc_idm.get_ref_path(state,np.array( road['left_lane']),cur_vel*0.2,15)
            _,traj_llc = lc_idm.run(vel=np.array(cur_vel),ref_x=lx,ref_y=ly,ref_vel=20,env=llc_env)
            return traj_llc
        if len(road['right_lane']) != 0:
            lx,ly = lc_idm.get_ref_path(state,np.array( road['right_lane']),cur_vel*0.2,15)
            _,traj_rlc = lc_idm.run(vel=np.array(cur_vel),ref_x=lx,ref_y=ly,ref_vel=20,env=llc_env)
            return traj_rlc
        print('------------------------------------------------------------------------')
        return None

    # print("acc")
    # print(ly)
    # print('-----------------------')
    # for e in traj_acc:
    #     print(e) 

# def ctrl(state:dict,road:dict,env:dict,counter):
#     global ctrl_init_flag
#     global acc_idm
#     global lc_idm
#     global actuator

#     dt = (counter + 1)*0.01 #

#     if ctrl_init_flag is False:
#         acc_idm,lc_idm = init_idm()
#         ctrl_init_flag = True
#         actuator = Actuator()
#         print("idm init successfully")

#     # print("counter:",counter)
#     if counter == 0:
#         # print("init state:",state)
#         #
#         cur_vel = state['v']
#         if cur_vel < 1:
#             cur_vel = 1

#         # gen traj & envs
#         traj_list = list()
#         acc_env,llc_env,rlc_env = get_envs(env)
#         # ctrl
#         lx,ly = acc_idm.get_ref_path(state,np.array( road['current_lane']),cur_vel*0.2,15)
#         _, traj_acc = acc_idm.run(vel=np.array(cur_vel),ref_x=lx,ref_y=ly,ref_vel=20,env=acc_env)
#         traj_list.append(traj_acc)

#         if env['leader'] is not None: # has leader vehicle 
#             if len(road['left_lane']) != 0:
#                 lx,ly = lc_idm.get_ref_path(state,np.array( road['left_lane']),cur_vel*0.2,15)
#                 _,traj_llc = lc_idm.run(vel=np.array(cur_vel),ref_x=lx,ref_y=ly,ref_vel=20,env=llc_env)
#                 traj_list.append(traj_llc)

#             elif len(road['right_lane']) != 0:
#                 lx,ly = lc_idm.get_ref_path(state,np.array( road['right_lane']),cur_vel*0.2,15)
#                 _,traj_rlc = lc_idm.run(vel=np.array(cur_vel),ref_x=lx,ref_y=ly,ref_vel=20,env=rlc_env)
#                 traj_list.append(traj_rlc)
#         # decison: chose final trajectory
#         trajectory = decision(traj_list)
#         #----------update actuator------------
#         veh_cfg = VehicleConf()
#         veh_cfg.t = 0
#         veh_cfg.x = state['x']
#         veh_cfg.y = state['y']
#         veh_cfg.yaw = state['h']
#         veh_cfg.vel = state['v']
#         actuator.reset()
#         actuator.set_new_conditions(traj=trajectory,conf=veh_cfg)

#     #----------get states--------
#     if actuator.trajectory is None :
#         print("Error, no trajectory in actuator!")
#         return None
#     else:
#         veh_cfg_dt = actuator.get_conf_dt(dt)

#         update_state =  {"x":veh_cfg_dt.x,
#         "y":veh_cfg_dt.y,
#         "h":veh_cfg_dt.yaw,
#         "v":veh_cfg_dt.vel}

#         # print("update:",update_state)
#         return update_state