"""


"""
from oc_idm.idm_ctrller import rsh_math as mt


class VehicleConf():

    def __init__(self,x=0,y=0,yaw=0,vel=0,t=0):
        self.x = x
        self.y = y
        self.yaw = yaw
        self.vel = vel
        self.t = t

    def __add__(self, delta):
        '''此方法用来制定self + other的规则'''

        x = self.x + delta.x
        y = self.y + delta.y
        yaw = self.yaw + delta.yaw
        vel = delta.vel
        t = delta.t
        return VehicleConf(x=x,y=y,yaw=yaw,vel=vel,t=t)

class Actuator():

    def __init__(self):
        self.trajectory = None # tpe: #list[VehicleConf]
        self.init_conf = None # type: #VehicleConf
        self.tran_x = 0
        self.tran_y = 0
        self.tran_yaw = 0

    def reset(self):
        self.trajectory = None # Note: trajectory time start from 0!
        self.init_conf = None

    def set_new_conditions(self,traj,conf):
        # 有了新的控制命令，就设置新的条件, 新轨迹和
        self.__set_traj(traj) # Note start from x =0, y = 0, yaw = 0
        self.__set_init_conf(conf)
        #print traj

    def __set_traj(self,traj):
        self.trajectory = traj  # in local vehicle frame (right hand z-up)

    def __set_init_conf(self,conf):
        self.init_conf = conf # in global frame (right hand z-up)
        self.tran_x,self.tran_y = mt.trans2DPt(0,0,self.init_conf.x,self.init_conf.y,self.init_conf.yaw) # global (0,0) in vehicle frame
        self.tran_yaw = -self.init_conf.yaw
        # print self.init_conf.x,self.init_conf.y
        # print mt.trans2DPt(0, 0, self.tran_x, self.tran_y, self.tran_yaw)

    def get_conf(self,current_time):
        delta_cfg = self.__get_delta_conf(current_time)
        x,y = mt.trans2DPt(delta_cfg.x,delta_cfg.y, self.tran_x,self.tran_y,self.tran_yaw)
        cfg = VehicleConf(x=x,y=y,
                    yaw=delta_cfg.yaw+self.init_conf.yaw,
                    vel=delta_cfg.vel,
                    t=delta_cfg.t + self.init_conf.t)

        return cfg


    def __get_delta_conf(self,current_time):
        dt = current_time - self.init_conf.t
        # Note: dt 的时间，是从set_new_condition的timestamp 开始算，单位：second
        # if self.previous_dt > dt:
        #     raise RuntimeError('ERROR,current dt is smaller than previous dt')
        # else:
        #     self.previous_dt = dt
        #  check trajectory
        if self.trajectory is None:
            raise RuntimeError('ERROR, no trajectory')
        # check initial config
        if self.init_conf is None:
            raise RuntimeError('ERROR, no init config')
        # check dt
        if dt < 0:
            raise RuntimeError('ERROR, dt is negative')

        # if dt < 0.0001:
        #     print('dt is small return init config')
        #     return self.init_conf

        # check trajectory length
        traj_len = len(self.trajectory)
        if traj_len <= 1:
            raise RuntimeError('ERROR, trajectory only has one waypoint')
        #check trajectory timestamp
        if dt > self.trajectory[-1][4]:
            raise RuntimeError('WARNING,dt is larger than the trajectory')
            #return self.init_conf+ self.trajectory[-1] # global frame (right hand z-up)
        # normal
        for i in range(traj_len-1):
            if dt < 0.0001:
                kp = 0
                d_x = mt.getinterpolation(self.trajectory[i][0],self.trajectory[i+1][0],kp)
                d_y = mt.getinterpolation(self.trajectory[i][1], self.trajectory[i + 1][1], kp)
                d_yaw = mt.getinterpolation(self.trajectory[i][2], self.trajectory[i + 1][2], kp)
                d_vel = mt.getinterpolation(self.trajectory[i][3], self.trajectory[i + 1][3], kp)
                d_t = mt.getinterpolation(self.trajectory[i][4], self.trajectory[i + 1][4], kp)
                delta_cfg = VehicleConf(d_x,d_y,d_yaw,d_vel,d_t)
                return delta_cfg #
            if dt > self.trajectory[i][4] and dt < self.trajectory[i+1][4] :
                kp = mt.getKp(self.trajectory[i][4],self.trajectory[i+1][4],dt)
                d_x = mt.getinterpolation(self.trajectory[i][0],self.trajectory[i+1][0],kp)
                d_y = mt.getinterpolation(self.trajectory[i][1], self.trajectory[i + 1][1], kp)
                d_yaw = mt.getinterpolation(self.trajectory[i][2], self.trajectory[i + 1][2], kp)
                d_vel = mt.getinterpolation(self.trajectory[i][3], self.trajectory[i + 1][3], kp)
                d_t = mt.getinterpolation(self.trajectory[i][4], self.trajectory[i + 1][4], kp)
                delta_cfg = VehicleConf(d_x,d_y,d_yaw,d_vel,d_t)
                return delta_cfg # x,y in vehicle/local frame
        # unknown condition
        raise RuntimeError('ERROR,unknown conditions')


    def get_conf_dt(self,dt):
        delta_cfg = self.__get_delta_conf_dt(dt)
        x,y = mt.trans2DPt(delta_cfg.x,delta_cfg.y, self.tran_x,self.tran_y,self.tran_yaw) #NOTE: to global frame
        cfg = VehicleConf(x=x,y=y,
                    yaw=delta_cfg.yaw+self.init_conf.yaw,
                    vel=delta_cfg.vel,
                    t=delta_cfg.t + self.init_conf.t)

        return cfg

    def __get_delta_conf_dt(self,dt):
        # Note: dt 的时间，是从set_new_condition的timestamp 开始算，单位：second
        # if self.previous_dt > dt:
        #     raise RuntimeError('ERROR,current dt is smaller than previous dt')
        # else:
        #     self.previous_dt = dt
        #  check trajectory
        if self.trajectory is None:
            raise RuntimeError('ERROR, no trajectory')
        # check initial config
        if self.init_conf is None:
            raise RuntimeError('ERROR, no init config')
        # check dt
        if dt < 0:
            raise RuntimeError('ERROR, dt is negative')

        # if dt < 0.0001:
        #     print('dt is small return init config')
        #     return self.init_conf

        # check trajectory length
        traj_len = len(self.trajectory)
        if traj_len <= 1:
            raise RuntimeError('ERROR, trajectory only has one waypoint')
        #check trajectory timestamp
        if dt > self.trajectory[-1][4]:
            raise RuntimeError('WARNING,dt is larger than the trajectory')
            #return self.init_conf+ self.trajectory[-1] # global frame (right hand z-up)
        # normal
        for i in range(traj_len-1):
            if dt < 0.0001:
                kp = 0
                d_x = mt.getinterpolation(self.trajectory[i][0],self.trajectory[i+1][0],kp)
                d_y = mt.getinterpolation(self.trajectory[i][1], self.trajectory[i + 1][1], kp)
                d_yaw = mt.getinterpolation(self.trajectory[i][2], self.trajectory[i + 1][2], kp)
                d_vel = mt.getinterpolation(self.trajectory[i][3], self.trajectory[i + 1][3], kp)
                d_t = mt.getinterpolation(self.trajectory[i][4], self.trajectory[i + 1][4], kp)
                delta_cfg = VehicleConf(d_x,d_y,d_yaw,d_vel,d_t)
                return delta_cfg #
            if dt > self.trajectory[i][4] and dt < self.trajectory[i+1][4] :
                kp = mt.getKp(self.trajectory[i][4],self.trajectory[i+1][4],dt)
                d_x = mt.getinterpolation(self.trajectory[i][0],self.trajectory[i+1][0],kp)
                d_y = mt.getinterpolation(self.trajectory[i][1], self.trajectory[i + 1][1], kp)
                d_yaw = mt.getinterpolation(self.trajectory[i][2], self.trajectory[i + 1][2], kp)
                d_vel = mt.getinterpolation(self.trajectory[i][3], self.trajectory[i + 1][3], kp)
                d_t = mt.getinterpolation(self.trajectory[i][4], self.trajectory[i + 1][4], kp)
                delta_cfg = VehicleConf(d_x,d_y,d_yaw,d_vel,d_t)
                return delta_cfg #
        # unknown condition
        raise RuntimeError('ERROR,unknown conditions')