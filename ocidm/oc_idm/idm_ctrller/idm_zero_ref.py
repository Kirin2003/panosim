"""
Author:
    SUN-Hao
Description:
    ref y is zero.
    initial states is the input parameters.
"""


import casadi as ca
import numpy as np
from oc_idm.idm_ctrller.idm_base import BaseOCPIDM


class ZeroRefIDM(BaseOCPIDM):
    def __init__(self, **kwargs):
        super().__init__( **kwargs)
        # build graph
        self.__build()

    def __build(self):
        # define ctrl cmd vars
        # cmd[0,] = omega, cmd[1,] = ax
        cmd = ca.SX.sym('U', self.get_ctrl_dim(), self.get_ctrl_horizon())
        para = ca.SX.sym('p', 3)  # y0,theta0,v0
        # set init states,i.e. vel = p0
        states = ca.SX.zeros(4)
        states[1] = para[0] # y0
        states[2] = para[1] # theta0
        states[3] = para[2] # v0

        # calculate trajectory
        trajectory = []
        for i in range(self.get_pred_horizon()):
            if i < self.get_ctrl_horizon():
                inputs = cmd[:, i]
            else:
                inputs = cmd[:, -1]

            res = self.vm_func(states=states, inputs=inputs)
            states = res['states_next']
            trajectory.append(states)

        # calculate objective function
        obj = 0
        for i in range(self.get_ctrl_horizon()):
            obj_omega = cmd[0, i] * cmd[0, i]
            obj_ax = cmd[1, i] * cmd[1, i]
            obj = obj + self.get_weight_comf() * (obj_omega + obj_ax)

        for i in range(self.get_pred_horizon()):
            # obj_x = (trajectory[i][0] - para[1 + i]) ** 2
            obj_y = trajectory[i][1]  ** 2
            obj_vel = trajectory[i][3] ** 2
            obj = obj + self.get_weight_acc() * obj_y # + self.get_weight_vel() * obj_vel

        #----add constraints for x--------------
        self.lbx = list()
        self.ubx  = list()
        for i in range(self.get_ctrl_horizon()):
            self.lbx.append(-ca.inf)
            self.ubx.append(ca.inf)

        for i in range(self.get_ctrl_horizon()):
            self.lbx.append(-8)
            self.ubx.append(6)

        #----add constraints for gx--------------
        self.lbg = list()
        self.ubg  = list()
        g = list()

        #--------------------------------------
        for i in range(self.get_pred_horizon()):
            g.append(trajectory[i][3])  # v boundary
            self.lbg.append(0)
            self.ubg.append(ca.inf)

        opts_setting = {
            'ipopt.max_iter': 100,
            'ipopt.print_level': 0,
            'print_time': 0,
            'ipopt.acceptable_tol': 1e-6,
            'ipopt.acceptable_obj_change_tol': 1e-4
        }

        nlp_prop = {
            'f': obj,
            'x': ca.reshape(cmd, -1, 1),
            'p': para,
            'g': ca.vertcat(*g)
        }
        # ---------------------------
        self.solver = ca.nlpsol('solver', 'ipopt', nlp_prop, opts_setting)

    def run0(self, inti_y, init_h, inti_vel):

        y0 = np.reshape(inti_y, (-1, 1))
        h0 = np.reshape(init_h, (-1, 1))
        v0 = np.reshape(inti_vel, (-1, 1))

        init_u = np.zeros(self.get_ctrl_horizon() * self.get_ctrl_dim())
        p = np.concatenate((y0, h0, v0))

        #-----update ctrl constraints-------------
        for i in range(self.get_ctrl_horizon()):
            self.lbx[i] = (-self.get_max_ay()/v0)
            self.ubx[i] = (self.get_max_ay()/v0)

        #----ipopt solve------------------------
        res = self.solver(x0=init_u, p=p,lbg=self.lbg,ubg=self.ubg,lbx=self.lbx,ubx=self.ubx)
        cmds = np.array(res['x'])
        # Note res['x'] is casadi.DM change to np: np.array(res['x'])
        u_sol = ca.reshape(res['x'], self.get_ctrl_dim(), self.get_ctrl_horizon())
        u_sol_np = np.array(u_sol)
        traj = self.gen_traj(u_sol_np, v0)
        return cmds
