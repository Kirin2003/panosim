"""
idm sacc:
    trajectory tracking with super adaptive cruise control
Author: SUN-Hao
"""

import casadi as ca
import numpy as np
from oc_idm.idm_ctrller.idm_base import BaseOCPIDM


class SAccIDM(BaseOCPIDM):
    def __init__(self, **kwargs):
        super().__init__( **kwargs)
        # build graph
        self.__build()


    def __build(self):
        # define ctrl cmd vars
        # cmd[0,] = omega, cmd[1,] = ax
        cmd = ca.SX.sym('U', self.get_ctrl_dim(), self.get_ctrl_horizon())

        #  ----------------------
        p_init_v = ca.SX.sym('inti_v')
        p_ref_x = ca.SX.sym('ref_x', self.get_pred_horizon())
        p_ref_y = ca.SX.sym('ref_y', self.get_pred_horizon())
        # p_ref_v = ca.SX.sym('ref_v')
        p_init_dist = ca.SX.sym('init_dist')
        p_ref_dist =  ca.SX.sym('des_dist')
        p_ahead_v = ca.SX.sym('ahead_v')
        # set init states,i.e. vel = p0
        states = ca.SX.zeros(4)
        states[3] = p_init_v[0]
        # calculate trajectory
        trajectory = []
        for i in range(self.get_pred_horizon()):
            if i < self.get_ctrl_horizon():
                inputs = cmd[:, i]
            else:
                inputs = cmd[:, -1]

            res = self.vm_func(states=states, inputs=inputs)
            states = res['states_next']
            trajectory.append(states)

        # calculate objective function: des_s, des_v, acceleration, y_err
        #  (1)  des_s = t_h * v_f + d_safe
        #       t_h = 2.0 ~ 2.5 s
        #       d_safe = 10 m
        #  (2)  des_v =  ahead_v
        #  (3)  acceleration,
        #  (4)  y_err
        obj = 0
        for i in range(self.get_ctrl_horizon()):
            obj_omega =  3 * cmd[0, i] * cmd[0, i]
            obj_ax = cmd[1, i] * cmd[1, i]
            obj = obj + self.get_weight_comf() * (obj_omega + obj_ax)

        ts = self.get_ts()
        delta_dist = 0
        for i in range(self.get_pred_horizon()):
            # obj_x = (trajectory[i][0] - p_ref_x[i]) ** 2 # x_err
            obj_y = (trajectory[i][1] - p_ref_y[i]) ** 2 # y_er
            obj_vel = (trajectory[i][3] - p_ahead_v) ** 2 # vel
            delta_dist = delta_dist + ( p_ahead_v -trajectory[i][3]  )* ts #
            obj_dist =(p_init_dist + delta_dist - p_ref_dist) ** 2 # dist
            obj = obj + self.get_weight_acc() * ( obj_y + obj_vel)  + 0.1 * obj_dist

        P = list()
        P.append(p_init_v)
        P.append(p_ref_x)
        P.append(p_ref_y)
        # P.append(p_ref_v)
        P.append(p_init_dist)
        P.append(p_ref_dist)
        P.append(p_ahead_v)

        #----add constraints for x--------------
        self.lbx = list()
        self.ubx  = list()
        for i in range(self.get_ctrl_horizon()):
            self.lbx.append(-ca.inf)
            self.ubx.append(ca.inf)

        for i in range(self.get_ctrl_horizon()):
            self.lbx.append(self.get_min_ax())
            self.ubx.append(self.get_max_ax())

        #----add constraints for gx--------------
        self.lbg = list()
        self.ubg  = list()
        g = list()

        for i in range(self.get_pred_horizon()):
            g.append(trajectory[i][3])  # v boundary
            self.lbg.append(0.0)
            self.ubg.append(ca.inf)

        # ---------------------------
        opts_setting = {
            'ipopt.max_iter': 100,
            'ipopt.print_level': 0,
            'print_time': 0,
            'ipopt.acceptable_tol': 1e-6,
            'ipopt.acceptable_obj_change_tol': 1e-4
        }
        nlp_prop = {
            'f': obj,
            'x': ca.reshape(cmd, -1, 1),
            'p': ca.vertcat(*P),
            'g':ca.vertcat(*g)
        }
        self.solver = ca.nlpsol('solver', 'ipopt', nlp_prop, opts_setting)

    def run(self, vel, ref_x, ref_y, ref_vel,obs=None):
        """
        vel: real velocity of the vehicle
        """
        if np.size(vel) != 1:
            raise RuntimeError("error vel")
        if np.size(ref_x) != self.get_pred_horizon():
            raise RuntimeError("error ref x")
        if np.size(ref_y) != self.get_pred_horizon():
            raise RuntimeError("error ref y")
        if np.size(ref_vel) != 1:
            raise RuntimeError("error ref vel")

        init_dist, des_vel , des_dist ,real_dist= self.get_leader_veh(ref_x,ref_y,ref_vel,vel,obs)
        aeb_flag = self.is_AEB(real_dist, vel,des_vel,8)
        if aeb_flag:
            traj = self.gen_traj_AEB( vel)
            print("SACC, vehicle stopped!")
            return None, traj

        is_stop_flag = self.is_stop( real_dist, des_vel)
        if is_stop_flag:
            traj = self.gen_traj_stopping( vel)
            print("SACC, vehicle stopped!")
            return None, traj

        if vel < self.get_lon_ctrl_min_vel(): #
            print("SACC WARNING! vel is not  correct!")
            vel = self.get_lon_ctrl_min_vel()
        # if des_vel < self.get_lon_ctrl_min_vel(): #
        #     des_vel = self.get_lon_ctrl_min_vel()
        #------update constraints boundaries------------
        for i in range(self.get_ctrl_horizon()):
            self.lbx[i] = (-self.get_max_ay()/vel)
            self.ubx[i] = (self.get_max_ay()/vel)

        #-------update paremeters in solver--------------------------------------
        vel = np.reshape(vel, (-1, 1))
        ref_x = np.reshape(ref_x, (-1, 1))
        ref_y = np.reshape(ref_y, (-1, 1))
        init_dist = np.reshape(init_dist, (-1, 1))
        des_dist = np.reshape(des_dist, (-1, 1))
        des_vel = np.reshape(des_vel, (-1, 1))
        p = np.concatenate((vel, ref_x, ref_y,init_dist,des_dist,des_vel)) # NOTE: vel and des_vel must larger than min because of lat control
        #-------update initial control commands
        init_x0 = np.zeros(self.get_ctrl_horizon() * self.get_ctrl_dim()) # initial u
        #--------control-------------
        if aeb_flag:
            print("AEB")
            raise  RuntimeError("aeb flag is actives")

        res = self.solver(x0=init_x0, p=p,lbx=self.lbx,ubx=self.ubx,lbg=self.lbg,ubg=self.ubg)
        cmds = np.array(res['x'])

        # print(cmds[self.get_ctrl_horizon()])
        # Note res['x'] is casadi.DM change to np: np.array(res['x'])
        u_sol = ca.reshape(res['x'], self.get_ctrl_dim(), self.get_ctrl_horizon())
        u_sol_np = np.array(u_sol)
        traj = self.gen_traj(u_sol_np, vel)
        return cmds, traj
