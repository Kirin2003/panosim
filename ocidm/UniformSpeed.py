from TrafficModelInterface import *


def ModelStart(userData):
	userData["stopList"] = []


def ModelOutput(userData):

    for id in getVehicleList():
        # set your velocity
        velocity = 40
        deltaX = velocity * 0.01
        curX = getVehicleX(id)
        y = getVehicleY(id)
        yaw = getVehicleYaw(id)
        x = curX + deltaX
        moveTo(id, x, y, yaw)


def ModelTerminate(userData):
	pass
