"""
Author:
    SUN-Hao

Description:
    Optimal control based intelligent driver model(OCIDM) for Panosim traffic model.

Input:
    state:
        state['x'] :float
        state['y'] :float
        state['h'] :float
        state['v'] :float
    road:
        road['left_lane']
        road['left_limit']
        road['current_lane']
        road['current_limit']
        road['right_lane']
        road['right_limit']
        road['speed_limit']
    env:
        env['leader']:dict obs
        env['left_leader']:dict obs
        env['right_leader']:dict obs
        env['left_follower']:dict obs
        env['right_follower']:dict obs
    obs:
        obs['v'] :float
        obs['h'] :float
        obs['x'] :float
        obs['y'] :float
        obs['w'] :float
        obs['l'] :float


    class DisVehMode:
        ID
        CurLaneID : str
        TargetLaneID  : ""
        ViaLaneID  : []    15second.
        AxDir:  -1 Dec     1 Acc,   0: up2u
        AxThreshold:     <0最小减速度    >0最小加速度   0：
"""


import numpy as np
from copy import deepcopy
from oc_idm.idm_ctrller.pano_traffic_driver import PanoDriver
import copy


my_flag = None # TODO delete

def init_idm():
    mpc_dt = 0.2
    ctrl_size = 10
    pred_size = 15
    args = {'ctrl_size': ctrl_size,
            'pred_size': pred_size,
            'ts': mpc_dt}
    print("idm args:",args)
    idm = PanoDriver(**args)
    return idm


def idm_ctrl(state=None,road=None,env=None,driver_type=0,idm=None,newCreated=False):
    # idm = g_idm
    global ctrl_init_flag,my_flag
    # print("idm working")
    # HF revise  这里使用全局变量，相当于只初始化了一次“当前道路”，切换车道之后，laneID就不对了，导致rb_offset和
    # right_ref_offset无法赋值。
    if newCreated == True:
        my_flag = road['current_lane_id']
    #print( "New Created Flag:", newCreated )

    ref_v = road['speed_limit']
    current_v = state['v']
    if current_v < 0.1 :
        current_v = 0.1
    dt,pred_size,_=idm.get_ctrl_parameters()
    ds = current_v*dt
    lane_shape = np.array( road['current_lane'])
    # print("current state:",state)
    lx,ly = idm.get_ref_path(state,lane_shape,ds,pred_size)

    lb_offset = -1
    left_ref_offset = 3.75
    rb_offset = 1
    right_ref_offset = -3.75
    # print('--------------',my_flag,road['current_lane_id'])
    if my_flag == road['current_lane_id']:
        if road['left_lane_id'] is not None:
            lb_offset = 5
            left_ref_offset = 3.5
            pass

        if road['right_lane_id'] is not None:
            rb_offset = -5
            right_ref_offset = -3.5
            pass

    #-------------get obs------------------
    # print('#from traffic oc idm#,env:',env)
    leader = env['leader']
    # print('#traffic oc dim # leader is :',leader)
    left_leader =  env['left_leader']
    left_follower =  env['left_follower']
    right_leader = env['right_leader']
    right_follower = env['right_follower']
    #---------------ctrl--------------------
    # print("right_follower",right_follower)
    # left_follower['v'] = 0.1
    # right_follower['v'] = 0.1
    
    idm.set_driver_type(driver_type) 
    traj,flag= idm.run(host_vel=state['v'],ref_x=lx,ref_y=ly,ref_v=ref_v,ref_ds=ds,
                        lb_offset=lb_offset,rb_offset=rb_offset,
                        left_ref_offset=left_ref_offset,right_ref_offset=right_ref_offset,
                        leader=leader,
                        left_leader=left_leader,left_follower=left_follower,
                        right_leader=right_leader,right_follower=right_follower,
                        dist = env['dist'])
    # print("idm done!")
    return traj
