import numpy as np
import casadi as ca


c = ca.DM(2,3)
print(c)

c_dense = c.full()
print(c_dense)

print('------------')
state = ca.DM.zeros(4)
state[3] = 10.0
print('state:',state)
print(type(state))