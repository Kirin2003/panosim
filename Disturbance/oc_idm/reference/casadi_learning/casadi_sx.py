import casadi as ca
from casadi import *



x = MX.sym("x",2)

print(x)


y = SX.sym('y',5) # 5 x 1 i.e. vector
print(y)
print('y-1:',y[-1])

z = SX.sym('Z',4,2)


print(z)


f = x**2 + 10
f = casadi.sqrt(f)
print(f)
#
#
#
# B1 = casadi.SX.zeros(4,5)
# print(B1) #  structural zeoros : 00, actual zeros:0
#
#
# B2 = casadi.SX(4,5)
#
# print(B2)
#
#
# print(casadi.SX(8))
# print(type(casadi.SX(8)))
#
# print(7*casadi.SX.ones(3,3))
#
# print('--------------')
# print(repmat(SX(3),2,1))

p_init_v = ca.SX.sym('inti_v')
print(p_init_v)
print(p_init_v[0])


