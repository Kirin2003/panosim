"""
idm sacc:
    trajectory tracking with super adaptive cruise control
Author: SUN-Hao
"""

import casadi as ca
import numpy as np
from oc_idm.idm_ctrller.idm_base import BaseOCPIDM


class SAccIDM(BaseOCPIDM):
    def __init__(self, **kwargs):
        super().__init__( **kwargs)
        # build graph
        self.__build()
        self.__average_vel = None
        self.__average_ay = None
        self.__average_ax = None

    def __build(self):
        # define ctrl cmd vars
        # cmd[0,] = omega, cmd[1,] = ax
        cmd = ca.SX.sym('U', self.get_ctrl_dim(), self.get_ctrl_horizon())

        #  ----------------------
        p_init_v = ca.SX.sym('inti_v')
        p_ref_x = ca.SX.sym('ref_x', self.get_pred_horizon())
        p_ref_y = ca.SX.sym('ref_y', self.get_pred_horizon())
        # p_ref_v = ca.SX.sym('ref_v')
        p_init_dist = ca.SX.sym('init_dist')
        p_ref_dist =  ca.SX.sym('des_dist')
        p_ahead_v = ca.SX.sym('ahead_v')
        # set init states,i.e. vel = p0
        states = ca.SX.zeros(4)
        states[3] = p_init_v[0]
        # calculate trajectory
        trajectory = []
        for i in range(self.get_pred_horizon()):
            if i < self.get_ctrl_horizon():
                inputs = cmd[:, i]
            else:
                inputs = cmd[:, -1]

            res = self.vm_func(states=states, inputs=inputs)
            states = res['states_next']
            trajectory.append(states)

        # calculate objective function: des_s, des_v, acceleration, y_err
        #  (1)  des_s = t_h * v_f + d_safe
        #       t_h = 2.0 ~ 2.5 s
        #       d_safe = 10 m
        #  (2)  des_v =  ahead_v
        #  (3)  acceleration,
        #  (4)  y_err

        obj = 0
        for i in range(self.get_ctrl_horizon()):
            obj_omega = cmd[0, i] * cmd[0, i]
            obj_ax = cmd[1, i] * cmd[1, i]
            obj = obj + (obj_omega + obj_ax)

        # for i in range(1,self.get_ctrl_horizon()):
        #     d_ax =  cmd[1, i] - cmd[1, i-1]
        #     obj_dax = d_ax **2
        #     obj = obj + 10*obj_dax

        ts = self.get_ts()
        # delta_dist = 0
        leader_dist = 0
        host_dist = 0
        for i in range(self.get_pred_horizon()):
            # obj_x = (trajectory[i][0] - p_ref_x[i]) ** 2 # x_err
            obj_y = (trajectory[i][1] - p_ref_y[i]) ** 2 # y_er
            obj_vel = (trajectory[i][3] - p_ahead_v) ** 2 # vel
            host_dist = host_dist + trajectory[i][3]*ts
            leader_dist = leader_dist + p_ahead_v*ts
            # delta_dist = delta_dist + ( p_ahead_v -trajectory[i][3]  )* ts #
            obj_dist = (p_init_dist + leader_dist - host_dist -  p_ref_dist) ** 2 # dist
            obj = obj + self.get_weight_acc() *  obj_y + obj_vel + obj_dist

        # for i in range(self.get_pred_horizon()):
        #     obj_vel = (trajectory[i][3] - p_ahead_v) ** 2 # vel
        #     obj = obj + obj_vel
        # obj = obj + self.get_pred_horizon() * (trajectory[0][3] - (p_ahead_v + d_vel)) ** 2

        P = list()
        P.append(p_init_v)
        P.append(p_ref_x)
        P.append(p_ref_y)
        P.append(p_init_dist)
        P.append(p_ref_dist)
        P.append(p_ahead_v)

        #----add constraints for x--------------
        self.lbx = list()
        self.ubx  = list()
        for i in range(self.get_ctrl_horizon()):
            self.lbx.append(-ca.inf)
            self.ubx.append(ca.inf)

        for i in range(self.get_ctrl_horizon()):
            self.lbx.append(self.get_min_ax())
            self.ubx.append(self.get_max_ax())

        #----add constraints for gx--------------
        self.lbg = list()
        self.ubg  = list()
        g = list()

        for i in range(self.get_pred_horizon()):
            g.append(trajectory[i][3])  # v boundary
            self.lbg.append(0.0)
            self.ubg.append(ca.inf)

        # ---------------------------
        opts_setting = {
            'ipopt.max_iter': 100,
            'ipopt.print_level': 0,
            'print_time': 0,
            'ipopt.acceptable_tol': 1e-6,
            'ipopt.acceptable_obj_change_tol': 1e-4
        }
        nlp_prop = {
            'f': obj,
            'x': ca.reshape(cmd, -1, 1),
            'p': ca.vertcat(*P),
            'g':ca.vertcat(*g)
        }
        self.solver = ca.nlpsol('solver', 'ipopt', nlp_prop, opts_setting)

    def run(self, state, ref_traj,env):  # only consider leader vehicle
        """
        vel: real velocity of the vehicle
        return :
        (1) commands
        (2) trajectory
        (3) exception flag, 0: normal, 1 input error
        """
        # print("sacc is working:",state['vel'])
        vel = state['vel']
        ref_x = env['ref_x']
        ref_y = env['ref_y']
        ref_vel = env['ref_v']
        ds = env['ref_ds']
        obs = env['leader']
        if obs is not None and obs['x'] <0: # TODO
            obs = None
        # print("#sacc#,obs:",obs)
        exception_flag = 0
        #   Exception:
        if np.size(vel) != 1:
            print("#SACC ERROR# from idm sacc: wrong vel")
            exception_flag = 1
            return None,None,exception_flag
        if np.size(ref_x) != self.get_pred_horizon():
            exception_flag = 1
            print("#SACC ERROR# from idm sacc: wrong ref x")
            return None, None, exception_flag
        if np.size(ref_y) != self.get_pred_horizon():
            exception_flag = 1
            print("#SACC ERROR# from idm sacc: wrong ref y")
        if np.size(ref_vel) != 1:
            exception_flag = 1
            print("#SACC ERROR# from idm sacc: wrong ref vel")
            return None, None, exception_flag

        # print("#sacc#,ref_x:",ref_x)
        # print("#sacc#,ref_y:",ref_y)
        init_dist, des_vel , des_dist ,real_dist= self.get_leader_veh(ref_x,ref_y,ref_vel,vel,ds,obs)
        # print('#sacc#acc info:',vel,init_dist, 'des_vel',des_vel , 'des_dist',des_dist,'real_dist' ,real_dist)
        # negative stop is working
        aeb_flag ,req_dec = self.negative_stop(real_dist, vel, des_vel ) # TODO
        # print('#sacc# aeb info:', aeb_flag ,req_dec)
        if aeb_flag:
            exception_flag = 2
            traj = self.gen_negative_stop_traj(vel,req_dec)
            # print("#WARNING# from idm sacc, negative stop is working")
            return None, traj, exception_flag

        #   positive stop is working
        is_stop_flag = self.positive_stop( real_dist, des_vel) # TODO : positive stop trajectory
        # print('obs',obs,'real_dist',real_dist,'des_vel',des_vel,"is stop",is_stop_flag)
        if is_stop_flag:
            exception_flag = 3
            traj = self.gen_positive_stop_traj( 0 )  #  s
            print("SACC, positive stop is working")
            return None, traj, exception_flag # TODO :::!!! exception of ref path

        #  ***********  normal  working ****************
        if vel < self.get_lon_ctrl_min_vel(): # min speed  , vel: real velocity of host vehicle
            vel = self.get_lon_ctrl_min_vel()
            print("SACC WARNING! vel is not  correct!")

        #------update constraints boundaries------------
        for i in range(self.get_ctrl_horizon()):
            self.lbx[i] = (-self.get_max_ay()/vel)
            self.ubx[i] = (self.get_max_ay()/vel)

        #-------update paremeters in solver--------------------------------------
        vel = np.reshape(vel, (-1, 1))
        ref_x = np.reshape(ref_x, (-1, 1))
        ref_y = np.reshape(ref_y, (-1, 1))
        init_dist = np.reshape(init_dist, (-1, 1))
        des_dist = np.reshape(des_dist, (-1, 1))
        des_vel = np.reshape(des_vel, (-1, 1))
        p = np.concatenate((vel, ref_x, ref_y,init_dist,des_dist,des_vel)) # NOTE: vel and des_vel must larger than min because of lat control
        # print('#sacc#,parameters:',vel,init_dist,des_dist,des_vel)
        #-------update initial control commands
        init_x0 = np.zeros(self.get_ctrl_horizon() * self.get_ctrl_dim()) # initial u

        res = self.solver(x0=init_x0, p=p,lbx=self.lbx,ubx=self.ubx,lbg=self.lbg,ubg=self.ubg)
        cmds = np.array(res['x'])

        # print(cmds[self.get_ctrl_horizon()])
        # Note res['x'] is casadi.DM change to np: np.array(res['x'])
        u_sol = ca.reshape(res['x'], self.get_ctrl_dim(), self.get_ctrl_horizon())
        u_sol_np = np.array(u_sol)
        # print( u_sol_np.shape ) # res = (2, 15)
        traj = self.gen_traj(u_sol_np, vel)
        self.__average_vel,self.__average_ax,self.__average_ay = self.eval_traj(u_sol_np, traj)
        # print('cmd',u_sol_np[1,:])
        return cmds, traj, exception_flag

    def get_eval(self):
        return self.__average_vel,self.__average_ax,self.__average_ay
