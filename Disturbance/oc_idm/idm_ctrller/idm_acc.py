"""
idm acc:
    trajectory tracking with adaptive cruise control

"""
import casadi as ca
import numpy as np
from oc_idm.idm_ctrller.idm_base import BaseOCPIDM


class AccIDM(BaseOCPIDM):
    def __init__(self, **kwargs):
        super().__init__( **kwargs)
        # build graph
        self.__build()

    def __build(self):
        # define ctrl cmd vars
        # cmd[0,] = omega, cmd[1,] = ax
        cmd = ca.SX.sym('U', self.get_ctrl_dim(), self.get_ctrl_horizon())

        #  ----------------------
        p_init_v = ca.SX.sym('inti_v')
        p_ref_x = ca.SX.sym('ref_x', self.get_pred_horizon())
        p_ref_y = ca.SX.sym('ref_y', self.get_pred_horizon())
        # p_ref_v = ca.SX.sym('ref_v')
        p_init_dist = ca.SX.sym('init_dist')
        p_ref_dist =  ca.SX.sym('des_dist')
        p_ahead_v = ca.SX.sym('ahead_v')
        # set init states,i.e. vel = p0
        states = ca.SX.zeros(4)
        states[3] = p_init_v[0]
        # calculate trajectory
        trajectory = []
        for i in range(self.get_pred_horizon()):
            if i < self.get_ctrl_horizon():
                inputs = cmd[:, i]
            else:
                inputs = cmd[:, -1]

            res = self.vm_func(states=states, inputs=inputs)
            states = res['states_next']
            trajectory.append(states)
        # calculate objective function
        obj = 0
        for i in range(self.get_ctrl_horizon()):
            obj_omega =  3 * cmd[0, i] * cmd[0, i]
            obj_ax = cmd[1, i] * cmd[1, i]
            obj = obj + self.get_weight_comf() * (obj_omega + obj_ax)

        ts = self.get_ts()
        add_station = 0
        for i in range(self.get_pred_horizon()):
            obj_x = (trajectory[i][0] - p_ref_x[i]) ** 2
            obj_y = (trajectory[i][1] - p_ref_y[i]) ** 2
            # obj_vel = (trajectory[i][3] - p_ref_v) ** 2
            add_station = add_station + ( p_ahead_v -trajectory[i][3]  )* ts
            obj_acc =(add_station + p_init_dist - p_ref_dist) ** 2
            obj = obj + self.get_weight_acc() * (obj_y + obj_x)  + obj_acc

        P = list()
        P.append(p_init_v)
        P.append(p_ref_x)
        P.append(p_ref_y)
        # P.append(p_ref_v)
        P.append(p_init_dist)
        P.append(p_ref_dist)
        P.append(p_ahead_v)

        #----add constraints for x--------------
        self.lbx = list()
        self.ubx  = list()
        for i in range(self.get_ctrl_horizon()):
            self.lbx.append(-ca.inf)
            self.ubx.append(ca.inf)

        for i in range(self.get_ctrl_horizon()):
            self.lbx.append(self.get_min_ax())
            self.ubx.append(self.get_max_ax())

        #----add constraints for gx--------------
        self.lbg = list()
        self.ubg  = list()
        g = list()

        for i in range(self.get_pred_horizon()):
            g.append(trajectory[i][3])  # v boundary
            self.lbg.append(0.0)
            self.ubg.append(ca.inf)

        # ---------------------------
        opts_setting = {
            'ipopt.max_iter': 100,
            'ipopt.print_level': 0,
            'print_time': 0,
            'ipopt.acceptable_tol': 1e-6,
            'ipopt.acceptable_obj_change_tol': 1e-4
        }
        nlp_prop = {
            'f': obj,
            'x': ca.reshape(cmd, -1, 1),
            'p': ca.vertcat(*P),
            'g':ca.vertcat(*g)
        }
        self.solver = ca.nlpsol('solver', 'ipopt', nlp_prop, opts_setting)

    def run(self, vel, ref_x, ref_y, ref_vel,env=None):
        if env is None:
            raise RuntimeError("error env=None")
        if np.size(vel) != 1:
            raise RuntimeError("error vel")
        if np.size(ref_x) != self.get_pred_horizon():
            raise RuntimeError("error ref x")
        if np.size(ref_y) != self.get_pred_horizon():
            raise RuntimeError("error ref y")
        if np.size(ref_vel) != 1:
            raise RuntimeError("error ref vel")

        # todo obs count

        obs = env[0]
        acc_flag ,init_dist,ref_dist,ahead_v = \
            self.get_acc_info(ref_x=ref_x,ref_y=ref_y,ref_v=ref_vel,vel=vel,obs=obs)
        if not acc_flag:
            raise  RuntimeError("aeb flag is actives")

        #------update constraints boundaries------------
        for i in range(self.get_ctrl_horizon()):
            self.lbx[i] = (-4/vel)
            self.ubx[i] = (4/vel)

        #-------update paremeters in solver--------------------------------------
        vel = np.reshape(vel, (-1, 1))
        ref_x = np.reshape(ref_x, (-1, 1))
        ref_y = np.reshape(ref_y, (-1, 1))
        # ref_vel = np.reshape(ref_vel, (-1, 1))
        init_dist = np.reshape(init_dist, (-1, 1))
        ref_dist = np.reshape(ref_dist, (-1, 1))
        ahead_v = np.reshape(ahead_v, (-1, 1))
        p = np.concatenate((vel, ref_x, ref_y,init_dist,ref_dist,ahead_v))
        #-------update initial control commands
        init_x0 = np.zeros(self.get_ctrl_horizon() * self.get_ctrl_dim()) # initial u

        res = self.solver(x0=init_x0, p=p,lbx=self.lbx,ubx=self.ubx,lbg=self.lbg,ubg=self.ubg)
        cmds = np.array(res['x'])

        # print(cmds[self.get_ctrl_horizon()])
        # Note res['x'] is casadi.DM change to np: np.array(res['x'])
        u_sol = ca.reshape(res['x'], self.get_ctrl_dim(), self.get_ctrl_horizon())
        u_sol_np = np.array(u_sol)
        traj = self.gen_traj(u_sol_np, vel)
        return cmds, traj
