"""
Author:
    SUN-Hao
"""
import numpy as np

from oc_idm.idm_ctrller.idm_base import PIDController
from oc_idm.idm_ctrller.idm_slc import SLCIDM
from oc_idm.idm_ctrller.idm_sacc  import SAccIDM


class SuperDriver():
    def __init__(self,**kwargs):
        self.__lb_x = None
        self.__lb_y = None

        self.__rb_x = None
        self.__rb_y = None

        self.__trajs = {}

        self.__mpc_dt = kwargs['ts']
        self.__ctrl_size = kwargs['ctrl_size']
        self.__pred_size = kwargs['pred_size']
        # -----create idm----------------
        print("create lane change driver")
        lc_args = {'ctrl_size': self.__ctrl_size,
                'pred_size': self.__pred_size,
                'ts': self.__mpc_dt,
                'tv_num': 3}
        self.__left_lc_idm = SLCIDM(**lc_args)
        # -----create idm----------------
        print("create lane change driver")
        lc_args = {'ctrl_size': self.__ctrl_size,
                'pred_size': self.__pred_size,
                'ts': self.__mpc_dt,
                'tv_num': 3}
        self.__right_lc_idm = SLCIDM(**lc_args)
        # -----create idm----------------
        print("create super acc driver")
        acc_args = {'ctrl_size': self.__ctrl_size,
                'pred_size': self.__pred_size,
                'ts': self.__mpc_dt,
                'tv_num': 1}
        self.__acc_idm = SAccIDM(**acc_args)

        # -----create  pid  acc controller---------------
        self.__s_ctrller = PIDController(K_P=1,K_I=0,K_D=0.5,ub = 10,lb=-10, q_len=10)
        self.__v_ctrller = PIDController(K_P=1,K_I=0,K_D=0.5,ub = 6,lb=-8,q_len=10)
        print( "create pid acc controller ok"  )

        print( "--------------super driver init ok-------------"  )


    def get_ctrl_parameters(self):
        return self.__mpc_dt,self.__pred_size,self.__ctrl_size

    def run(self,host_vel,ref_x,ref_y,ref_v,ref_ds,
            lb_offset=-1,rb_offset=1,
            left_ref_offset=0,right_ref_offset=0,
            leader=None,
            left_leader=None,left_follower=None,
            right_leader=None,right_follower=None):
        # reset
        state = {}
        env = {}
        self.__trajs['acc'] = None
        self.__trajs['left_lc'] = None
        self.__trajs['right_lc'] = None
        # go go go
        state['vel'] = host_vel

        env['ref_ds'] = ref_ds
        env['ref_x'] = ref_x
        env['ref_y'] = ref_y
        env['ref_v'] = ref_v
        env['speed_limit'] = 1.05* ref_v
        env['left_ref_offset'] = None
        env['right_ref_offset'] = None
        if left_ref_offset < lb_offset and left_ref_offset > 3:
            env['left_ref_offset'] = left_ref_offset
        if  right_ref_offset > rb_offset and right_ref_offset< -3:
            env['right_ref_offset'] = right_ref_offset

        env['lb_offset'] = None
        env['rb_offset'] = None
        if lb_offset > 0 :
            env['lb_offset'] = lb_offset
        if rb_offset < 0 :
            env['rb_offset'] = rb_offset

        env['leader'] = leader
        env['left_leader'] = left_leader
        env['left_follower'] = left_follower
        env['right_leader'] = right_leader
        env['right_follower'] = right_follower
        # print('ready to drive')
        traj = self.__drive(state,env)
        return traj,0

    def get_ref_path(self,state:dict,lane_shape:np.ndarray,in_ds:float,in_pt_num:int): # interface for panosim
        # print('-------------super driver----------')
        return self.__acc_idm.get_ref_path(state,lane_shape,in_ds,in_pt_num)

    def __drive(self,state:dict,env:dict):
        """
        (1): 
        return:  traj
        """
        # print("get env",env)
        # print("get state",state)



        _, traj, exception_flag = self.__acc_idm.run(state, ref_traj={},env=env)
        self.__trajs['acc'] = traj

        #  get boundaries
        if  env['lb_offset'] is not None:
            offset =  env['lb_offset']
        else:
            offset = 5
        self.__lb_x, self.__lb_y = self.__left_lc_idm.gen_offset_ref_path(env['ref_x'], env['ref_y'], offset=offset)
        env['lb_y'] = self.__lb_y

        if  env['rb_offset']  is not None:
            offset = env['rb_offset']
        else:
            offset = -5
        self.__rb_x, self.__rb_y = self.__left_lc_idm.gen_offset_ref_path(env['ref_x'], env['ref_y'], offset=offset)
        env['rb_y'] = self.__rb_y

        # try left lane change
        # print("#super driver#   try left")
        if env['left_ref_offset'] is not None: # has left reference
            env['obs_list'] = self.__create_left_lc(env)
            ref = {}
            self.__lc_ref_x, self.__lc_ref_y = self.__left_lc_idm.gen_offset_ref_path(env['ref_x'], env['ref_y'],
                                                                                      offset=env['left_ref_offset'])
            ref['ref_x'] = self.__lc_ref_x
            ref['ref_y'] = self.__lc_ref_y
            ref['ref_v'] = env['ref_v']
            _, traj, exception_flag = self.__left_lc_idm.run(state, ref,env)
            self.__trajs['left_lc'] = traj
            # print("left lane change")

        # try right lane change
        # print("#super driver#   try right")
        if env['right_ref_offset'] is not None: # has left reference
            env['obs_list'] = self.__create_right_lc(env)
            ref = {}
            self.__rc_ref_x, self.__rc_ref_y = self.__right_lc_idm.gen_offset_ref_path(env['ref_x'], env['ref_y'],
                                                                                       offset=env['right_ref_offset'])
            ref['ref_x'] = self.__rc_ref_x
            ref['ref_y'] = self.__rc_ref_y
            ref['ref_v'] = env['ref_v']
            _, traj, exception_flag = self.__right_lc_idm.run(state, ref,env)
            self.__trajs['right_lc'] = traj
            # print("right lane change")
        # print("#super driver#   try right done")
        best = self.__decision()
        return  self.__trajs[best]

    def __decision(self):
        best = 'acc'
        reward = np.zeros((3,3),dtype=np.float32)
        reward[0,0],reward[0,1],reward[0,2] = self.__acc_idm.get_eval()
        if self.__trajs['left_lc'] is not None:
            # dist1 = self.__left_lc_idm.get_longitude_dist()
            # print(dist1)
            reward[1,0],reward[1,1],reward[1,2]= self.__left_lc_idm.get_eval()
        if self.__trajs['right_lc'] is not None:
            # dist2 = self.__right_lc_idm.get_longitude_dist()
            # print(dist2)
            reward[2, 0], reward[2, 1], reward[2, 2] = self.__right_lc_idm.get_eval()

        max=  reward[0,0]
        if reward[1,0] > max:
            best = 'left_lc'
            max = reward[1,0]
        if reward[2,0]>max:
            best = 'right_lc'
            max = reward[2,0]
        return best

    def __create_left_lc(self,env)->list:
        lc_env = list()
        if env['leader'] is not None:
            lc_env.append(env['leader'])
        if env['left_leader'] is not None:
            lc_env.append(env['left_leader'])
        if env['left_follower'] is not None:
            lc_env.append(env['left_follower'])
        return lc_env

    def __create_right_lc(self,env)->list:
        rc_env = list()
        if env['leader'] is not None:
            rc_env.append(env['leader'])
        if env['right_leader'] is not None:
            rc_env.append(env['right_leader'])
        if env['right_follower'] is not None:
            rc_env.append(env['right_follower'])
        return rc_env