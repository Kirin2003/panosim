"""


"""
import casadi as ca
import numpy as np
from oc_idm.idm_ctrller.idm_base import BaseOCPIDM


class FreeIDM(BaseOCPIDM):
    def __init__(self, **kwargs):
        super().__init__( **kwargs)

        # build graph
        self.__build()

    def __build(self):
        # define ctrl cmd vars
        # cmd[0,] = omega, cmd[1,] = ax
        cmd = ca.SX.sym('U', self.get_ctrl_dim(), self.get_ctrl_horizon())
        #  para[0] = vel
        #  para[1:self.pred_horizon+1] =  ref_x
        #  para[self.pred_horizon+1:1+ 2* self.pred_hoizon] = ref_y
        #  para[1+ 2* self.pred_horizon] = ref_vel
        para = ca.SX.sym('p', 1 + 2 * self.get_pred_horizon() + 1)  #

        # set init states,i.e. vel = p0
        states = ca.SX.zeros(4)
        states[3] = para[0]

        # calculate trajectory
        trajectory = []
        for i in range(self.get_pred_horizon()):
            if i < self.get_ctrl_horizon():
                inputs = cmd[:, i]
            else:
                inputs = cmd[:, -1]

            res = self.vm_func(states=states, inputs=inputs)
            states = res['states_next']
            trajectory.append(states)

        # calculate objective function
        obj = 0
        for i in range(self.get_ctrl_horizon()):
            obj_omega = cmd[0, i] * cmd[0, i]
            obj_ax = cmd[1, i] * cmd[1, i]
            obj = obj + self.get_weight_comf() * (obj_omega + obj_ax)

        for i in range(self.get_pred_horizon()):
            obj_x = (trajectory[i][0] - para[1 + i]) ** 2
            obj_y = (trajectory[i][1] - para[1 + self.get_pred_horizon() + i]) ** 2
            obj_vel = (trajectory[i][3] - para[1 + 2 * self.get_pred_horizon()]) ** 2
            obj = obj + self.get_weight_acc() * (obj_y + obj_x) + self.get_weight_vel() * obj_vel


        #----add constraints for x--------------
        self.lbx = list()
        self.ubx  = list()
        for i in range(self.get_ctrl_horizon()):
            self.lbx.append(-ca.inf)
            self.ubx.append(ca.inf)

        for i in range(self.get_ctrl_horizon()):
            self.lbx.append(-8)
            self.ubx.append(6)


        #----add constraints for gx--------------
        self.lbg = list()
        self.ubg  = list()
        g = list()

        #--------------------------------------
        for i in range(self.get_pred_horizon()):
            g.append(trajectory[i][3])  # v boundary
            self.lbg.append(0)
            self.ubg.append(ca.inf)


        opts_setting = {
            'ipopt.max_iter': 100,
            'ipopt.print_level': 0,
            'print_time': 0,
            'ipopt.acceptable_tol': 1e-6,
            'ipopt.acceptable_obj_change_tol': 1e-4
        }
        # ---------------------------
        nlp_prop = {
            'f': obj,
            'x': ca.reshape(cmd, -1, 1),
            'p': para,
            'g': ca.vertcat(*g)
        }
        # ---------------------------
        self.solver = ca.nlpsol('solver', 'ipopt', nlp_prop, opts_setting)

    def run(self, vel, ref_x, ref_y, ref_vel,env=None):
        if np.size(vel) != 1:
            raise RuntimeError("error vel")
        if np.size(ref_x) != self.get_pred_horizon():
            raise RuntimeError("error ref x")
        if np.size(ref_y) != self.get_pred_horizon():
            raise RuntimeError("error ref y")
        if np.size(ref_vel) != 1:
            raise RuntimeError("error ref vel")

        vel = np.reshape(vel, (-1, 1))
        ref_x = np.reshape(ref_x, (-1, 1))
        ref_y = np.reshape(ref_y, (-1, 1))
        ref_vel = np.reshape(ref_vel, (-1, 1))

        init_u = np.zeros(self.get_ctrl_horizon() * self.get_ctrl_dim())
        p = np.concatenate((vel, ref_x, ref_y, ref_vel))

        #-----update ctrl constraints-------------
        for i in range(self.get_ctrl_horizon()):
            self.lbx[i] = (-self.get_max_ay()/vel)
            self.ubx[i] = (self.get_max_ay()/vel)

        #----ipopt solve------------------------
        res = self.solver(x0=init_u, p=p,lbg=self.lbg,ubg=self.ubg,lbx=self.lbx,ubx=self.ubx)
        cmds = np.array(res['x'])
        # Note res['x'] is casadi.DM change to np: np.array(res['x'])
        u_sol = ca.reshape(res['x'], self.get_ctrl_dim(), self.get_ctrl_horizon())
        u_sol_np = np.array(u_sol)
        traj = self.gen_traj(u_sol_np, vel)
        return cmds, traj
