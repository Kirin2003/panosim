"""
Author:
    SUN-Hao
Description:
    Lane change IDM based on full constraints dynamic IDM

"""
import casadi as ca
import numpy as np
from oc_idm.idm_ctrller.idm_base import BaseOCPIDM


class SLCIDM(BaseOCPIDM):
    def __init__(self, **kwargs):
        super().__init__( **kwargs)

        self.__circle_x = np.zeros((self.get_pred_horizon(),self.get_circle_num_step()),dtype=np.float32)
        self.__circle_y = np.zeros((self.get_pred_horizon(),self.get_circle_num_step()), dtype=np.float32)
        self.__check_start  = 1
        # build graph
        self.__build()
        self.check_circle_x = None
        self.check_circle_y = None
        self.longitude_safety_yb = 3.5
        self.__longitude_dist = []

        self.__average_vel = None
        self.__average_ay = None
        self.__average_ax = None

        self.default_obs_x = -10

    def __build(self):
        # define ctrl cmd vars
        # cmd[0,] = omega, cmd[1,] = ax
        cmd = ca.SX.sym('U', self.get_ctrl_dim(), self.get_ctrl_horizon())

        #  ----------------------
        p_init_v = ca.SX.sym('inti_v')
        p_ref_x = ca.SX.sym('ref_x', self.get_pred_horizon())
        p_ref_y = ca.SX.sym('ref_y', self.get_pred_horizon())
        p_ref_v = ca.SX.sym('ref_v')
        # set init states,i.e. vel = p0
        states = ca.SX.zeros(4)
        states[3] = p_init_v[0]
        # calculate trajectory
        trajectory = []
        for i in range(self.get_pred_horizon()):
            if i < self.get_ctrl_horizon():
                inputs = cmd[:, i]
            else:
                inputs = cmd[:, -1]

            res = self.vm_func(states=states, inputs=inputs)
            states = res['states_next']
            trajectory.append(states)
        # calculate objective function
        obj = 0
        for i in range(self.get_ctrl_horizon()):
            obj_omega = cmd[0, i] * cmd[0, i]
            obj_ax = cmd[1, i] * cmd[1, i]
            obj = obj + self.get_weight_comf() * (obj_omega + obj_ax)

        for i in range(self.get_pred_horizon()):
            obj_x = (trajectory[i][0] - p_ref_x[i]) ** 2
            obj_y = (trajectory[i][1] - p_ref_y[i]) ** 2
            obj_vel = (trajectory[i][3] - p_ref_v) ** 2
            obj = obj + self.get_weight_acc() * (obj_y + obj_x) + self.get_weight_vel() * obj_vel

        #----add constraints for input-------------
        self.lbx = list()
        self.ubx  = list()
        for i in range(self.get_ctrl_horizon()):
            self.lbx.append(-ca.inf)
            self.ubx.append(ca.inf)

        for i in range(self.get_ctrl_horizon()):
            self.lbx.append(self.get_min_ax())
            self.ubx.append(self.get_max_ax())


        #----add constraints for fucntions of inputs--------------
        g = list()
        self.lbg = list()
        self.ubg  = list()

        for i in range(self.get_pred_horizon()):
            # v boundary
            g.append(trajectory[i][3])
            self.lbg.append(0.01) # min speed
            self.ubg.append(ca.inf) # max speed

        for i in range(self.get_pred_horizon()):
            # y boundary
            g.append(trajectory[i][1])
            self.lbg.append(-ca.inf)
            self.ubg.append(ca.inf)

        #------safe constraints---------------
        p_safety_x = list()
        p_safety_y = list()
        for j in range(self.get_circle_num_step()):
            p_centre_x = ca.SX.sym('centre_x', self.get_check_horizon() -self.__check_start )
            p_centre_y = ca.SX.sym('centre_y', self.get_check_horizon() -self.__check_start)

            p_safety_x.append(p_centre_x)
            p_safety_y.append(p_centre_y)
            for i in range(self.__check_start,self.get_check_horizon()): # for each step with safe constraints
                dist = (trajectory[i][0] - p_centre_x[i-self.__check_start])**2  + \
                       (trajectory[i][1] - p_centre_y[i-self.__check_start]) ** 2 # dist between two circles
                g.append(dist)
                self.lbg.append(self.get_safe_dist())
                self.ubg.append(ca.inf)

        # ---------------------------
        opts_setting = {
            'ipopt.max_iter': 100,
            'ipopt.print_level': 0,
            'print_time': 0,
            'ipopt.acceptable_tol': 1e-6,
            'ipopt.acceptable_obj_change_tol': 1e-4
        }

        # ---------------------------
        P = list()
        P.append(p_init_v)
        P.append(p_ref_x)
        P.append(p_ref_y)
        P.append(p_ref_v)
        P.extend(p_safety_x)
        P.extend(p_safety_y)

        nlp_prop = {
            'f': obj,
            'x': ca.reshape(cmd, -1, 1),
            'p': ca.vertcat(*P),
            'g':ca.vertcat(*g)
        }
        # ---------------------------
        self.solver = ca.nlpsol('solver', 'ipopt', nlp_prop, opts_setting)

    def pred_env(self, env):
        if env is None:
            return
        col = 0
        for obs in env:
            obs_preds = self.predict_const_vel(obs) # predict obs motion
            # print(obs)
            centres_num = 0
            for i in range(self.get_pred_horizon()):
                centres = self.gen_circle_centre(obs_preds[i])
                centres_num = len(centres)
                for j in range(centres_num):
                    self.__circle_x[i, col + j] = centres[j]['x']
                    self.__circle_y[i, col + j] = centres[j]['y']
            col += centres_num

    def eval_longitude_safty(self,traj):
        longitude_dist = []
        pred_size = self.get_pred_horizon()
        circle_num = self.get_circle_num_step()
        for i in range(pred_size):
            for j in range(circle_num):
                if self.__circle_x[i, j] <  self.default_obs_x + 1:
                    continue
                dy = np.fabs(traj[i+1][1] - self.__circle_y[i,j])
                # print("current",i,j,dy)
                if dy > self.longitude_safety_yb:
                    continue
                else:
                    dist = np.fabs(traj[i+1][0] - self.__circle_x[i,j])
                    longitude_dist.append(dist)
        return longitude_dist

    def run(self, state,ref_traj,env):
        # ---------------reset env----------------------
        self.reset_circle_centres()

        vel = state['vel']

        ref_x = ref_traj['ref_x']
        ref_y = ref_traj['ref_y']
        ref_vel = ref_traj['ref_v']

        obs_list = env['obs_list']
        speed_limit = env['speed_limit']
        rb_y = env['rb_y']
        lb_y = env['lb_y']

        exception_flag = 0
        # if env is None:
        #     raise RuntimeError("error env=None")
        if np.size(vel) != 1:
            print("error vel")
            exception_flag = 1
            return None, None, exception_flag
        if np.size(ref_x) != self.get_pred_horizon():
            print("desired size:",self.get_pred_horizon())
            print("error ref x:",ref_x,)
            exception_flag = 1
            return None, None, exception_flag
        if np.size(ref_y) != self.get_pred_horizon():
            print("error ref y")
            exception_flag = 1
            return None, None, exception_flag
        if np.size(ref_vel) != 1:
            print("error ref vel")
            exception_flag = 1
            return None, None, exception_flag

        vel = np.reshape(vel, (-1, 1))
        ref_x = np.reshape(ref_x, (-1, 1))
        ref_y = np.reshape(ref_y, (-1, 1))
        ref_vel = np.reshape(ref_vel, (-1, 1))

        #  slices
        self.pred_env(obs_list) # update circles
        self.check_circle_x = self.__circle_x[self.__check_start:self.get_check_horizon(),:]
        self.check_circle_y = self.__circle_y[self.__check_start:self.get_check_horizon(),:]
        circle_x = np.reshape(self.check_circle_x, (-1, 1))
        circle_y = np.reshape(self.check_circle_y, (-1, 1))

        init_u = np.zeros(self.get_ctrl_horizon() * self.get_ctrl_dim())
        p = np.concatenate((vel, ref_x, ref_y, ref_vel, circle_x, circle_y))

        if vel < self.get_lon_ctrl_min_vel(): # min speed  , vel: real velocity of host vehicle
            vel = self.get_lon_ctrl_min_vel()

        # -----update ctrl constraints-------------
        c_h = self.get_ctrl_horizon()
        for i in range(c_h):
            self.lbx[i] = (-self.get_max_ay()/vel)
            self.ubx[i] = (self.get_max_ay()/vel)

        # -----update function constraints-------------
        # boundary of vel
        p_h = self.get_pred_horizon()
        for i in range(p_h):
            # self.lbg[i] = ()
            self.ubg[i] = speed_limit # max speed
        #  update boundary  of y
        for i in range(p_h, 2*p_h):
            self.lbg[i] = rb_y[i-p_h]
            self.ubg[i] = lb_y[i-p_h]

        res = self.solver(x0=init_u, p=p,lbg=self.lbg,ubg=self.ubg,lbx=self.lbx,ubx=self.ubx)
        cmds = np.array(res['x'])
        # Note res['x'] is casadi.DM change to np: np.array(res['x'])
        u_sol = ca.reshape(res['x'], self.get_ctrl_dim(), self.get_ctrl_horizon())
        u_sol_np = np.array(u_sol)
        traj = self.gen_traj(u_sol_np, vel)
        self.__average_vel, self.__average_ax, self.__average_ay = self.eval_traj(u_sol_np, traj)
        self.__longitude_dist = self.eval_longitude_safty(traj)
        return cmds, traj ,exception_flag

    def get_check_circle_centres(self):
        return self.check_circle_x,self.check_circle_y

    def reset_circle_centres(self):
        # all circle centre reset 10m behind host vehicle
        self.__circle_x =  self.default_obs_x +  np.zeros((self.get_pred_horizon(), self.get_circle_num_step()), dtype=np.float32)
        self.__circle_y = np.zeros((self.get_pred_horizon(), self.get_circle_num_step()), dtype=np.float32)

    def get_eval(self):
        return self.__average_vel,self.__average_ax,self.__average_ay

    def get_longitude_dist(self):
        return self.__longitude_dist

    # def get_boundaries(self):
    #     return self.lb_x,self.lb_y,self.rb_x,self.rb_y

if __name__=="__main__":
    n = 10
    for i in range(n):
        print(i)

    for i in range(n,2*n):
        print(i)
