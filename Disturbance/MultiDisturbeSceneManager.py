# 脚本描述：     该文件提供一个场景车辆管理类myDisturbeSceneManager
# 版本：        v1.9
# 作者：        韩飞
# 说明：        场景管理，用于多车干扰功能的发车模型
# 日期：        2021-07-05

# 更新：       v0.1    2021-05-16    起草场景车辆管理类
# 更新：       v0.2    2021-05-17    添加场景管理初始化函数，以TopLeader车辆限制多车干扰整体交通的速度；
#                                   添加场景管理更新函数，判断需要删除的车辆（原则是超过TopLeader车辆且超过2秒的车辆），并删除车辆
# 更新：       v0.3    2021-05-18    修改语法错误，并完成初步验证
# 更新：       v0.4    2021-05-20    修改语法及用法错误，并完成初步验证
#                                   修改myDisturbeSceneManager::__myGetSceneDeleteVehicleList函数，在删除车辆的同时，增加一个发车
# 更新：       v0.5    2021-05-30    增加主车的行为识别，且在不同行为下，配置周围交通车
# 更新：       v0.6    2021-06-02    增加车辆管理的删除原则，增加交叉口处交通车处理列表条件
# 更新：       v0.7    2021-06-03    修改v10 API相关函数和调用
# 更新：       v0.8    2021-06-04    增加车辆管理的新增车辆原则；增加主车临近交叉口时，对驶出交叉口车辆的创建；
# 更新：       v0.9    2021-06-05    测试用发现deleteVehicle接口耗时严重，优化调用频次和性能，减少耗时
# 发布：       v1.0    2021-06-05    todo: [1] 需要判断一下主车是否超过了TopLeader?  -> 已解决
#                                         [2] 使用setSpeed和setLateral之后，车辆位置异常。如何保证主车在仅setSpeed后，不会产生换道？
#                                         [3] 使用addVehicleXY后，无法获得laneID，导致交叉口处车辆消失
#                                         [4] 在后方增加车辆的方法需要优化   -> 已解决
# 更新：       v1.1    2021-06-07    修改查找前车们的方法，优化车辆删除的方法
# 更新：       v1.2    2021-06-25    所有车辆初始速度，最低设为10m/s
#                                   todo: [1] 修改承接UI的“交通流密度”参数    -> 已解决
# 更新：       v1.3    2021-06-26    新增一个选择多车干扰的类myMultiDisturbeChosen，该类更新后，返回需要控制的“多车”，
#                                   和该步长需要删除的“多车”，用于下一步多车的控制。通过全局变量g_nDefaultMaxDisturbVehicleCount
#                                   可以调整最多控制几台干扰车
# 更新：       v1.4    2021-06-27    优化发车模型，扩大发车模型校验区域，防止车辆重叠
#                                   优化删车模型，车道内超过校验区域的车辆进行删除，防止车道内车辆过多
#                                   增加发车模型的车速
#                                   优化交叉口处的选车模型
#                                   发车和选车模型，增加时间间隔
# 更新：       v1.5    2021-06-30    更新myMultiDisturbeChosen的选车参数，和时间间隔
# 更新：       v1.6    2021-07-05    增加删车条件：车辆产生两次换道行为后500ms，删车；
#                                   调整“选车”和“删车”的距离参数，并以全局变量声明在文件最上方，方便调整；
# 更新：       v1.7    2021-08-13    执行效率优化
# 更新：       v1.8    2021-08-14    执行效率优化
# 更新：       v1.9    2021-08-16    执行效率优化
#                                   临近交叉口时，在所有可能的驶向车道，添加车辆，并使其静止，知道主车进入交叉口才开始启动
#                                   与主车在同一车道的前车，使用P参数控制加速度，从而更新位置，尽可能保持g_dMaxLeaderDistance的距离，其他头车设置为主车车速
#                                   修复bug
# 更新：       v2.0    2021-08-16    添加UI接口；添加删除干扰车条件，激活超过10秒后删除；
# 更新：       v2.1    2021-08-16    当头车消失后，添加一辆头车
# 发布：       v2.2    2021-08-16    更新主车前车、临道前车的速度，主车前车速度快于其他车道，临道前车们慢于主车。主车侧后车，快于主车。这样设计是为了保障，临车道有条件和意愿换道主车车道上；
#                                   更新选车函数，当临车道前车符合条件的数量足够，则不选择后方车辆（尝试）
# 更新：       v2.3    2021-08-26    更新各车道前车的更新速度，调整主车车道车速及间距；修复选车bug；
#                                   增加车辆侧向位移


import math
from numpy import random
import operator


from TrafficModelInterface import *
from PanoSimTrafficAPI2 import *

g_dMaxLeaderDistance = 150
g_dMaxFollowerDistance = 50
g_nHostVehicleID = 0

g_dicExceedVehicles = {}
g_dicLocalUserData = {}


g_dicAddedVehicleByXY = {}
g_dicVehicleLateralProperty = {}

g_nDefaultTrafficDensity = 20        # 这里可以修改交通流的密度  todo：编写UI接口
g_dDefaultMinVehicleSpeed = 5          # 这里可以修改交通车的最小初始车速
g_nDefaultMaxDisturbVehicleCount = 1    # 这里可以修改，最多支持几个多车干扰！ todo：编写UI接口

g_nDefaultAggressiveDriverPercent = 34  # 速度激进型占比34%
g_nDefaultNormalDriverPercent = 33      # 普通舒适型占比33%
g_nDefaultRookieDriverPercent = 33      # 安全愚蠢型占比33%


g_dCreateFocus_MaxLeaderDistance = 30       # “选车” 交通车与主车的最大间距（前向）   30-50
g_dCreateFocus_MaxFollowerDistance = 5     # "选车“ 交通车与主车的最大间距（后向），必须是正数，表示向后的距离   5-10
g_dRemoveFocus_MaxLeaderDistance = 30       # ”删车“ 交通车与主车的最大间距（前向）
g_dRemoveFocus_MaxFollowerDistance = 5      # ”删车“ 交通车与主车的最大间距（后向），必须是正数，表示向后的距离
g_dRemoveFocus_MaxDisturbDuration = 10000   # "删车“ 交通车已经激活干扰工况持续10s
# 选车范围，   20 > 主车 > -20;     删车范围，   ! 30 > 主车 > -5




MY_PANOSIM_INVALID_VALUE = -1073741824

def myGetRandomInt(min, max):
    d = myGetRandomDouble(min, max)
    return int(d)

def mySetValidRoute1(vehID, printFlag = False):
    #print("In set valid route1")
    laneID = myGetLaneID(vehID)
    if laneID == "": return False
    potentialDirs = getValidDirections(laneID)

    if len(potentialDirs) > 0:
        dir = potentialDirs[myGetRandomInt(0, len(potentialDirs))]
    else:
        if myIsDeadEnd(laneID):
            return True
        #print( "Veh:", vehID, "Current Lane:", laneID, "has no connection!" )
        return False

    #print("Set Valid Route - potentialDirs:", vehID, laneID, potentialDirs, dir)
    changeRoute(vehID, route_type(dir))
    if printFlag:
        print( "FromLane:", laneID, "Valid Dirs:", potentialDirs )
        print( "SetRoute", dir)
    return True

def myGetVehiclesInLane1(laneID):
    vehiclesInLane = getLaneVehicles(laneID)
    vehiclesInLane.reverse()
    return vehiclesInLane

def getVehicleModelingControlRights(vehId, disThreshold = 20):
    disToJunction = myGetDistanceToLaneEnd(vehId)
    if 0 <= disToJunction < disThreshold:
        return False
    if myCheckInternalLane(myGetLaneID(vehId)):
        return False
    else:
        return True




class myDisturbeSceneManager:

    def test(self, modelUserData):
        pass

    # 函数: mySceneInitialization
    # 用途: 设置场景管理的初始化状态
    # [o]: Bool -   True: 已完成车辆初始化；  False： 未完成车辆初始化
    def mySceneInitialization(self, modelUserData):
        global g_dicLocalUserData
        g_dicLocalUserData = modelUserData

        (x,y) = getXYFromSL("gneE3_3", 5, 0)

        if not myCheckHostVehicleInitialized():
            self.m_bInit = False
            self.__myReinitialized()
            return False
        elif self.m_bInit:
            return True
        else:
            self.m_bInit = True
            print("Host Position when Init:", myGetHostVehicleCoordinate(), myGetHostVehicleStationInLane(), myGetHostVehicleSpeed())
            (self.m_lGlobalLeaders, self.m_lVehicleList, self.m_nVehCountInLane, self.m_dVehIntervalInLane) = setInitializedVehicles(self.m_nTrafficDensity)
            self.m_nVehLeaderCount = self.m_nVehCountInLane * g_dMaxLeaderDistance/(g_dMaxLeaderDistance + g_dMaxFollowerDistance) - 1
            if self.m_nVehLeaderCount <= 0:
                self.m_nVehLeaderCount = 1

            return True

    # 函数: mySceneUpdate
    # 用途: 更新场景，期间包括补发车辆、删除车辆、更新TopLeader车辆，并返回场景管理状态
    # [o]: [vehicleList] -   返回需要被控制的车辆列表
    def mySceneUpdate(self):
        if not self.m_bInit: return []
        self.m_lVehicleList = []

        # todo: 这里因为无法直接设置行驶路径，临时措施。但是这样做也还是不行！
        global g_dicAddedVehicleByXY
        tmpDicAddedVehicleByXY = dict(g_dicAddedVehicleByXY)
        g_dicAddedVehicleByXY = {}
        for id in tmpDicAddedVehicleByXY:
            code = mySetValidRoute1(int(id))

        # 判断主车的行为，适时地增加仿真车辆
        (hostVehicleBehavior, isStatusChanged) = self.m_cHostVehicleBehaivorClass.updateHostVehicleStatus()
        if( hostVehicleBehavior == 0 or hostVehicleBehavior == 1 ) and isStatusChanged:  # 主车正常行驶或换道行驶
            # todo：需要判断一下主车是否超过了TopLeader
            pass
        elif hostVehicleBehavior == 2  and isStatusChanged:  # 主车接近交叉口
            self.__mySetVehicles4ApproachJunction()
            print( "Tips: setVehicles When Approaching Junction" )
        elif hostVehicleBehavior == 3  and isStatusChanged:  # 主车在交叉口中
            self.__mySetVehicles4InJunction() # 其他车道的车，直到主车驶入交叉口，再开始移动
            pass
        elif hostVehicleBehavior == 4  and isStatusChanged:  # 主车驶出交叉口，到正常的车道中
            self.__mySetVehicles4NextLane()
            self.m_nLastAddFollowerTime = myGetSimulationTimeStamp() - 650 #如果进入下一个车道，则立即开始更新车辆，这里调整了增加车辆的时钟
            print("Tips: setVehicles When into a new lane from a junction")
            pass

        self.m_lGlobalLeaders = self.m_cVehListManager.update()
        #tmpLeaders = self.m_cVehListManager.update()

        # 更新TopLeader交通车辆
        self.__mySceneUpdateTopLeaders()
        lDeleteVehicleList = self.__myGetSceneDeleteVehicleList(hostVehicleBehavior, isStatusChanged)  # 确定需要删除的交通车辆
        #print( "DeleteVehicles:", lDeleteVehicleList )

        # 删除需要被删除的车辆，即那些跑到TopLeader前面的车辆
        for id in lDeleteVehicleList:
            if id not in self.m_lGlobalLeaders:  # 不是头车，即一个交通车超车到头车前方，删除，并在该车道后方添加交通车
                myDeleteVehicle(id)
            else:  # 头车被删除了
                self.m_bNeedRescene = True

        # 根据场景情况，在车辆后方增加车辆
        if myGetSimulationTimeStamp() - self.m_nLastAddFollowerTime > 600 : #每0.6秒判断增加后方车辆
            if self.__myCheckAddVehicles():
                self.m_nLastAddFollowerTime = myGetSimulationTimeStamp()

        # 如果场景需要重建，即TopLeader被删除了，那么需要重新设定TopLeaders
        # if self.m_bNeedRescene:
        #     pass

        # 如果PanoSim提供的车辆列表中的车辆，不属于主车且不在需要被删除的交通车辆列表中，则需要被控制，插入控制列表
        for id in getVehicleList():
            if id != g_nHostVehicleID and id not in self.m_lGlobalLeaders:
                if getVehicleModelingControlRights(id, 80):  # 如果该车已经接近交叉口，则不再由交通模型控制
                    self.m_lVehicleList.append(id)
                #self.m_lVehicleList.append(id)

        #print( "@@@@@@ TopLeaders: ", self.m_lGlobalLeaders )
        # 从车辆列表中，选择多车干扰的控制车辆和本次删除的控制车辆
        (lDisturbVehicleChosen, lDisturbRemoveFocusID, lDisturbeVehicleCluster) = self.m_cDisturbVehicleChosenClass.updateDisturbeChosen(hostVehicleBehavior, self.m_cVehListManager, self.m_lVehicleList, myGetSimulationTimeStamp())
        #print( lDisturbVehicleChosen )
        return (self.m_lVehicleList, lDisturbVehicleChosen, lDisturbRemoveFocusID, lDisturbeVehicleCluster)

    def __init__(self, modelUserData):
        self.__myReinitialized()
        global g_dicLocalUserData
        g_dicLocalUserData = modelUserData

        # todo： UI参数传入
        global g_nDefaultTrafficDensity, g_nDefaultMaxDisturbVehicleCount
        global g_nDefaultAggressiveDriverPercent, g_nDefaultNormalDriverPercent, g_nDefaultRookieDriverPercent
        Global_in = modelUserData['parameters']
        # print("UI VehDensity:", float(Global_in["multiDisturbeVehDensity"]))
        # print("UI DisturbeVehCount:", float(Global_in["multiDisturbeVehCount"]))
        # print("UI DriverType_aggressive:", float(Global_in["multiDisturbeDriverType_aggressive"]))
        # print("UI DriverType_normal:", float(Global_in["multiDisturbeDriverType_normal"]))
        # print("UI DriverType_rookie:", float(Global_in["multiDisturbeDriverType_rookie"]))
        g_nDefaultTrafficDensity = float(Global_in["multiDisturbeVehDensity"])
        if g_nDefaultTrafficDensity < 10: g_nDefaultTrafficDensity = 10
        elif g_nDefaultTrafficDensity > 50: g_nDefaultTrafficDensity = 50
        self.m_nTrafficDensity = g_nDefaultTrafficDensity

        g_nDefaultMaxDisturbVehicleCount = float(Global_in["multiDisturbeVehCount"])
        if g_nDefaultMaxDisturbVehicleCount > 4: g_nDefaultMaxDisturbVehicleCount = 4
        elif g_nDefaultMaxDisturbVehicleCount < 1: g_nDefaultMaxDisturbVehicleCount = 1

        a = float(Global_in["multiDisturbeDriverType_aggressive"])
        b = float(Global_in["multiDisturbeDriverType_normal"])
        c = float(Global_in["multiDisturbeDriverType_rookie"])
        g_nDefaultAggressiveDriverPercent = a / (a+b+c)
        g_nDefaultNormalDriverPercent = b / (a+b+c)
        g_nDefaultRookieDriverPercent = c / (a+b+c)

        print( "*****  Multi-Disturbance-Flow UI Setting  *******" )
        print( "Density:", g_nDefaultTrafficDensity )
        print( "Disturbance vehicle count:", g_nDefaultMaxDisturbVehicleCount )
        print( "Driver Type: ",
               "[Aggresive] ", g_nDefaultAggressiveDriverPercent,
               "[Normal] ", g_nDefaultNormalDriverPercent,
               "[Rookie] ", g_nDefaultRookieDriverPercent)
        print("**************************************************")

        global g_dicExceedVehicles, g_dicAddedVehicleByXY
        g_dicExceedVehicles = {}
        g_dicAddedVehicleByXY = {}


    def __myReinitialized(self):
        self.m_bInit = False
        self.m_bNeedRescene = False
        self.m_lGlobalLeaders = []
        self.m_lVehicleList = []
        self.m_lAddedToLanesApproachingJunction = []
        self.m_nTrafficDensity = g_nDefaultTrafficDensity
        self.m_cHostVehicleBehaivorClass = myHostVehicleEstimation()
        self.m_cDisturbVehicleChosenClass = myMultiDisturbeChosen()
        self.m_nVehCountInLane = 0
        self.m_nVehLeaderCount = 0
        self.m_dVehIntervalInLane = 0
        self.m_nLastAddFollowerTime = 0
        self.m_nLastCheckAddTimeStamp = 0
        self.m_nLastCheckDeletedTimeStamp = 0
        self.m_cVehListManager = myVehListManager()

        global g_dicExceedVehicles
        g_dicExceedVehicles = {}

    # 更新TopLeader车辆，这里设置尽量让TopLeader车辆与主车保持一定领航的距离。
    def __mySceneUpdateTopLeaders(self):
        # self.__myCheckTopLeaderStatus()
        # hostCurrentLane = myGetLaneID(g_nHostVehicleID)
        # hostToJunction = myGetToJunction(hostCurrentLane)

        # for topLeaderID in self.m_lGlobalLeaders:
        #     if not getVehicleModelingControlRights(topLeaderID, 50):
        #         continue
        #     leaderCurrentLane = myGetLaneID(topLeaderID)
        #
        #     if leaderCurrentLane == hostCurrentLane:
        #         pass
        #         # leaderSpeed = myGetVehicleSpeed(topLeaderID)
        #         # leaderToJunction = myGetToJunction(leaderCurrentLane)
        #         # if hostToJunction != leaderToJunction:
        #         #     print("leader", topLeaderID, "is not in the same lane of host.  [", leaderCurrentLane, "]->[", hostCurrentLane, "]")
        #         #     deltaStation = math.sqrt( math.pow( myGetVehicleX(topLeaderID) - myGetVehicleX(g_nHostVehicleID),2 )\
        #         #                               + math.pow(myGetVehicleY(topLeaderID)-myGetVehicleY(g_nHostVehicleID), 2) )
        #         # else:
        #         #     deltaStation = myGetLongitudinalDistance(topLeaderID, g_nHostVehicleID)
        #         #
        #         # targetStation = g_dMaxLeaderDistance - 10
        #         # a = (targetStation - deltaStation) * 0.2
        #         # if a < -8:
        #         #     a = -8
        #         # elif a > 4:
        #         #     a = 4
        #         # #print( "Top Leader update: ", leaderSpeed, a, (targetStation - deltaStation), "ID:", topLeaderID, "in lane", leaderCurrentLane )
        #         # changeSpeed(topLeaderID, leaderSpeed + a * 0.01, 0.01)  # todo 采用的P调制，没有用PI
        #         # #changeLateralOffset(topLeaderID, 0.0)  # todo 添加该句后，会使这些车辆横在道上
        #     else:
        #         hostSpeed = myGetHostVehicleSpeed()
        #         changeSpeed(topLeaderID, hostSpeed * 0.9, 0.01)

        # 获得干扰车的前车和目标车道前车
        disturbEnvVehicles = {}
        for key in self.m_cDisturbVehicleChosenClass.m_dicVehicleChosenCluster.keys():
            disturbInfo = self.m_cDisturbVehicleChosenClass.m_dicVehicleChosenCluster[key]
            disturbID = disturbInfo[0]
            laneDirType = disturbInfo[2]

            if laneDirType == 0: # 直行
                leader = getLeaderVehicle( disturbID )
                #disturbEnvVehicles[ str(leader) ] = [disturbID, leader, 0, -1 ] # 0->无所谓,  -1->减速
            elif laneDirType == 1: # 左转
                leftLeader = getLeftLeaderVehicle(disturbID)
                #disturbEnvVehicles[ str(leftLeader) ] = [disturbID, leftLeader, 0, 1]
                leader = getLeaderVehicle( disturbID )
                disturbEnvVehicles[ str(leader) ] = [disturbID, leader, leftLeader, -1] # leftLeader需要参考位置的ID，  -1->减速
            else:
                rightLeader = getRightLeaderVehicle(disturbID)
                #disturbEnvVehicles[ str(rightLeader) ] = [disturbID, rightLeader, 0, 1]
                leader = getLeaderVehicle( disturbID )
                disturbEnvVehicles[ str(leader) ] = [disturbID, leader, rightLeader, -1] # leftLeader需要参考位置的ID，  -1->减速

        # 更新所有leader车辆
        controlVMap = {}
        (lanes, dirs) = self.m_cVehListManager.getLanesInEdge()
        idx = 0
        hostSpeed = myGetHostVehicleSpeed()
        if hostSpeed < 5:
            hostSpeed = 5
        for laneID in lanes:
            dir = dirs[idx]
            idx += 1
            if -1<=dir<=1:
                leaders = self.m_cVehListManager.getLeaders(laneID, g_dMaxLeaderDistance)
            else:
                continue

            vehCount = len(leaders)
            if vehCount <= 0: continue
            if dir == 0:
                expSpeed = hostSpeed * 1.4
                changeSpeed(leaders[0], hostSpeed, 0.01)
                vehIdx = 1
                interval1 = self.m_dVehIntervalInLane
                interval2 = g_dMaxLeaderDistance / vehCount
                if interval1 < interval2 :
                    interval = interval1
                else:
                    interval = interval2
                while( vehIdx < vehCount ):
                    #v = myIDMFollower( leaders[vehIdx-1], leaders[vehIdx], expSpeed, 7, 1, 0.5 )
                    v = myIDMFollower(leaders[vehIdx - 1], leaders[vehIdx], expSpeed, 7, interval, 0.5)
                    vSafe = 9999
                    if vehIdx == vehCount - 1: # 主车前车
                        vSafe = myIDMFollower(leaders[vehIdx - 1], leaders[vehIdx], expSpeed, 7, 2, 0.5)
                        if vSafe < v:
                            vSafe = v
                    controlVMap[ str(leaders[vehIdx]) ] = [v, vSafe]
                    #changeSpeed(leaders[vehIdx], v, 0.01)
                    vehIdx += 1

                sameLaneLeaderThreshold = self.m_nVehCountInLane * g_dMaxLeaderDistance / (g_dMaxLeaderDistance + g_dMaxFollowerDistance)
                if sameLaneLeaderThreshold <= 2:
                    sameLaneLeaderThreshold = 2
                if vehCount > sameLaneLeaderThreshold: # 如果主车前方车辆过多，则删除最中间的车
                    deleteIdx = int(vehCount / 2)
                    myDeleteVehicle( leaders[ deleteIdx ] )
                    controlVMap.pop( str(leaders[ deleteIdx ]) )
                    #print( "#### Delete due to more vehicles in leader pos!" )

            else:
                expSpeed = hostSpeed * 0.8
                changeSpeed(leaders[0], hostSpeed, 0.01)
                vehIdx = 1
                while(vehIdx < vehCount):
                    if vehIdx == vehCount-1:
                        deltaS = myGetLongitudinalDistance(leaders[vehIdx], g_nHostVehicleID)
                        if deltaS < 30:
                            expSpeed = hostSpeed * 1.05
                    v = myIDMFollower( leaders[vehIdx-1], leaders[vehIdx], expSpeed, 2, 10, 2 )
                    #changeSpeed(leaders[vehIdx], v, 0.01)
                    controlVMap[str(leaders[vehIdx])] = [v, 999]
                    vehIdx += 1
                followers = self.m_cVehListManager.getFollowers(laneID, g_dMaxFollowerDistance)
                if len(followers) > 0:
                    countLeaders = len(leaders)
                    if countLeaders > 0:
                        leaderVehicle = leaders[ countLeaders-1 ]
                        v = myIDMFollower(leaderVehicle, followers[0], hostSpeed * 1.2, 9, 3, 0.5)
                        changeSpeed(followers[0], v, 0.01)
                    else:
                        v = hostSpeed * 1.5
                        changeSpeed(follower[0], v, 0.01)

        ownControlled = []
        for vehID in controlVMap.keys():
            if vehID in disturbEnvVehicles:
                envInfo = disturbEnvVehicles[ str(vehID) ]
                envID = envInfo[1]
                refID = envInfo[2]
                speedDir = envInfo[3]
                envSpeed = myGetVehicleSpeed(envID)
                # if refID > 0 and str(refID) in controlVMap and envSpeed > myGetVehicleSpeed(refID):
                #     changeSpeed(int(refID), controlVMap[str(refID)][1], 0.01)
                #     print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^Control host leader as aggressive!")
                #     #controlVMap.pop(str(refID))
                #     ownControlled.append( int(refID) )

                # if refID == 0:
                #     nextSpeed = envSpeed - 4 * 0.01
                #     if nextSpeed < 0: nextSpeed = 0
                #     changeSpeed( envID, nextSpeed, 0.01 )
                #     ownControlled.append( envID )
                # else:
                #     if refID < 0:
                #
                #     sEnv = myGetDistanceFromLaneStart(envID)
                #     sRef = myGetDistanceFromLaneStart(refID)
                #     if sRef < sEnv:
                #         changeSpeed(  )
            if int(vehID) not in ownControlled:
                changeSpeed( int(vehID), controlVMap[vehID][0], 0.01 )
                mySetLateralOffset(int(vehID))

        # leaders = self.m_cVehListManager.getLeaders( myGetLaneID(g_nHostVehicleID), g_dMaxLeaderDistance )
        # hostSpeed = myGetHostVehicleSpeed()
        # for id in leaders:
        #     changeSpeed(id, hostSpeed * 1.2, 0.01)

    # 这里校验TopLeader是否应该被删除：
    # 如果topLeader驶出场景，则在列表中删除ID，
    # 如果topLeader不在是主车的车道，则从场景中删除，并添加新的头车
    def __myCheckTopLeaderStatus(self):
        lNotExistList = []
        for topLeaderID in self.m_lGlobalLeaders:
            if topLeaderID not in getVehicleList():
                lNotExistList.append(topLeaderID)

        for id in lNotExistList:
            self.m_lGlobalLeaders.remove(id)

    # 确认需要删除的车辆，并返回删除车辆的列表，同时需要在相同的车道增加一个车辆
    # 基本原则是，TopLeader的前车们，并持续了2秒以上
    def __myGetSceneDeleteVehicleList(self, hostVehicleBehavior, isStatusChanged):
        vehDeleteList = []
        curTimeStamp = myGetSimulationTimeStamp()
        # 如果有车辆超过头车，则删除
        for topLeader in self.m_lGlobalLeaders:
            leaders = myGetLeaders(topLeader)
            for exceedVehicleID in leaders:
                global g_dicExceedVehicles
                lastExceedTime = g_dicExceedVehicles[str(exceedVehicleID)]
                if (curTimeStamp - lastExceedTime > 3500):  # 如果比上一个超过时间超3.5秒以上还没被处理，证明这个时间是初始化时间
                    g_dicExceedVehicles[str(exceedVehicleID)] = curTimeStamp
                elif (curTimeStamp - lastExceedTime > 2000):  # 如果比上一个超过时间超2.0秒以上，则删除车辆
                    vehDeleteList.append(exceedVehicleID)
                else:
                    pass

        # 需要判断一下，当车辆在常规车道中行驶时，哪些车辆需要被删除，
        # 这里期望时删除那些非主车edge且距离大于50m的车辆
        global g_nLastCheckDeletedTimeStamp
        if( curTimeStamp - self.m_nLastCheckDeletedTimeStamp > 2000 ):
            lanes = []
            dirs = []
            self.m_nLastCheckDeletedTimeStamp = curTimeStamp
            hostLaneID = myGetLaneID(g_nHostVehicleID)
            if (hostLaneID == ""):
                print("Tips: host vehicle is not located in a lane!")
            else:
                (lanes, dirs) = self.m_cVehListManager.getLanesInEdge()

            if(hostVehicleBehavior==4 and isStatusChanged == True) or (hostVehicleBehavior==0 or hostVehicleBehavior==1):  # 主车刚刚进入下一个车道
                if len(lanes) > 0:
                    lTempVehicleList = getVehicleList()
                    for vehId in lTempVehicleList:
                        if(vehId not in vehDeleteList) and (vehId != g_nHostVehicleID) and (vehId not in self.m_lGlobalLeaders):
                            vehLaneID = myGetLaneID(vehId)
                            if( vehLaneID not in lanes):
                                (hostX, hostY) = myGetHostVehicleCoordinate()
                                (vehX, vehY) = myGetVehicleCoordinate(vehId)
                                distanceBetween = math.sqrt( math.pow(hostX-vehX,2) + math.pow(hostY-vehY,2) )
                                if( distanceBetween > g_dMaxLeaderDistance ):
                                    vehDeleteList.append(vehId)
                pass

            if(hostVehicleBehavior==0 or hostVehicleBehavior==1): # 主车在车道中行驶（车道保持 或 换道）
                if len(lanes) > 0:
                    func_getFollowers = self.m_cVehListManager.getFollowers
                    func_getLeaders = self.m_cVehListManager.getLeaders
                    for laneID in lanes:
                        if laneID == "": continue
                        #(followers1, leaders1) = myGetAllVehiclesWithHostVehicleReleated( laneID, g_dMaxLeaderDistance + 20, g_dMaxFollowerDistance + 20 )
                        followers = func_getFollowers(laneID, g_dMaxFollowerDistance + 20)
                        leaders = func_getLeaders(laneID, g_dMaxLeaderDistance + 20)
                        vehicleListInLane = myGetVehiclesInLane1(laneID)
                        for vehID in vehicleListInLane:
                            if vehID not in followers and vehID not in leaders and vehID != g_nHostVehicleID:
                                #print( "Delete because beyond distance" )
                                vehDeleteList.append(vehID)

        return vehDeleteList


    # # 获得所有的当前车道前车
    # def __myGetLeadersInCurLane(self, vehID):
    #     leaders = []
    #     targetID = vehID
    #     targetID = getLeaderVehicle(targetID)
    #     while targetID != -1 and targetID != MY_PANOSIM_INVALID_VALUE:
    #         leaders.append(targetID)
    #         targetID = getLeaderVehicle(targetID)
    #
    #     return leaders

    # 根据不同的主车状态，判断是否增加车辆
    # 情况2： 主车即将进入交叉口，在交叉口的其他驶入车道上增加交通车辆
    def __mySetVehicles4ApproachJunction(self):
        defaultInterval = self.m_dVehIntervalInLane  #车道中各车辆的间距
        defaultStartStation = 15 #头车距交叉口的距离
        vehCount = 6
        defaultVehicleSpeed = 20.0/3.6
        lAddVehicleList = []
        curLane = myGetLaneID(g_nHostVehicleID)
        (lanesInEdge, dirsInEdge) = self.m_cVehListManager.getLanesInEdge()

        # 在交叉口的incomingLanes上增加车辆
        junctionID = myGetToJunction(curLane)
        incomingLanes = myGetIncomingLanes(junctionID)
        for laneID in incomingLanes:
            if laneID in lanesInEdge : continue

            laneLength = myGetLaneLength(laneID)
            idx = 0
            sumStation = defaultStartStation
            while idx < vehCount:
                dstId = myAddVehicleInLane( laneID, laneLength - sumStation, defaultVehicleSpeed, )
                if dstId >= 0 :
                    sumStation = sumStation + defaultInterval * random.ranf() * defaultInterval
                    lAddVehicleList.append(dstId)
                idx = idx + 1

        # 在主车可能行驶的车道们上增加车辆   junction的去向车道
        lToLanes = myGetAllToLanes(curLane)
        if len(lToLanes) > 0 :
            #print( "Approaching junction, to all to lanes:", lToLanes )
            for toLane in lToLanes: #获得可能去向的车道
                #print( "Approaching junction, XXX Lane:", toLane )
                (toEdges, toEdgesDir) = myGetAllLanesInEdge(toLane) #获得可能去向的Edge中的所有车道
                # dstId = myAddVehicleInLane(toLane, 10, 0)
                # if dstId > 0:
                #     lAddVehicleList.append(dstId)
                #print("Approaching junction, XXX Edge:", toEdges)
                for laneIter in toEdges:
                    #print("Approaching junction, one of the toEdge:", edgeIter)
                    #for laneIter in edgeIter:
                    #print("Approaching junction, one of the toLane:", laneIter)
                    #dstId = myAddVehicleInLane(laneIter, 5, 0)
                    s = 5
                    interval = myGetRandomDouble(self.m_dVehIntervalInLane/2, self.m_dVehIntervalInLane*3/2)
                    s += interval
                    while (1):
                        dstId = myAddVehicleInLane(laneIter, s, 0)
                        #print( "Add vehicle in", toLane, " station:", s )
                        if dstId > 0:
                            s += interval + myGetRandomDouble(-interval/3, interval/3)
                            lAddVehicleList.append(dstId)
                            changeSpeed(dstId, 0, 100)
                            self.m_lAddedToLanesApproachingJunction.append( dstId )
                            if s > g_dMaxLeaderDistance:
                                break
                        else:
                            #print( "Tips: end until vehicle beyond length of lane" )
                            break
        #print( "Add vehicles when approaching junction:", lAddVehicleList )
        return lAddVehicleList

    # 情况3： 主车已经在某一个交叉口中
    def __mySetVehicles4InJunction(self):
        for id in self.m_lAddedToLanesApproachingJunction:
            changeSpeed( id, 10/3.6, 0.02 )
        pass

    # 情况4： 主车驶入了下一条车道，这时需要确立新的TopLeader车辆，并判断是否需要补车
    def __mySetVehicles4NextLane(self):
        idx = 0
        finalLeaders = []
        hostLaneID = myGetLaneID(g_nHostVehicleID)
        if myCheckInternalLane(hostLaneID) :
            #print( "Error when setting vehicle for next lane, due to the host vehicle is still in junction" )
            return
        (allLanes, laneDir) = self.m_cVehListManager.getLanesInEdge()

        baseDeltaStation = g_dMaxLeaderDistance * 0.3
        if baseDeltaStation > 50:
            baseDeltaStation = 50.0
        elif baseDeltaStation < -50:
            baseDeltaStation = -50.0

        if idx == 0: randomDeltaStation = 0
        elif idx % 2 == 1: randomDeltaStation = myGetRandomDouble( 10, 10 + baseDeltaStation )
        else: randomDeltaStation = myGetRandomDouble(0, baseDeltaStation/2)

        for laneID in allLanes:
            lLeaders = self.m_cVehListManager.getLeaders( laneID, g_dMaxLeaderDistance )
            leaderCount = len(lLeaders)

            if leaderCount == 0:
                newID = myAddVehicleRelated(g_nHostVehicleID, g_dMaxLeaderDistance*2/3, 0, myGetHostVehicleSpeed(), laneDir[idx])
                if idx == 0:
                    newID = myAddVehicleRelated(g_nHostVehicleID, g_dMaxLeaderDistance*4/5, 0, myGetHostVehicleSpeed(), laneDir[idx])
                else:
                    newID = myAddVehicleRelated(g_nHostVehicleID, g_dMaxLeaderDistance - randomDeltaStation, 0, myGetHostVehicleSpeed(), laneDir[idx])
                if newID < 0:
                    #print( "****", randomDeltaStation, g_dMaxLeaderDistance - randomDeltaStation, laneDir[idx] )
                    print("Error when add vehicles due to nextLane in lane!")
                else:
                    finalLeaders.append(newID)
            else:
                maxLeaderDeltaDistance = myGetLongitudinalDistance( lLeaders[leaderCount-1], g_nHostVehicleID )
                if maxLeaderDeltaDistance < g_dMaxLeaderDistance*2/3 :
                    if idx == 0 :
                        newID = myAddVehicleRelated(g_nHostVehicleID, g_dMaxLeaderDistance*4/5, 0, myGetHostVehicleSpeed(), laneDir[idx])
                    else:
                        newID = myAddVehicleRelated(g_nHostVehicleID, g_dMaxLeaderDistance - randomDeltaStation, 0, myGetHostVehicleSpeed(), laneDir[idx])
                    if newID < 0:
                        #print("####", randomDeltaStation, g_dMaxLeaderDistance - randomDeltaStation, laneDir[idx])
                        print("Error when add vehicles due to nextLane in lane!")
                    else:
                        finalLeaders.append(newID)
                else:
                    finalLeaders.append(lLeaders[leaderCount-1])

            idx = idx + 1
        #self.m_lGlobalLeaders = finalLeaders
        #print( "New Global Leaders due to a new lane", self.m_lGlobalLeaders )

    # 实时监控道路中的交通情况，如果主车station超过maxFollowerDistance，且车道内车辆数不足密度要求或主车后方没有车辆，则插入一个车辆。
    def __myCheckAddVehicles(self):
        vehicleAdded = False
        curTimeStamp = myGetSimulationTimeStamp()
        if curTimeStamp - self.m_nLastCheckAddTimeStamp < 1500 : return
        self.m_nLastCheckAddTimeStamp = curTimeStamp
        hostLaneID = myGetLaneID(g_nHostVehicleID)
        (lanesInEdge, lanesDir) = self.m_cVehListManager.getLanesInEdge()
        if len(lanesInEdge) > 0:
            idx = 0
            newID = -1
            hostStation = myGetHostVehicleStationInLane()

            func_getFollowers = self.m_cVehListManager.getFollowers #getFollowers function-call
            func_getLeaders = self.m_cVehListManager.getLeaders #getLeaders function-cal

            for laneID in lanesInEdge:
                followers = func_getFollowers(laneID, g_dMaxFollowerDistance+20)
                leaders = func_getLeaders(laneID, g_dMaxLeaderDistance)
                vehCount = len(followers) + len(leaders)

                # 添加后方车辆
                if vehCount < self.m_nVehCountInLane * 1.2:
                    if len(followers) == 0:
                        newID = myAddVehicleInLane( laneID, hostStation - g_dMaxFollowerDistance, mySpeedSetting(myGetHostVehicleSpeed()*1.5) )
                    else: #如果最后一个follower的位置超过最大距离的15米，则增加车辆
                        lastVeh = followers[0]
                        deltaDistance = myGetLongitudinalDistance(lastVeh, g_nHostVehicleID)
                        if deltaDistance > (-g_dMaxFollowerDistance + 30) :
                            newID = myAddVehicleInLane(laneID, hostStation - g_dMaxFollowerDistance, mySpeedSetting(myGetHostVehicleSpeed()*1.5))

                # todo 添加前方车辆
                hostDistanceToJunction = myGetDistanceToLaneEnd(g_nHostVehicleID)
                hostDistance = myGetDistanceFromLaneStart(g_nHostVehicleID)
                if hostDistanceToJunction > g_dMaxLeaderDistance:
                    if len(leaders) <= 0:
                        #newID = myAddVehicleRelated(g_nHostVehicleID, g_dMaxLeaderDistance-30, 0, mySpeedSetting(myGetHostVehicleSpeed()*0.5), lanesDir[idx])
                        newID = myAddVehicleInLane( laneID, hostDistance + g_dMaxLeaderDistance - 30, mySpeedSetting(myGetHostVehicleSpeed()*0.5) )
                        print( "Tips: Add leader vehicles [", newID, "] in lane [", laneID,"]" )
                    else:
                        deltaS = myGetLongitudinalDistance(leaders[0], g_nHostVehicleID)
                        if deltaS < g_dMaxLeaderDistance / 2:
                            #newID = myAddVehicleRelated(g_nHostVehicleID, g_dMaxLeaderDistance - 30, 0, mySpeedSetting(myGetHostVehicleSpeed()*0.5), lanesDir[idx])
                            newID = myAddVehicleInLane(laneID, hostDistance + g_dMaxLeaderDistance - 30, mySpeedSetting(myGetHostVehicleSpeed() * 0.5))
                            print("Tips: Add leader vehicles [", newID, "] in lane [", laneID, "]")
                        elif len(leaders) == 1 and laneID!=hostLaneID:
                            newID = myAddVehicleInLane(laneID, hostDistance + g_dMaxLeaderDistance - 30, myGetVehicleSpeed(leaders[0]))
                            print("Tips: Add leader vehicles [", newID, "] in lane [", laneID, "]")
                        elif laneID!=hostLaneID:
                            sL = myGetDistanceFromLaneStart(leaders[0])
                            ssL = myGetDistanceFromLaneStart(leaders[1])
                            if sL - ssL > self.m_dVehIntervalInLane * 1.5:
                                newID = myAddVehicleInLane(laneID, sL - self.m_dVehIntervalInLane/2, myGetVehicleSpeed(leaders[0]))
                                print("Tips: Add leader vehicles [", newID, "] in lane [", laneID, "]")

                idx = idx + 1
        return vehicleAdded



    # members
    m_bInit = False
    m_lVehicleList = []
    m_lGlobalLeaders = []
    m_lAddedToLanesApproachingJunction = []
    m_nTrafficDensity = g_nDefaultTrafficDensity


    m_nVehCountInLane = 0
    m_dVehIntervalInLane = 0
    m_nVehLeaderCount = 0


    m_nLastAddFollowerTime = 0
    m_nLastCheckDeletedTimeStamp = 0
    m_nLastCheckAddTimeStamp = 0

    m_bNeedRescene = False
    m_cHostVehicleBehaviorClass = 0
    m_cDisturbVehicleChosenClass = 0
    m_cVehListManager = 0

class myVehListManager:
    def __init__(self):
        self.vehLists = {}

    def update(self):
        self.vehLists = {}
        lLaneLeaders = []
        (self.lanesInEdge, self.lanesDir) = myGetAllLanesInEdge(myGetLaneID(g_nHostVehicleID))
        for i in self.lanesInEdge:
            listInst = myVehListInstance( i )
            listInst.update( i )
            if len(listInst.leaders) > 0:
                lLaneLeaders.append( listInst.leaders[0] )
            self.vehLists[str(i)] = listInst
        return lLaneLeaders

    def getFollowers(self, laneID, maxFollowerDistance):
        if laneID in self.vehLists:
            return self.vehLists[laneID].getFollowers(maxFollowerDistance)
        return []
    def getLeaders(self, laneID, maxLeaderDistance):
        if laneID in self.vehLists:
            return self.vehLists[laneID].getLeaders(maxLeaderDistance)
        return []
    def getLanesInEdge(self):
        return self.lanesInEdge, self.lanesDir

    vehLists = {}
    lanesInEdge = []
    lanesDir = []

class myVehListInstance:
    def __init__(self, laneID):
        self.laneID = laneID
        self.followers = []
        self.leaders = []
        self.normalFollowerDistance = g_dMaxFollowerDistance + 20
        self.normalLeaderDistance = g_dMaxLeaderDistance + 20

    def update(self, laneID):
        (self.followers, self.leaders) = myGetAllVehiclesWithHostVehicleReleated(laneID, self.normalLeaderDistance, self.normalFollowerDistance)
    def getFollowers(self, maxFollowerDistance):
        if maxFollowerDistance >= self.normalFollowerDistance:
            return self.followers
        else:
            tmpFollower = list(self.followers)
            idx = len(tmpFollower)-1
            while idx >= 0:
                vehID = tmpFollower[idx]
                deltaS = myGetLongitudinalDistance( vehID, g_nHostVehicleID)
                if deltaS < -maxFollowerDistance:
                    tmpFollower.remove(vehID)
                    idx -= 1
                    continue
                else:
                    break
            return tmpFollower
    def getLeaders(self, maxLeaderDistance):
        if maxLeaderDistance >= self.normalLeaderDistance:
            return self.leaders
        else:
            tmpLeader = list(self.leaders)
            idx = 0
            count = len(tmpLeader)
            while idx < count:
                vehID = tmpLeader[idx]
                deltaS = myGetLongitudinalDistance( vehID, g_nHostVehicleID )
                if deltaS > maxLeaderDistance:
                    tmpLeader.remove(vehID)
                    count -= 1
                else:
                    break
            return tmpLeader
    laneID = ""
    followers = []
    leaders = []
    normalFollowerDistance = 0
    normalLeaderDistance = 0


class myHostVehicleEstimation:

    def __init__(self):
        self.reset()

    def reset(self):
        self.m_nHostVehicleBehavior = -1
        self.m_nLastHostVehicleBehavior = -1
        self.m_dLastDistanceToJunction = MY_PANOSIM_INVALID_VALUE
        self.m_dLastVehicleSpeed = MY_PANOSIM_INVALID_VALUE
        self.m_nLastLaneID = -1
        self.m_plCheckFun = [ self.__checkApproachJunction, self.__checkInJunction, self.__checkEnterLane, self.__checkLaneChange ]
        self.m_nCheckInJunctionIdx = 1
        self.m_nCheckApproachJunctionIdx = 0
        self.m_nCheckLaneChangeIdx = 3
        self.m_nCheckEnterLaneIdx = 2

    def updateHostVehicleStatus(self):
        newSpeed = myGetHostVehicleSpeed()
        newDistanceToJunction = myGetDistanceToLaneEnd(g_nHostVehicleID)
        newLaneID = myGetLaneID(g_nHostVehicleID)
        #print( newDistanceToJunction, newLaneID  )
        if self.m_nHostVehicleBehavior == -1:
            #print("###@@@ -1")
            isInited = myCheckHostVehicleInitialized()
            if isInited == True :
                self.m_nHostVehicleBehavior = 0
                self.m_nLastHostVehicleBehavior = self.m_nHostVehicleBehavior
                self.m_nLastLaneID = myGetLaneID(g_nHostVehicleID)
                self.m_dLastVehicleSpeed = myGetHostVehicleSpeed()
                if myCheckInternalLane(self.m_nLastLaneID) :
                    self.m_dLastDistanceToJunction = MY_PANOSIM_INVALID_VALUE
                    #print( "Warning: the host vehicle is initialized in a junction" )
                else:
                    self.m_dLastDistanceToJunction = myGetDistanceToLaneEnd(g_nHostVehicleID)
            else:
                return -1
        elif self.m_nHostVehicleBehavior == 0 or self.m_nHostVehicleBehavior == 1:
            #print("###@@@ 0 1")
            if self.m_plCheckFun[self.m_nCheckInJunctionIdx]() :  #进入junction
                self.m_nHostVehicleBehavior = 3
                newDistanceToJunction = MY_PANOSIM_INVALID_VALUE
                print( "### Warning: Host vehicle is from a lane to a junction directly without approaching junction")
            elif self.m_plCheckFun[self.m_nCheckApproachJunctionIdx]() :  #接近junction
                self.m_nHostVehicleBehavior = 2
            elif self.m_plCheckFun[self.m_nCheckEnterLaneIdx]() :  #驶入下一条车道
                self.m_nHostVehicleBehavior = 4
                print( "### Warning: Host vehicle is from a lane to next lane directly without via junction!" )
            elif self.m_plCheckFun[self.m_nCheckLaneChangeIdx]() :  #主车换道
                self.m_nHostVehicleBehavior = 1
            else:
                self.m_nHostVehicleBehavior = 0
        elif self.m_nHostVehicleBehavior == 2 :
            #print("###@@@ 2")
            if self.m_plCheckFun[self.m_nCheckInJunctionIdx]() :
                self.m_nHostVehicleBehavior = 3
                newDistanceToJunction = MY_PANOSIM_INVALID_VALUE
            else:
                pass
        elif self.m_nHostVehicleBehavior == 3:
            #print("###@@@ 3")
            if self.m_plCheckFun[self.m_nCheckEnterLaneIdx]() :
                self.m_nHostVehicleBehavior = 4
            else:
                pass
        elif self.m_nHostVehicleBehavior == 4:
            #print("###@@@ 4")
            self.m_nHostVehicleBehavior = 0


        self.m_dLastDistanceToJunction = newDistanceToJunction
        self.m_dLastVehicleSpeed = newSpeed
        self.m_nLastLaneID = newLaneID

        if self.m_nHostVehicleBehavior == self.m_nLastHostVehicleBehavior :
            isFirstChanged = False
        else:
            isFirstChanged = True
            #print("###@@@ HostVehicle is ", self.m_nHostVehicleBehavior)

        self.m_nLastHostVehicleBehavior = self.m_nHostVehicleBehavior
        return (self.m_nHostVehicleBehavior, isFirstChanged)


    def __checkApproachJunction(self):
        newDistanceToJunction = myGetDistanceToLaneEnd(g_nHostVehicleID)
        curLaneID = myGetLaneID(g_nHostVehicleID)
        if myCheckInternalLane(curLaneID) : return False
        else:
            timeToJunction = 9999
            if myGetHostVehicleSpeed() > 0.1 : timeToJunction = newDistanceToJunction / myGetHostVehicleSpeed()
            if newDistanceToJunction<30 or timeToJunction < 3.0 :
                return True
            else:
                return False

    def __checkInJunction(self):
        curLaneID = myGetLaneID(g_nHostVehicleID)
        if myCheckInternalLane(curLaneID) :
            return True
        else:
            return False

    def __checkLaneChange(self):
        curLaneID = myGetLaneID(g_nHostVehicleID)
        if curLaneID == self.m_nLastLaneID: return False
        else:
            if myCheckInternalLane(curLaneID) == myCheckInternalLane(self.m_nLastLaneID): return True
            else: return False

    def __checkEnterLane(self):
        curLaneID = myGetLaneID(g_nHostVehicleID)
        if curLaneID == self.m_nLastLaneID: return False
        else:
            if not myCheckInternalLane(curLaneID) and myCheckInternalLane(self.m_nLastLaneID): return True
            else: return False


    ### -1->0; 0->1,2; 1->0,2; 2->3; 3->4; 4->0
    m_nHostVehicleBehavior = -1  # -1: Init, 0: keeping, 1: laneChange, 2: approachJunction, 3: in junction, 4: next lane
    m_nLastHostVehicleBehavior = -1
    m_dLastDistanceToJunction = -99999
    m_dLastVehicleSpeed = -99999
    m_nLastLaneID = -1

    m_plCheckFun = []
    m_nCheckInJunctionIdx = -1
    m_nCheckApproachJunctionIdx = -1
    m_nCheckLaneChangeIdx = -1
    m_nCheckEnterLaneIdx = -1





class myMultiDisturbeChosen:
    def __init__(self):
        self.__reInitialized()

    def updateDisturbeChosen(self, hostVehicleBehavior, vehListManager, vehicleList, curTime):
        maxDisturbCount = g_nDefaultMaxDisturbVehicleCount
        #print("Input vehicle list:", self.m_lVehicleChosen)
        self.__getInterestLanes()

        self.m_lVehicleChosen = self.__focusRemove(vehicleList, curTime)
        self.__focusUpdate(curTime)

        #print("After removal chosen:", self.m_lVehicleChosen, maxDisturbCount)
        if curTime - self.m_nLastCreateTime > 1510: # 每1.51s，更新一次“多车”
            self.m_lVehicleChosen = self.__focusCreate(vehicleList, vehListManager, self.m_lVehicleChosen, maxDisturbCount, curTime)
            if len(self.m_lVehicleChosen) <= 0:
                self.m_nLastCreateTime = curTime
        # self.m_lVehicleChosen = self.__focusCreate(vehicleList, self.m_lVehicleChosen, maxDisturbCount, curTime)

        #print( "######Chosen Vehicle:", self.m_lVehicleChosen )
        #print( "######Chosen dic:", self.m_dicVehicleFlag)
        #print( "After chosen:", self.m_lVehicleChosen )

        if len(self.m_lVehicleChosen) != len(self.m_dicVehicleChosenCluster):
            pass
            #print("m_lVehicleChosen is not same with m_dicVehicleChosenCluster *1*!")
        for id in self.m_lVehicleChosen:
            if str(id) not in self.m_dicVehicleChosenCluster:
                pass
                #print( self.m_lVehicleChosen, self.m_dicVehicleChosenCluster )
                #print( "m_lVehicleChosen is not same with m_dicVehicleChosenCluster *2*!" )

        if len(self.m_lVehicleChosen) > 0:
            self.m_nEmptyChosenTimer = curTime

        return (self.m_lVehicleChosen, self.m_lRemoveFocusID, self.m_dicVehicleChosenCluster)

    def __focusRemove(self, vehicleList, curTime):
        tmpVehicleList = list(self.m_lVehicleChosen)
        lInterestLanes = self.m_lInterestLanes
        self.m_lRemoveFocusID = []

        for id in self.m_lVehicleChosen:
            # 计算条件0： 车辆已不在车辆列表中。
            if id not in vehicleList:
                tmpVehicleList = self.__focusDelete(id, tmpVehicleList)
                print("### Disturbe Chosen ###", "Delete ID[", id, "] due to not in vehicleList!")
                continue

            dicVehInfo = self.m_dicVehicleFlag[str(id)]
            # 计算条件4： 车辆已经选中10s以上
            condition5 = dicVehInfo[self.m_nVehFlagEachConditionIdx][4]
            if not condition5:
                tmpVehicleList = self.__focusDelete(id, tmpVehicleList)
                print("### Disturbe Chosen ###", "Delete ID[", id, "] due to active time over 10s!")
                continue

            # 计算条件3： 车辆进入交叉口管理范围
            if not dicVehInfo[self.m_nVehFlagEachConditionIdx][3]:
                tmpVehicleList = self.__focusDelete(id, tmpVehicleList)
                print( "### Disturbe Chosen ###", "Delete ID[", id, "] due to control by Junction!" )
                continue

            # 计算条件0： 车辆不在主车的周边车道。 todo: 暂时未考虑交叉口的情况
            condition1 = dicVehInfo[self.m_nVehFlagEachConditionIdx][0]
            if not condition1 and not self.__checkCondition_FalseTimer(id, curTime, 1000): #不满足车道要求，且持续时间1s
                tmpVehicleList = self.__focusDelete(id, tmpVehicleList)
                print("### Disturbe Chosen ###", "Delete ID[", id, "] due to not interested lanes!")
                continue

            # 计算条件1： 车辆不在主车的有效距离内
            condition2 = dicVehInfo[self.m_nVehFlagEachConditionIdx][1]
            if not condition2 and not self.__checkCondition_FalseTimer(id, curTime, 500): # 不满足距离要求，且持续时间0.5s
                tmpVehicleList = self.__focusDelete(id, tmpVehicleList)
                print("### Disturbe Chosen ###", "Delete ID[", id, "] due to out of distance range!")
                continue

            # 计算条件2： 车辆产生了2次以上的换道行为
            condition3 = dicVehInfo[self.m_nVehFlagEachConditionIdx][2]
            if not condition3 and not self.__checkCondition_FalseTimer(id, curTime, 500): # 不满足换道次数要求，且持续时间0.5s
                tmpVehicleList = self.__focusDelete(id, tmpVehicleList)
                print("### Disturbe Chosen ###", "Delete ID[", id, "] due to lane-change beyond two times!")
                continue

        return tmpVehicleList

    def __focusUpdate(self, curTime):
        check_interestLanes = self.__checkCondition_interestLanes
        check_distance = self.__checkCondition_distance
        check_laneChangeCount = self.__checkCondition_laneChangeBehavior
        check_overTime = self.__checkCondition_overTime
        for id in self.m_lVehicleChosen:
            if id in self.m_lRemoveFocusID: continue
            if( str(id) not in self.m_dicVehicleFlag ):
                self.__focusInit(id, curTime)
                #print( " Re-added in focusUpdate [", id, "]")
                continue
            dicVehInfo = self.m_dicVehicleFlag[str(id)]

            condition1 = dicVehInfo[self.m_nVehFlagEachConditionIdx][0] = check_interestLanes(id)
            condition2 = dicVehInfo[self.m_nVehFlagEachConditionIdx][1] = check_distance(id)
            condition3 = dicVehInfo[self.m_nVehFlagEachConditionIdx][2] = getVehicleModelingControlRights(id, 80)
            condition4 = dicVehInfo[self.m_nVehFlagEachConditionIdx][3] = check_laneChangeCount(id)
            condition5 = dicVehInfo[self.m_nVehFlagEachConditionIdx][4] = check_overTime(id, curTime)
            if condition1 and condition2 and condition3 and condition4 and condition5:
                condition = True
            else:
                condition = False


            lastLaneID = dicVehInfo[self.m_nVehFlagLaneBelongIdx]
            curLaneID = myGetLaneID(id)
            if lastLaneID != "":
                if lastLaneID != curLaneID:
                    dicVehInfo[self.m_nVehFlagLaneBelongIdx] = curLaneID
                    dicVehInfo[self.m_nVehFlagLaneChangeCountIdx] += 1
            else:
                if curLaneID != "":
                    dicVehInfo[self.m_nVehFlagLaneBelongIdx] = curLaneID
                    dicVehInfo[self.m_nVehFlagLaneChangeCountIdx] = 0

            dicVehInfo[self.m_nVehFlagConditionIdx] = condition
            if condition:
                dicVehInfo[self.m_nVehFlagTimeFlagIdx] = True
                dicVehInfo[self.m_nVehFlagStartTimeIdx] = curTime
            #print( id, dicVehInfo )

    def __focusCreate(self, vehicleList, vehListManager, vehicleChosen, maxDisturbVehicle, curTime):
        if len(vehicleChosen) >= maxDisturbVehicle: return vehicleChosen
        idx = 0
        alreadyChosenLength = len(vehicleChosen)
        nNeedVehicles = maxDisturbVehicle - alreadyChosenLength
        lTmpVehicleChosenList = vehicleChosen
        lVehicleAroundHost = []

        hostLaneID = myGetLaneID(g_nHostVehicleID)

        func_getLeaders = vehListManager.getLeaders
        func_getFollowers = vehListManager.getFollowers
        for laneID in self.m_lInterestLanes:
            if laneID == hostLaneID: continue
            #(followers, leaders) = myGetAllVehiclesWithHostVehicleReleated(laneID, self.m_dCreateFocus_LeaderDistance, self.m_dCreateFocus_FollowerDistance)  #寻找车道内，主车前方20米后方50米范围内的所有车辆
            followers = func_getFollowers(laneID, self.m_dCreateFocus_FollowerDistance)
            leaders = func_getLeaders(laneID, self.m_dCreateFocus_LeaderDistance)
            condition = True
            # dir = 0
            # if myGetLeftLaneIDBasedLane(laneID) == hostLaneID:
            #     dir = 1
            # else:
            #     dir = -1
            for id in leaders:
                # sLID = getLeaderVehicle(id)
                # if dir == 1:
                #     sRefLID = getLeftLeaderVehicle(id)
                # else:
                #     sRefLID = getRightLeaderVehicle(id)
                # if myGetLongitudinalDistance( sRefLID, sLID ) < 5:
                #     condition = False
                # else:
                #     condition = True
                if id not in vehicleChosen and id not in self.m_lRemoveFocusID and getVehicleModelingControlRights(id, 80) and condition:
                    lVehicleAroundHost.append(id)
            # todo: 这里是希望更大可能地选择“主车前车”，只有当前车不够时，才选择后车。
            if( len(lVehicleAroundHost) >= nNeedVehicles ):
                continue
            #print("************************Leader not enough!", lVehicleAroundHost, nNeedVehicles)
            for id in followers:
                if id not in vehicleChosen and id not in self.m_lRemoveFocusID and getVehicleModelingControlRights(id, 80):
                    lVehicleAroundHost.append(id)


        count = len(lVehicleAroundHost)
        while idx < nNeedVehicles:
            if count <= 0:
                break
            vehID = lVehicleAroundHost[myGetRandomInt(0, count)]
            lVehicleAroundHost.remove(vehID)
            count -= 1

            self.__focusInit(vehID, curTime)
            lTmpVehicleChosenList.append(vehID)
            idx = idx + 1

        return lTmpVehicleChosenList

    def __focusInit(self, vehID, curTime):
        condition1 = self.__checkCondition_interestLanes(vehID)
        condition2 = self.__checkCondition_distance(vehID)
        condition = condition1 or condition2

        timerFlag = False
        if condition: timerFlag = True
        else: timerFlag = False

        timerStartTime = 9999999
        if condition: timerStartTime = curTime

        laneID = myGetLaneID(vehID)
        laneChangeCount = 0
        self.m_dicVehicleFlag[str(vehID)] = [ condition, timerFlag, timerStartTime, laneID, laneChangeCount, [True, True, True, True, True], curTime]

        drType = self.__createDriverType()

        hostLane = myGetLaneID(g_nHostVehicleID)

        if hostLane == laneID:
            laneDirType = 0
        elif myGetLeftLaneIDBasedLane(hostLane) == laneID:
            laneDirType = 1
        else:
            laneDirType = -1

        self.m_dicVehicleChosenCluster[ str(vehID) ] = [vehID, drType, laneDirType, curTime]
        print( "### Disturbe Chosen:", vehID, "with driverType", drType, "[", myGetLaneID(vehID), "]", "DeltaS:", myGetLongitudinalDistance(vehID, g_nHostVehicleID) )
        pass

    def __focusDelete(self, id, vehicleList):
        self.m_lRemoveFocusID.append(id)
        vehicleList.remove(id)
        self.m_dicVehicleFlag.pop(str(id))
        self.m_dicVehicleChosenCluster.pop(str(id))
        changeLateralOffset(id, 0)
        return vehicleList

    def __getInterestLanes(self):
        self.m_lInterestLanes = []
        hostLaneID = myGetLaneID(g_nHostVehicleID)
        if hostLaneID == "":
            print("Warning: cannot get host vehicle in lane. Skip!")
            return
        if myCheckInternalLane(hostLaneID):
            #print("Warning: host vehicle is in an internal lane. Skip!")
            return
        leftLaneID = myGetLeftLaneID(g_nHostVehicleID)
        rightLaneID = myGetRightLaneID(g_nHostVehicleID)

        self.m_lInterestLanes.append(hostLaneID)
        if leftLaneID != "":
            self.m_lInterestLanes.append(leftLaneID)
        if rightLaneID != "":
            self.m_lInterestLanes.append(rightLaneID)
        return self.m_lInterestLanes

    def __checkCondition_interestLanes(self, clientID):
        clientLaneID = myGetLaneID(clientID)
        # 计算条件1： 车辆不在主车的周边车道。 todo: 暂时未考虑交叉口的情况
        if clientLaneID not in self.m_lInterestLanes:
            return False
        else:
            clientLaneID = myGetLaneID(clientID)
            hostLaneID = myGetLaneID(g_nHostVehicleID)
            if clientLaneID == hostLaneID:
                clientStation = myGetDistanceFromLaneStart(clientID)
                hostStation = myGetDistanceFromLaneStart(g_nHostVehicleID)
                if clientStation < hostStation:
                    return False
                else:
                    return True
            else:
                return True

    def __checkCondition_distance(self, clientID):
        # 计算条件2： 车辆不在主车的有效距离内
        deltaDistance = myGetLongitudinalDistance(clientID, g_nHostVehicleID)
        if self.m_dRemoveFocus_LeaderDistance > deltaDistance > -self.m_dRemoveFocus_FollowerDistance:
            return True
        else:
            #print("Disturbe Vehicle is out of range now.")
            return False

    def __checkCondition_laneChangeBehavior(self, clientID):
        # 计算条件3： 车辆产生两次以上的换道行为
        dicVehInfo = self.m_dicVehicleFlag[str(clientID)]
        laneChangeBehavior = dicVehInfo[self.m_nVehFlagLaneChangeCountIdx]

        if laneChangeBehavior <= 1:
            return True
        else:
            return False

    def __checkCondition_overTime(self, clientID, curTime):
        if curTime - self.m_dicVehicleFlag[str(clientID)][self.m_nVehFlagInitTime] < self.m_nDisturbeMaxTime:
            return True
        else:
            return False

    def __checkCondition_FalseTimer(self, clientID, curTime, limitDuration):
        dicVehInfo = self.m_dicVehicleFlag[str(clientID)]
        #print( dicVehInfo, curTime )
        if not dicVehInfo[self.m_nVehFlagTimeFlagIdx]:
            return True
        if dicVehInfo[self.m_nVehFlagConditionIdx]:
            return True
        timerStartTime = dicVehInfo[self.m_nVehFlagStartTimeIdx]
        if curTime - timerStartTime > limitDuration:
            #print("^^^^^^^^^^^^^^^^^^^^^^^^^^")
            return False
        else:
            return True

    def __createDriverType(self):
        t1 = g_nDefaultAggressiveDriverPercent
        t2 = g_nDefaultAggressiveDriverPercent + g_nDefaultNormalDriverPercent
        t3 = 1
        value = myGetRandomDouble(0, 1)
        result = 0
        if 0 <= value <= t1:
            result = 0 # Speed Aggressive Type
        elif t1 < value <= t2:
            result = 10 # Normal Comfort Type
        else:
            result = 20 # Rookie Type
        #print( "Driver Type:", result )
        return result

    def __reInitialized(self):
        self.m_lVehicleChosen = []
        self.m_dicDriverType = {}
        self.m_dicVehicleChosenCluster = {}

        self.m_dicVehicleFlag = {}
        self.m_lInterestLanes = []
        self.m_lRemoveFocusID = []
        self.m_nLastCreateTime = 0

        self.m_nEmptyChosenTimer = 0

        self.m_nVehFlagConditionIdx = 0
        self.m_nVehFlagTimeFlagIdx = 1
        self.m_nVehFlagStartTimeIdx = 2
        self.m_nVehFlagLaneBelongIdx = 3
        self.m_nVehFlagLaneChangeCountIdx = 4
        self.m_nVehFlagEachConditionIdx = 5
        self.m_nVehFlagInitTime = 6

        self.m_dCreateFocus_LeaderDistance = g_dCreateFocus_MaxLeaderDistance
        self.m_dCreateFocus_FollowerDistance = g_dCreateFocus_MaxFollowerDistance
        self.m_dRemoveFocus_LeaderDistance = g_dRemoveFocus_MaxLeaderDistance
        self.m_dRemoveFocus_FollowerDistance = g_dRemoveFocus_MaxFollowerDistance
        self.m_nDisturbeMaxTime = g_dRemoveFocus_MaxDisturbDuration

    #self.m_dicVehicleFlag[str(vehID)] = {"Condition": condition, "TimerFlag": timerFlag, "StartTime": timerStartTime,"LaneBelongs": laneID, "LaneChangeCount": laneChangeCount}

    m_lVehicleChosen = []
    m_dicDriverType = {}
    m_dicVehicleChosenCluster = {}

    m_dicVehicleFlag = {}
    m_lInterestLanes = []
    m_lRemoveFocusID = []
    m_nLastCreateTime = 0

    m_nEmptyChosenTimer = 0

    m_nVehFlagConditionIdx = -1
    m_nVehFlagTimeFlagIdx = -1
    m_nVehFlagStartTimeIdx = -1
    m_nVehFlagLaneBelongIdx = -1
    m_nVehFlagLaneChangeCountIdx = -1
    m_nVehFlagEachConditionIdx = -1

    m_dCreateFocus_LeaderDistance = 0
    m_dCreateFocus_FollowerDistance = 0
    m_dRemoveFocus_LeaderDistance = 0
    m_dRemoveFocus_FollowerDistance = 0
    m_nVehFlagInitTime = 0
    m_nDisturbeMaxTime = 0








# ==================================
#           基础函数
# =================================

# 函数: myCheckHostVehicleInitialized
# 用途: 判断主车是否完成初始化过程
# [i]: vehID - 车辆ID
# [o]: Bool - True已经完成，False正在初始化
def myCheckHostVehicleInitialized():
    (hostX, hostY) = myGetHostVehicleCoordinate()
    if ((hostX < -999999.0 and hostY < -999999.0) or (hostX == 0.0 and hostY == 0.0)) and (
            myGetSimulationTimeStamp() < 1000):
        return False
    else:
        return True

def myGetSimulationTimeStamp():
    global g_dicLocalUserData
    return g_dicLocalUserData["time"]


# 函数: myAddVehicleRelated
# 用途: 根据相对位置，添加车辆，接口与PanoSimAPI相同，仅对距离产生判断
# [i]: id - 参考车ID
# [i]: s - 相对station
# [i]: l - 相对offset
# [i]: speed - 车速
# [i]: lane -   -1：左侧； 0：当前； 1：右侧
# [i]: type - 车辆类型
# [i]: shape - 车辆形状
# [i]: driver - 驾驶员类型
# [o]: int - 创建的车辆ID
def myAddVehicleRelated(id, s, l, speed, lane=0, type=0, shape=0, driver=0):
    if speed < g_dDefaultMinVehicleSpeed: speed = g_dDefaultMinVehicleSpeed
    targetStation = myGetDistanceFromLaneStart(id)
    if s + targetStation < 0: return -1
    if s > myGetDistanceToLaneEnd(id): return -1

    dstId = addVehicleRelated(id, s, l, speed, lane_type(lane), vehicle_type(type), )
    if dstId > 0:
        if not mySetValidRoute1(dstId):
            global g_dicAddedVehicleByXY
            g_dicAddedVehicleByXY[str(dstId)] = 0
        global g_dicExceedVehicles, g_dicVehicleLateralProperty
        g_dicExceedVehicles[str(dstId)] = 0
        g_dicVehicleLateralProperty[str(dstId)] =  myGetSimulationTimeStamp() + myGetRandomInt( 1000, 3000 )
    return dstId

# def myAddVehicleRelatedJunction(id, direction, route, speed, distance, type=0, shape=0, driver=0):
#     if speed < g_dDefaultMinVehicleSpeed: speed = g_dDefaultMinVehicleSpeed
#     dstId = addVehicleRelatedJunction(id, direction, route, speed, distance, vehicle_type(type),)
#     if dstId > 0:
#         mySetValidRoute(dstId, True)
#         global g_dicExceedVehicles
#         g_dicExceedVehicles[str(dstId)] = 0
#     return dstId

def myAddVehicleInLane(laneID, s, speed, type=0, shape=0, driver=0):
    if speed < g_dDefaultMinVehicleSpeed: speed = g_dDefaultMinVehicleSpeed
    #print( "in myAddVehicleInLane:", laneID, s )
    (x,y) = getXYFromSL( laneID, s, 0)
    if x == None or y == None : return -1

    dstId = addVehicle(x, y, speed, vehicle_type(type), )  # todo: 该函数后，无法获得laneID，导致该车到交叉口就消失了
    if dstId > 0:
        if not mySetValidRoute1(dstId):
            global g_dicAddedVehicleByXY
            g_dicAddedVehicleByXY[str(dstId)] = 0
        global g_dicExceedVehicles, g_dicVehicleLateralProperty
        g_dicExceedVehicles[str(dstId)] = 0
        g_dicVehicleLateralProperty[str(dstId)] =  myGetSimulationTimeStamp() + myGetRandomInt( 1000, 3000 )
    return dstId

def mySetLateralOffset(id):
    return
    global g_dicVehicleLateralProperty
    i_dicVehLat = g_dicVehicleLateralProperty
    if str(id) not in i_dicVehLat:
        i_dicVehLat[str(id)] = myGetSimulationTimeStamp() + myGetRandomInt(1000, 3000)
    if myGetSimulationTimeStamp() > i_dicVehLat[str(id)]:
        curOffset = getVehicleLateralOffset(id)
        nextOffset = curOffset + myGetRandomDouble(-0.05, 0.05)
        if nextOffset < -0.5: nextOffset = -0.5
        elif nextOffset > 0.5: nextOffset = 0.5
        i_dicVehLat[str(id)] = myGetSimulationTimeStamp() + myGetRandomInt(1000, 3000)
        changeLateralOffset(id, nextOffset)


def myDeleteVehicle(id):
    if id > 0:
        deleteVehicle(id)
        global g_dicExceedVehicles, g_dicAddedVehicleByXY, g_dicVehicleLateralProperty
        i_dicExceedVehicle = g_dicExceedVehicles
        i_dicAddedVehicleByXY = g_dicAddedVehicleByXY
        i_dicVehicleLateralProperty = g_dicVehicleLateralProperty
        if str(id) in i_dicExceedVehicle:
            i_dicExceedVehicle.pop(str(id))
        if str(id) in i_dicAddedVehicleByXY:
            i_dicAddedVehicleByXY.pop(str(id))
        if str(id) in i_dicVehicleLateralProperty:
            i_dicVehicleLateralProperty.pop(str(id))


# 函数: myGetFirstVehicleInCurLane
# 用途: 获得vehID当前车道中的头车
# [i]: vehID - 车辆ID
# [o]: vehID - 头车ID。   -1：无车；  INVALID：无车道；   其他：头车ID
def myGetFirstVehicleInCurLane(vehID, maxDistanceWithHost = 999999, type = 0):
    if vehID < 0: return -1
    curLane = myGetLaneID(vehID)

    curLeaderID = getLeaderVehicle(vehID)
    if type == 0:
        if curLeaderID == -1:
            return -1
        elif curLeaderID == MY_PANOSIM_INVALID_VALUE:
            print("Error when searching leader in current lane, returned invalid int value!")
            return MY_PANOSIM_INVALID_VALUE
    else:
        if curLeaderID < 0:  # -1 or INVALID_VALUE
            return vehID
    deltaDistance = myGetLongitudinalDistance(curLeaderID, vehID)
    if deltaDistance > maxDistanceWithHost :
        if type == 0:
            return -1
        else:
            return vehID
    return myGetFirstVehicleInCurLane(curLeaderID, maxDistanceWithHost, 1)


# 函数: myGetFirstVehicleInLeftLane
# 用途: 获得vehID左车道中的头车
# [i]: vehID - 车辆ID
# [o]: vehID - 头车ID。   -1：无车；  INVALID：无车道；   其他：头车ID
def myGetFirstVehicleInLeftLane(vehID, maxDistanceWithHost = 999999):
    leftLeaderID = getLeftLeaderVehicle(vehID)

    if leftLeaderID == -1:  # 有路没车
        leftFollowerID = getLeftFollowerVehicle(vehID)
        if leftFollowerID == -1:
            return -1
        elif leftFollowerID == MY_PANOSIM_INVALID_VALUE:
            print("Error in searching left follower in myGetFirstVehicleInLeftLane")
            return -1
        else:
            return myGetFirstVehicleInCurLane(leftFollowerID, maxDistanceWithHost, 1)
    elif leftLeaderID == MY_PANOSIM_INVALID_VALUE:
        print("Error in left first leader")
        return MY_PANOSIM_INVALID_VALUE
    else:
        deltaDistance = myGetLongitudinalDistance(leftLeaderID, g_nHostVehicleID)
        if deltaDistance > maxDistanceWithHost:
            return -1
        else:
            return myGetFirstVehicleInCurLane(leftLeaderID, maxDistanceWithHost, 1)


# 函数: myGetFirstVehicleInRightLane
# 用途: 获得vehID右车道中的头车
# [i]: vehID - 车辆ID
# [o]: vehID - 头车ID。   -1：无车；  INVALID：无车道；   其他：头车ID
def myGetFirstVehicleInRightLane(vehID, maxDistanceWithHost = 999999, type = 0):
    rightLeaderID = getRightLeaderVehicle(vehID)

    if rightLeaderID == -1:  # 有路没车
        rightFollowerID = getRightFollowerVehicle(vehID)
        if rightFollowerID == -1:
            return -1
        elif rightFollowerID == MY_PANOSIM_INVALID_VALUE:
            print("Error in searching right follower in myGetFirstVehicleInRightLane")
            return -1
        else:
            return myGetFirstVehicleInCurLane(rightFollowerID, maxDistanceWithHost, 1)
    elif rightLeaderID == MY_PANOSIM_INVALID_VALUE:
        print("Error in right first leader")
        return MY_PANOSIM_INVALID_VALUE
    else:
        deltaDistance = myGetLongitudinalDistance(rightLeaderID, g_nHostVehicleID)
        if deltaDistance > maxDistanceWithHost:
            return -1
        else:
            return myGetFirstVehicleInCurLane(rightLeaderID, maxDistanceWithHost, 1)

# 函数: clearVehicleList
# 用途: 清除车辆列表中非主车车辆
def __clearVehicleList():
    for id in getVehicleList():
        if id == g_nHostVehicleID:
            continue
        else:
            deleteVehicle(id)

# 函数: setInitializedVehicles
# 用途: 初始化车辆，放置全局头车以及其他交通车
# [i]: density - 1km内的车辆数
# [o]: globalLeader - 全局头车
# [o]: lVehicleList - 除全局头车的车辆信息
# [o]: vehCountInLane - 车道内需要的车辆数目
# [o]: avgVehiclesInterval - 平均车辆间隔
def setInitializedVehicles(density):
    __clearVehicleList()
    if not myCheckHostVehicleInitialized():
        return ([], [], 0, 0)

    lTopVehicles = []
    lVehicleList = []

    hostSpeed = myGetHostVehicleSpeed()
    ######
    ###### 设置各车道头车
    ######
    tmpLeaderDistance = g_dMaxLeaderDistance - 20
    laneLength = myGetLaneLength( myGetLaneID(g_nHostVehicleID) )
    hostStation = myGetDistanceFromLaneStart( g_nHostVehicleID )
    if hostStation+tmpLeaderDistance > laneLength - 5:
        tmpLeaderDistance = laneLength - 5 - hostStation

    # 增加当前车道头车
    tmpIDinSameLane = myAddVehicleRelated(g_nHostVehicleID, tmpLeaderDistance, 0.0, hostSpeed, 0)
    lTopVehicles.append(tmpIDinSameLane)

    # 单独定义：
    # 设置左侧车道（们）的头车
    setNeighborTopVehicles(tmpIDinSameLane, tmpLeaderDistance, -1, 0, lTopVehicles)

    # 设置右侧车道（们）的头车
    setNeighborTopVehicles(tmpIDinSameLane, tmpLeaderDistance, 1, 0, lTopVehicles)

    print("Init top leaders:", lTopVehicles)

    ######
    ###### 设置各车道车辆
    ######
    hostVehicleLength = getVehicleLength(getVehicleType(g_nHostVehicleID))
    vehAssignmentCountInLane = density * (g_dMaxLeaderDistance + g_dMaxFollowerDistance) / 1000
    if vehAssignmentCountInLane <= 1: vehAssignmentCountInLane = 2
    avgVehiclesInterval = (g_dMaxLeaderDistance + g_dMaxFollowerDistance) / vehAssignmentCountInLane
    print("VehicleInLane:", vehAssignmentCountInLane, "VehicleInterval:", avgVehiclesInterval)

    for laneTopLeaderID in lTopVehicles:
        vehCountInLane = 1
        sumInterval = 0
        leaderVehicleLength = getVehicleLength(getVehicleType(laneTopLeaderID))
        while vehCountInLane <= vehAssignmentCountInLane:
            randomInterval = random.ranf() * avgVehiclesInterval / 3.0
            #print("Value: ", randomInterval, "Type: ", type(randomInterval))
            sumInterval = sumInterval \
                          + avgVehiclesInterval \
                          + leaderVehicleLength \
                          + randomInterval
            # + 0 #这里应该加一个随机数
            if laneTopLeaderID == tmpIDinSameLane \
                    and abs(g_dMaxLeaderDistance - sumInterval) < 30:
                print( "Skip because of too near to host!" )
                continue

            tmpID = myAddVehicleRelated(laneTopLeaderID, -sumInterval, 0.0, hostSpeed, 0)
            if tmpID > 0:
                lVehicleList.append(tmpID)
                vehCountInLane = vehCountInLane + 1
                leaderVehicleLength = getVehicleLength(getVehicleType(tmpID))
            else:
                break

    print( "Total Vehicle Count: ", len(lTopVehicles) + len(lVehicleList) )

    return (lTopVehicles, lVehicleList, vehAssignmentCountInLane, avgVehiclesInterval)

# 函数: setNeighborTopVehicles
# 用途: 迭代设置旁车道头车
# [i]: ownVehicleID - 基于的车辆ID
# [i]: ownVehicleStation - 基于的车辆station
# [i]: dir - 方向   -1：左；  1：右
# [i]: iteration - 默认为0，迭代的起始次数
# [io]: topVehicles - 头车列表
def setNeighborTopVehicles(ownVehicleID, ownVehicleStation, dir, iteration, topVehicles):
    print("Set neighbor tops")
    if dir == 0:
        return
    if not myCheckLaneExist(ownVehicleID, dir):
        return

    baseDeltaStation = g_dMaxLeaderDistance * 0.2
    if baseDeltaStation > 50:
        baseDeltaStation = 50.0

    factor = 1.0
    if iteration % 2 == 1:
        factor = 1.0
    else:
        factor = -1.0

    randomDeltaStation = (baseDeltaStation * myGetRandomDouble(0, 1) + 20.0) * factor
    print( "NeighborTopVehicle: ", randomDeltaStation )
    finalStation = ownVehicleStation + randomDeltaStation

    tmpID = myAddVehicleRelated(ownVehicleID, randomDeltaStation, 0, myGetHostVehicleSpeed(), dir)
    if tmpID < 0:
        print("Error in setNeighborTopVehicles when creating vehicles", dir, iteration)
        return

    topVehicles.append(tmpID)
    setNeighborTopVehicles(tmpID, finalStation, dir, iteration + 1, topVehicles)
    return


# 函数: getVehicleListInLaneRelatedHost
# 用途: 基于主车返回，主车所在的车道中的所有车辆，范围限制在主车后g_dMaxFollowerDistance米的范围
# [i]: direction  -1:left, 0: cur, 1:right
# [o]: vehicleList[]
def getVehicleListInLaneRelatedHost(vehID, dir):
    lVehicleListInLane = []
    if dir<0:
        firstCar = myGetFirstVehicleInLeftLane(vehID)
    elif dir==0:
        firstCar = myGetFirstVehicleInCurLane(vehID)
    else:
        firstCar = myGetFirstVehicleInRightLane(vehID)

    if firstCar < 0 :
        if dir < 0: firstCar = getLeftFollowerVehicle(g_nHostVehicleID)
        elif dir == 0: firstCar = getFollowerVehicle(g_nHostVehicleID)
        else: firstCar = getRightFollowerVehicle(g_nHostVehicleID)

        deltaDistance = myGetLongitudinalDistance(firstCar, g_nHostVehicleID)
        if (firstCar > 0 and deltaDistance < -g_dMaxFollowerDistance) or firstCar < 0:
            firstCar = MY_PANOSIM_INVALID_VALUE  # INVALID

    if firstCar <= 0:
        return lVehicleListInLane

    lVehicleListInLane.append(firstCar)
    while firstCar >= 0 :
        followerId = getFollowerVehicle(firstCar)
        if followerId == 0: continue
        elif followerId < 0:
            firstCar = followerId
            break
        else:
            deltaDistance = myGetLongitudinalDistance(followerId, g_nHostVehicleID)
            if deltaDistance < -g_dMaxFollowerDistance: break
            lVehicleListInLane.append(followerId)
            firstCar = followerId

    return lVehicleListInLane


# 函数: myGetAllLanesInEdge
# 用途: 基于一个车道，获得edge中的所有车道
# [i]: laneID
# [o]: lanes[], laneDir[]
def myGetAllLanesInEdge(laneID):
    lLanes = []
    lLaneDir = []

    if laneID == "": return (lLanes, lLaneDir)
    if myCheckInternalLane(laneID): return (lLanes, lLaneDir)

    lLanes.append(laneID)
    lLaneDir.append(0)
    # 寻找左侧
    idx = -1
    targetLane = laneID
    while  targetLane!="":
        targetLane = myGetLeftLaneIDBasedLane(targetLane)
        if targetLane!="":
            lLanes.append(targetLane)
            lLaneDir.append(idx)
            idx = idx - 1
    # 寻找右侧
    idx = 1
    targetLane = laneID
    while targetLane!="":
        targetLane = myGetRightLaneIDBasedLane(targetLane)
        if targetLane!="":
            lLanes.append(targetLane)
            lLaneDir.append(idx)
            idx = idx + 1
    return (lLanes, lLaneDir)


def myGetAllVehiclesWithHostVehicleReleated(laneID, maxLeaderDistance, maxFollowerDistance):
    lFollowers = []
    lLeaders = []
    if laneID=="" : return (lFollowers, lLeaders)
    vehicleListInLane = myGetVehiclesInLane1(laneID)

    inited = False
    for idx in vehicleListInLane:
        if idx == g_nHostVehicleID:
            continue
        deltaDistance = myGetLongitudinalDistance( idx, g_nHostVehicleID )
        # if deltaDistance < -maxFollowerDistance :
        #     continue
        # elif deltaDistance > maxLeaderDistance:
        #     continue
        # else:
        if -maxFollowerDistance <= deltaDistance <= maxLeaderDistance:
            inited = True
            #print( "^^^^^^^^^^^^^", deltaDistance )
            if deltaDistance < 0:
                lFollowers.append(idx)
            else:
                lLeaders.append(idx)
        else:
            if inited:
                break
    return (lFollowers, lLeaders)

def myGetLeaders(vehID, maxLeaderDistance = 99999999):
    lLeaders = []
    if vehID < 0 : return lLeaders
    vehicleListInlane = myGetVehiclesInLane1(myGetLaneID(vehID))

    for idx in vehicleListInlane:
        deltaDistance = myGetLongitudinalDistance( vehID, idx )
        if deltaDistance >= 0 : continue
        else:
            lLeaders.append( idx )
    return lLeaders

def myGetAllToLanes(laneID):
    lToLanes = []
    dirs = myGetValidDirections(laneID)
    for dir in dirs:
        route = myGetNextLanes(laneID, dir)
        if len(route) > 0 :
            toLane = route[ len(route)-1 ]
            if myCheckInternalLane(toLane) :
                pass
                #print( "Warning: an internal lane is regarded!" )
            else:
                lToLanes.append( toLane )
    return lToLanes

def myIDMFollower(leaderID, followID, expSpeed, maxAcc=5.0, minDistance = 2.0, headway = 1.0):
    iMinDistance = minDistance
    iMaxAcc = maxAcc
    iBrakeCommon = -3
    iHeadway = headway

    leaderSpeed = myGetVehicleSpeed(leaderID)
    followSpeed = myGetVehicleSpeed(followID)
    DelU = followSpeed - leaderSpeed
    DelS = myGetLongitudinalDistance(leaderID, followID)
    if DelS < iMinDistance:
        return followSpeed - 6 * 0.01
    Sd = followSpeed * iHeadway
    if Sd < 0.01: Sd = iMinDistance
    ax_IDM = iMaxAcc * (1-math.pow(followSpeed/expSpeed, 4) - math.pow(Sd/DelS,2))
    if ax_IDM < -7:
        ax_IDM = -7
    return followSpeed + ax_IDM * 0.01

def mySpeedSetting(expSpeed):
    if expSpeed < g_dDefaultMinVehicleSpeed:
        expSpeed = g_dDefaultMinVehicleSpeed
    return expSpeed